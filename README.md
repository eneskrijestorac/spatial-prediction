Requirements:

+ tensorflow 1.13.1
+ numpy
+ scipy
+ pandas
+ matplotlib

Module explanations:

+ city_gen.py generates Wireless InSite simulation setup files with randomly generated cities and transmitter locations. These files can be used to run Wireless InSite simulations.
+ data_set.py is an API class that is used to access data produced by WirelessInsite simulations
+ process_ray_trac_dat_new.py processes data produced by WirelessInsite before it can be utilized by data_set.py
+ fit_model.py is used to fit a path propagation model in data produce by WirelessInsite. Not utilized as of now.
+ geometry.py contains various helper functions for building and handling city geometric objects
+ predictor.py defines the class that is used to instantiate and train deep learning models for spatial prediction
+ train_predictor.py is wrapper around predictor.py. Run it to train a model with particular parameters
+ test_predictor.py tests trained spatial prediction models with optional comparison against the Ordinary Kriging benchmark
+ calculate.py is used to run comparisons against the Ordinary Kriging benchmark
+ channel_reconstruction.py implements a benchmark approach described in: https://web.ece.ucsb.edu/~ymostofi/papers/GC17_MuralidharanMostofi.pdf
+ active_sensing.py and active_sensing_utils.py are in development for extensions of this work

