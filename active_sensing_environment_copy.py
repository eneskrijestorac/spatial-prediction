from data_set import Dataset
from predictor import Predictor
from rl_utils import get_rl_obs
from active_sensing_utils import EnviornmentState, ActionSpace, PredictorWrapper, divide_area_among_uavs
import random
from utils import get_session, plot_grid, plot_confusion_matrix
import numpy as np
from matplotlib import pyplot as plt
from multiprocessing import Pool
from semivar import estimate_eta, semivar_fit, kriging_fit
import tensorflow as tf
import json
from tf_agents.trajectories import time_step as ts
import tf_agents
import os

MAX_EPISODE_LENGTH = 10


class ExplorationEnvironment:
	'''
	'''
	def __init__(self, predictor: Predictor, dataset: Dataset, n_uavs = 1, seed = 3123213, blind = False,
			max_episode_length = MAX_EPISODE_LENGTH, policy = 'greedy', 
			repeat_probability = 0.9, planning_steps = 100, used_steps = 20,
			kriging_predictor = None, rl_policy_path = None, use_train_ds = False, uav_starting_proximity = True):
		"""
		"""
		random.seed(seed)
		self.n_uavs = n_uavs
		self.n_users = 1
		self.max_episode_length = max_episode_length
		self.repeat_probability = repeat_probability # Probability of repeating the same action if using random exploration models
		self.policy = policy # Exploration policy
		self.blind = blind
		self.ds = dataset
		self.uav_starting_proxmity = uav_starting_proximity
		self.flying_altitude = self.ds.UAV_ALTITUDE
		self.grid_len = self.ds.GRID_LEN # Spacing between grid points
		self.action_space = ActionSpace(n_uavs, self.flying_altitude,self.repeat_probability) # Only four possible discrete actions
		self.predictor = predictor # Predictor object that will provide mean and variance of signal strength
		# Used by the least uncertainty path planning algorithms
		self.planning_steps = planning_steps
		self.used_steps = used_steps 
		self.max_episode_length = max_episode_length
		self.kriging_predictor = kriging_predictor
		self.use_train_ds = use_train_ds



		# Information retained over all episodes
		self.paths = {} # Store the paths over all previous episodes if self.fixed_map
		for i in range(n_uavs):
			self.paths[i] = []		

		# Restore RL policy
		if rl_policy_path != None:
			converter = tf.lite.TFLiteConverter.from_saved_model(rl_policy_path, signature_keys=["action"])

			converter.target_spec.supported_ops = [
			tf.lite.OpsSet.TFLITE_BUILTINS, # enable TensorFlow Lite ops.
			tf.lite.OpsSet.SELECT_TF_OPS # enable TensorFlow ops.
			]
			tflite_policy = converter.convert()
			with open(os.path.join(rl_policy_path, 'policy.tflite'), 'wb') as f:
				f.write(tflite_policy)

			interpreter = tf.lite.Interpreter(os.path.join(rl_policy_path, 'policy.tflite'))
			self.policy_runner = interpreter.get_signature_runner()
				
			print("Loaded RL policy from", rl_policy_path)
			self.rl_policy = tf.saved_model.load(rl_policy_path)
			config_json = open(rl_policy_path + "/config.json")
			self.rl_config_json = json.loads(config_json.read())
			self.rl_config_json = json.loads(self.rl_config_json)


		

	def get_action_space_size(self,):
		return self.action_space.size 

	def reset(self, ss_grids = None, building_grid = None, user_locations = None, 
	   				initial_coordinates = None, scenario_idx = None, transmitter_idx = None):
		# Information retained over a single episode
		self.episode_rewards = []
		self.episode_lengths = []
		self.success_history = []
		self.total_steps = np.zeros(self.n_uavs)
		self.state_history = {} # Stores the coordinates of the UAV motion during a single episode


		if ss_grids is None:
			# Loads a random set of grids
			# Each signal gain grid corresponds to a random environment
			# One signal gain grid per user
			self.ss_grids, self.building_grid, self.user_locations = self.ds.sample_maps_per_scen(scenario_idx=scenario_idx , transmitter_idx = transmitter_idx, test_dataset=not(self.use_train_ds)) 
			user_idx = 0
			if np.mean(self.ss_grids[user_idx]) < -200:
				return self.reset()

		else:
			self.ss_grids = ss_grids
			self.building_grid = building_grid
			self.user_locations = user_locations
		assert self.building_grid.shape == self.ss_grids[0].shape

		self.state = EnviornmentState(self.flying_altitude, self.building_grid, self.ss_grids, self.user_locations,
					self.max_episode_length, self.n_uavs, self.n_users, initial_coordinates=initial_coordinates, uav_starting_proximity = self.uav_starting_proxmity)


		# Planned path for each UAV
		self.action_sequence = {}
		for uav_idx in range(self.n_uavs):
			self.action_sequence[uav_idx] = []

		# State history per UAV
		for uav_idx in range(self.n_uavs):
			if not(uav_idx in self.state_history.keys()):
				self.state_history[uav_idx] = []
				self.state_history[uav_idx].append(np.copy(self.state.coordinates[uav_idx, :]))
			else:
				self.state_history[uav_idx].append(np.copy(self.state.coordinates[uav_idx, :]))
		
		# Number of explored locations
		self.n_explored_locs = 0

		# For RL policy
		self.obs_buffers = []
		self.action_overrides = 0 # Count how many times an action was inviable

		return self.ss_grids, self.building_grid, self.state.initial_coordinates, self.user_locations



	def reset_action_plan(self):
		for uav_idx in range(self.n_uavs):
			self.action_sequence[uav_idx] = []

	def get_random_exhaustive_actions(self, step_idx, exhaustive_paths = 208, stride = 1):
		actions = {}

		
		
		if any([len(self.action_sequence[uav_idx]) == 0 for uav_idx in range(self.n_uavs)]):
			paths_per_uav = []
			action_paths_per_uav  = []
			for uav_idx in range(self.n_uavs):
				states_per_path, actions_per_path = self.action_space.get_random_paths(self.state, uav_idx, exhaustive_paths, 
									self.max_episode_length - step_idx+1, stride_size=stride)
				paths_per_uav.append(states_per_path)
				action_paths_per_uav.append(actions_per_path)
		
			states_per_path_combined = []
			for ii in range(exhaustive_paths):
				path_combined = []
				for uav_idx in range(self.n_uavs):
					path_combined.append(paths_per_uav[uav_idx][ii])
				path_combined = sum(path_combined, [])
				states_per_path_combined.append(path_combined)

			best_path_idx = self.predict_ss_for_many_paths(user_idx = 0, path_continuations = states_per_path_combined)
			for uav_idx in range(self.n_uavs):
				self.action_sequence[uav_idx] = action_paths_per_uav[uav_idx][best_path_idx]

		for uav_idx in range(self.n_uavs):
			action = self.action_sequence[uav_idx].pop(0)
			actions[uav_idx] = action
		return actions
	
	def get_obs_for_rl_pol(self, sd, pred, uav_loc_grid, true_norm_gain, uav_loc_grids_per_uav, n_uavs):
		use_sd = self.rl_config_json["use_sd"]
		clairvoyante = self.rl_config_json["clairvoyante"]
		step_size = self.rl_config_json["step_size"]
		mem_length = self.rl_config_json["mem_length"]
		no_ob_downsample = self.rl_config_json["no_ob_downsample"]


		rl_ob = get_rl_obs(step_size, use_sd, clairvoyante, pred, true_norm_gain, sd, uav_loc_grid, n_uavs, uav_loc_grids_per_uav, no_ob_downsample)

		first_obs = len(self.obs_buffers) == 0

		if first_obs:
			for uav_idx in range(self.n_uavs):
				obs_buffer = []
				for _ in range(mem_length):
					obs_buffer.append(rl_ob[uav_idx].astype(np.float32))
				self.obs_buffers.append(obs_buffer)
		else:
			for uav_idx in range(self.n_uavs):
				obs_buffer = self.obs_buffers[uav_idx]
				for _ in range(mem_length):
					obs_buffer.insert(0, rl_ob[uav_idx].astype(np.float32))
					obs_buffer.pop()
				self.obs_buffers[uav_idx] = obs_buffer



		return [np.concatenate(ob_buffer, axis = -1) for ob_buffer in self.obs_buffers]


		

	
	def step(self, step_idx,  render = False, input_actions = None):
		actions = {}
		if (self.policy == 'random') or (self.policy == 'random_kriging'):
			# Get a random action for each UAV
			for uav_idx in range(self.n_uavs):
				action = self.action_space.get_random_action(self.state, self.state_history, uav_idx)
				actions[uav_idx] = action

		elif self.policy == 'random_exhaustive':
			actions = self.get_random_exhaustive_actions(step_idx=step_idx, stride=1, exhaustive_paths=400)

		elif self.policy == 'restored_rl':
			if any([len(self.action_sequence[uav_idx]) == 0 for uav_idx in range(self.n_uavs)]):
				# Predict the gain
				raw_value_pred, sd, ob, covar, pred, uav_loc_grid, true_norm_gain, uav_loc_grids_per_uav, _ = self.predict_ss(user_idx=0, no_full_covariance=True)
				rl_obs = self.get_obs_for_rl_pol(sd, pred, uav_loc_grid, true_norm_gain, uav_loc_grids_per_uav, self.n_uavs)

			for uav_idx in range(self.n_uavs):
				if len(self.action_sequence[uav_idx]) == 0:		
					
					ob = np.asarray([rl_obs[uav_idx]])

					discount = tf.constant([1.0])
					observation = tf.constant(ob.astype(np.float32))
					reward = tf.constant([1.0])
					step_type = tf.constant([0])

					from tf_agents.trajectories import TimeStep

					tf_ts = TimeStep( step_type, reward, discount, observation)

					# Get action
					action_step = self.rl_policy.action(tf_ts)
					a = action_step.action

					#a = self.policy_runner(**{'0/discount':tf.constant(0.0), '0/observation': ob.astype(np.float32), '0/reward':tf.constant(0.0), '0/step_type':tf.constant(0)})['action'][0]

					step_size = self.rl_config_json["step_size"]
					self.action_sequence[uav_idx] = [a for _ in range(step_size)]

			for uav_idx in range(self.n_uavs):
				action = self.action_sequence[uav_idx].pop(0)
				viable_actions = self.action_space.get_viable_actions(self.state, uav_idx)
				if not(action in viable_actions):
					self.action_overrides += 1
					action = self.action_space.get_random_action(self.state, self.state_history, uav_idx)
				actions[uav_idx] = action
	


		elif self.policy == 'rl':
			# Get the RL action for each UAV
			assert input_actions != None
			actions = input_actions
			for uav_idx, action in actions.items():
				viable_actions = self.action_space.get_viable_actions(self.state, uav_idx)
				if not(action in viable_actions):
					self.action_overrides += 1
					action = self.action_space.get_random_action(self.state, self.state_history, uav_idx)
				actions[uav_idx] = action
		
		elif (self.policy == 'greedy_mock_covariance' or self.policy == 'greedy' or 
				self.policy == 'greedy_n_step' or self.policy == 'greedy_with_true_error' 
					or self.policy == 'greedy_n_step_divide_&_conquer' or self.policy == "greedy_joint_n_step" or
						self.policy == 'greedy_n_step_kriging' or  self.policy == 'greedy_n_step_divide_&_conquer_kriging' or
						self.policy == 'entropy_kriging'):
			
			# TODO: Still using measurements from a single UAV for a single user
			if self.policy == 'greedy_mock_covariance':
				# Do random search for highet uncertainty
				if step_idx < 30:
					# Get a random action for each UAV
					for uav_idx in range(self.n_uavs):
						action = self.action_space.get_random_action(self.state, self.state_history, uav_idx)
						actions[uav_idx] = action
				else:
					# If any UAV needs its path updated, update the prediction
					if any([len(self.action_sequence[uav_idx]) == 0 for uav_idx in range(self.n_uavs)]):
						raw_value_pred, sd, ob, covar, pred, uav_location_grid, _ = self.predict_ss(user_idx =0, no_full_covariance = False)
						
					for uav_idx in range(self.n_uavs):
						if len(self.action_sequence[uav_idx]) == 0:	
							self.action_sequence[uav_idx] = self.action_space.n_step_random_search(self.state, uav_idx, covar, sd, self.building_grid > self.flying_altitude, )
					for uav_idx in range(self.n_uavs):
						action = self.action_sequence[uav_idx].pop(0)
						actions[uav_idx] = action
			elif self.policy == 'greedy_with_true_error':
				raise ValueError("Not extended to multiple UAVs")
				error = np.square(raw_value_pred - self.ss_grids[0])
				sd = error
				if len(self.action_sequence[uav_idx]) == 0:
					self.action_sequence[uav_idx] = self.action_space.n_step_greedy(self.state, uav_idx, sd, self.building_grid > self.flying_altitude, pred, self.ss_grids[0])
				action = self.action_sequence[uav_idx].pop(0)
			
			

			elif self.policy in ['random_kriging','greedy_n_step', 
					'greedy_n_step_kriging', 'greedy_n_step_divide_&_conquer', 'greedy_n_step_divide_&_conquer_kriging', 'entropy_kriging']:
				if (step_idx < 0) and self.policy != 'entropy_kriging':
					# Get a random action for each UAV
					for uav_idx in range(self.n_uavs):
						action = self.action_space.get_random_action(self.state, self.state_history, uav_idx)
						actions[uav_idx] = action
				else:
					# If any UAV needs its path updated, update the prediction
					if any([len(self.action_sequence[uav_idx]) == 0 for uav_idx in range(self.n_uavs)]):
						
						if self.policy in ['greedy_n_step_kriging', 'greedy_n_step_divide_&_conquer_kriging', 'entropy_kriging', 'random_kriging']:
							krig_pred, krig_var, covar_matrix, covar_matrix_for_single_point = self.predict_ss_kriging(user_idx=0)
							krig_pred[self.building_grid > self.flying_altitude] = -250
							krig_var[self.building_grid > self.flying_altitude] = 0
							if (render or 1 ) and 0:
								# Render Kriging prediction and compare it to the Deep learning approach
								raw_value_pred, sd, ob, covar, pred, uav_location_grid,  true_norm_gain, uav_location_grids_per_uav, _ = self.predict_ss(user_idx =0, no_full_covariance = False)
								plot_grid(krig_pred, title = "krig_pred %d" % step_idx )
								plot_grid(covar_matrix_for_single_point, title = "krig_pred_covar %d" % step_idx )
								plot_grid(covar_matrix[2000, :].reshape(krig_pred.shape), title = "krig_pred_covar %d" % step_idx, spacing=1 )
								plot_grid(krig_var, title = "Kriging uncertainty", spacing=1)
								plot_grid(raw_value_pred, title = "raw_value_pred")
								plot_grid(sd / np.max(sd), title = "Proposed uncertainty", spacing=1)
								plot_grid(self.ss_grids[0], title = "true")
								plt.show()
							sd = np.abs(krig_var)
							pred = krig_pred
						else:
							raw_value_pred, sd, ob, covar, pred, uav_location_grid, _, _, _ = self.predict_ss(user_idx =0, no_full_covariance = False)
							


						if self.policy in ['greedy_n_step', 'greedy_n_step_kriging', 'greedy_n_step_divide_&_conquer', 'greedy_n_step_divide_&_conquer_kriging']:
							for uav_idx in range(self.n_uavs):
								if any([len(self.action_sequence[uav_idx]) == 0 for uav_idx in range(self.n_uavs)]):
									# For divide and conquer, divide the space amongst UAVs
									if (self.n_uavs > 1) and 0:
										closest_uav_idx, uav_locs = divide_area_among_uavs(self.state)
										exclude_states = closest_uav_idx != uav_idx
									else:
										exclude_states = []
								if len(self.action_sequence[uav_idx]) == 0:	
									if self.policy in ['greedy_n_step_kriging', 'greedy_n_step_divide_&_conquer_kriging']:
										planning_steps=min(self.planning_steps, self.max_episode_length - step_idx+1)
										used_steps=self.used_steps
									else:
										used_steps=self.used_steps
										planning_steps=min(self.planning_steps, self.max_episode_length - step_idx+1)
									# Find the paths for each UAV independently
									self.action_sequence[uav_idx], s_seq = self.action_space.n_step_greedy(self.state, uav_idx, sd**2, self.building_grid > self.flying_altitude, 
												pred, self.ss_grids[0], planning_steps=planning_steps,
														used_steps=used_steps, exclude_states=exclude_states)
									#sprint(len(self.action_sequence[uav_idx]))
						elif self.policy in ['entropy_kriging']:
							print("step_idx", step_idx)
							if any([len(self.action_sequence[uav_idx]) == 0 for uav_idx in range(self.n_uavs)]):
								stride_size = 8
								sequence_of_action_vectors, sequence_of_states = self.action_space.entropy_kriging(self.state, covar_matrix, self.building_grid > self.flying_altitude, 
										planning_steps=min(self.planning_steps, self.max_episode_length - step_idx+1),
										used_steps=self.used_steps,
										exclude_states_input = [], stride_size = stride_size)
								if stride_size == 1:
									for uav_idx in range(self.n_uavs):
										self.action_sequence[uav_idx] = [av[uav_idx] for av in sequence_of_action_vectors]
								else:
									for uav_idx in range(self.n_uavs):		
										sd_sub = np.copy(sd)
										sd_sub[self.building_grid > self.flying_altitude] = 0
										for i_s, s in enumerate(sequence_of_states):
											sd_sub[s[uav_idx*2], s[uav_idx*2+1]] = 1000 # Set to a large value
										# plot_grid(sd, spacing=1, title = "UAV %d" % uav_idx)
										# plot_grid(self.building_grid > self.flying_altitude, spacing=1, title = "UAV %d" % uav_idx)
										# print("Calculating subpaths since stride size is larger than 1...", self.planning_steps, len(sequence_of_states))
										self.action_sequence[uav_idx], s_seq = self.action_space.n_step_greedy(self.state, uav_idx, sd_sub, self.building_grid > self.flying_altitude, 
													pred, self.ss_grids[0], planning_steps=min(len(sequence_of_states)*stride_size, self.max_episode_length - step_idx+1),
															used_steps=min(len(sequence_of_states)*stride_size, self.max_episode_length - step_idx+1), exclude_states=[])		
										# print("Obtained next", len(self.action_sequence[uav_idx]), "actions")		
										plt.show()		
						else:
							raise ValueError("Invalid policy")
					# print(self.action_sequence)	
					# print(len(self.action_sequence[0]), len(self.action_sequence[1]))			
					for uav_idx in range(self.n_uavs):
						action = self.action_sequence[uav_idx].pop(0)
						# print(action)
						actions[uav_idx] = action		


		else:
			raise ValueError('Invalid policy.')

		# Update the states of UAVs based on the actions
		for uav_idx, action in actions.items():
			self.state.update(self.action_space, action, uav_idx, self.building_grid > self.flying_altitude)

		# State history per UAV
		for uav_idx in range(self.n_uavs):
			if not(uav_idx in self.state_history.keys()):
				self.state_history[uav_idx] = []
				self.state_history[uav_idx].append(np.copy(self.state.coordinates[uav_idx, :]))
			else:
				self.state_history[uav_idx].append(np.copy(self.state.coordinates[uav_idx, :]))


	def get_predict_error(self, raw_value_pred, user_idx = 0, median_error = False):
		# Find the mean error
		mean_error = np.abs(raw_value_pred - self.ss_grids[user_idx])
		indoor = self.building_grid > self.flying_altitude
		outdoor = self.building_grid <= self.flying_altitude
		mean_error[indoor] = 0
		if median_error:
			raise ValueError("Median error obsolete")
			mean_error = np.median(mean_error[outdoor])
		else:
			avg_mean_error = np.sum(mean_error) / np.sum(outdoor)
		

		return avg_mean_error, mean_error
	
	def get_measured_coords(self, uav_idx = None):
		measured_coords = []
		if uav_idx is None:
			for uav_idx in range(self.n_uavs):
				measured_coords = measured_coords + [tuple(s) for s in self.state_history[uav_idx]]
		else:
			measured_coords = measured_coords + [tuple(s) for s in self.state_history[uav_idx]]			
		measured_coords = list(set(measured_coords))
		measured_coords = np.asarray(measured_coords, dtype = np.int32)
		measured_coords[:,[1,0]] = measured_coords[:,[0,1]]
		return measured_coords 
	
	def run(self, n_steps = 200, render = False):
		for step_idx in range(n_steps):
			try:
				self.step(step_idx, render)
			except Exception as e:
				print(step_idx)
				print(self.action_sequence)
				print(e)
				raise ValueError("CRASHED")
		# Make the prediction
		raw_value_pred, sd, ob, covar, pred, uav_location_grid, true_norm_gain, uav_location_grids_per_uav, _ = self.predict_ss(user_idx =0, no_full_covariance = True)
		var_grid = sd**2
		if self.policy in ['greedy_n_step_kriging', 'greedy_n_step_divide_&_conquer_kriging', 'entropy_kriging', 'random_kriging']:
			print("Using Kriging interpolation.")
			krig_pred, krig_var, _, _ = self.predict_ss_kriging(user_idx=0)
			raw_value_pred = krig_pred
			var_grid = krig_var


		# Find the mean error
		mean_error, errors = self.get_predict_error(raw_value_pred, user_idx=0)

		# Measured coords
		measured_coords = self.get_measured_coords()
		
		if render:
			self.render(0)
			plot_grid(ob[:, :, 0], title = 'Observation 1')
			plot_grid(ob[:, :, 1], title = 'Observation 2')
			plot_grid(raw_value_pred, title = 'Mean')
			plot_grid(sd, title = 'SD', spacing=1)



		user_idx = 0
		return mean_error, measured_coords, errors, self.ss_grids[user_idx], self.ds.scale_var_to_raw(var_grid)

	def predict_ss_for_many_paths(self, user_idx, path_continuations):
		city_ob = self.building_grid
		pwr_ob = self.ss_grids[user_idx]
		indoor = self.building_grid > self.flying_altitude

		# Get measured coords up to this point
		measured_coords = self.get_measured_coords()

		obs = []
		for path in path_continuations:
			new_measured_coords = []
			new_measured_coords = new_measured_coords + [tuple(s) for s in path]
			new_measured_coords = list(set(new_measured_coords))
			new_measured_coords = np.asarray(new_measured_coords, dtype = np.int32)
			new_measured_coords[:,[1,0]] = new_measured_coords[:,[0,1]]		

			new_measured_coords = np.concatenate([measured_coords, new_measured_coords], axis = 0)


			# Mask the observations based on measured locations
			ob = self.ds.get_obs_from_meas_and_grid(new_measured_coords,  pwr_ob, city_ob)

			obs.append(ob)

		# Normalized true grid
		true_norm_gain = (pwr_ob - self.ds.MIN_PWR) / (self.ds.MAX_PWR - self.ds.MIN_PWR)
		
		# Predict the signal strength using the observation
		predictions = self.predictor.forward_pass(np.asarray(obs, dtype=np.float32)).numpy()

		# Get the errors
		errors_per_path = np.abs(predictions-true_norm_gain.reshape(predictions.shape[1:]))
		errors_per_path = np.mean(errors_per_path, axis = 1)
		errors_per_path = np.mean(errors_per_path, axis = 1)
		errors_per_path = np.mean(errors_per_path, axis = 1).flatten()
		assert len(errors_per_path) == len(path_continuations)
		
		return np.argmin(errors_per_path)
		

	def predict_ss_kriging(self, user_idx = 0):
		pwr_ob = self.ss_grids[user_idx]

		# Get measured coords
		measured_coords = self.get_measured_coords()

		# Use Kriging predictor 
		raw_value_pred, variance, covar_matrix, covar_matrix_for_single_point = self.kriging_predictor.predict_signal_strength(measured_coords, true_pwr_grid = pwr_ob)
		
		return raw_value_pred, variance, covar_matrix, covar_matrix_for_single_point

	def predict_ss(self, user_idx =0, no_full_covariance = False):
		city_ob = self.building_grid
		pwr_ob = self.ss_grids[user_idx]
		indoor = self.building_grid > self.flying_altitude

		# Get measured coords
		measured_coords = self.get_measured_coords()
		n_measured_coords = measured_coords.shape[0]

		# UAV location grid for all UAVs
		uav_location_grid = np.zeros(city_ob.shape)
		# UAV location grid per UAV
		uav_location_grids_per_uav = []
		for uav_idx in range(self.n_uavs):
			mc = self.get_measured_coords(uav_idx=uav_idx)
			uav_location_grid_per_uav = np.zeros(city_ob.shape)
			uav_location_grid_per_uav[mc[-1,0], mc[-1,1]] = 1
			uav_location_grid[mc[-1,0], mc[-1,1]] = 1
			uav_location_grids_per_uav.append(uav_location_grid_per_uav)



		# Mask the observations based on measured locations
		ob = self.ds.get_obs_from_meas_and_grid(measured_coords,  pwr_ob, city_ob)

		# Normalized true grid
		true_norm_gain = (pwr_ob - self.ds.MIN_PWR) / (self.ds.MAX_PWR - self.ds.MIN_PWR)
		
		# Predict the signal strength and variance using the observation
		predictions = self.predictor.forward_pass(np.asarray([ob], dtype=np.float32)).numpy()
		raw_value_pred = self.ds.scaled_to_raw(predictions[0].copy()).reshape(predictions[0].shape[0:-1])
		pred = (predictions[0].copy()).reshape(predictions[0].shape[0:-1])
		pred[indoor] = 0
		variance = self.predictor.get_variance(np.asarray([ob], dtype=np.float32))
		sd = np.sqrt(variance).reshape(predictions[0].shape[0:-1])



		#sd = self.ds.scale_sd_to_raw(sd).reshape(predictions[0].shape[0:-1])
		#sd = np.minimum(sd, np.ones(sd.shape) * 300)
		# sd[0:4, :] = 0
		# sd[sd.shape[0]-4:sd.shape[0], :] = 0
		# sd[:, 0:4] = 0
		# sd[:, sd.shape[1]-4:sd.shape[1]] = 0

		# For measured coordinates set sd to 0 
		sd[measured_coords[:, 0], measured_coords[:, 1]] = 0
		sd[indoor] = 0

		# For indoor and measured coordinates set gain to the true gain 
		raw_value_pred[indoor] = -250
		raw_value_pred[measured_coords[:, 0], measured_coords[:, 1]] = pwr_ob[measured_coords[:, 0], measured_coords[:, 1]]
		# Predict the covariance
		if not(no_full_covariance):
			covar = self.predictor.get_covariance(np.asarray([ob])).numpy()
			covar = covar.reshape(covar.shape[1:])
		else:
			covar = None


		# Find the squared error

		return raw_value_pred, sd, ob, covar, pred, uav_location_grid, true_norm_gain, uav_location_grids_per_uav, n_measured_coords

	def render(self, user_idx, measured_coords = []):
		self.fig1, (self.ax1, self.ax2) = plt.subplots(2, 1)
		if len(measured_coords) == 0:
			measured_coords = self.get_measured_coords()

		# Location of the ground user
		indmax = np.argmax(self.ss_grids[user_idx])
		coord = np.unravel_index(indmax, self.ss_grids[0].shape)
		coord = [coord[1], coord[0]]

		# Building grid
		im1 = self.ax1.imshow(self.building_grid, cmap='Pastel2')


		self.ax1.scatter(measured_coords[:,1], measured_coords[:,0], c='r', s=5)
		for uav_idx in range(self.n_uavs):
			self.ax1.scatter([self.state_history[uav_idx][0][0]], [self.state_history[uav_idx][0][1]], c='g', s=10)


		self.ax1.scatter([coord[0]], [coord[1]], c='black', s=10)
		self.ax1.set(xlabel='x (m)', ylabel='y (m)',
		  title='Trajectory')
		self.ax1.set_xticks(np.arange(self.building_grid.shape[1], step = 50))
		self.ax1.set_xticklabels(np.arange(self.building_grid.shape[1], step = 50)*self.grid_len)
		self.ax1.set_yticklabels(np.arange(self.building_grid.shape[0], step = 20)*self.grid_len)
		self.ax1.set_yticks(np.arange(self.building_grid.shape[0], step = 20))
		cbar1 = self.ax1.figure.colorbar(im1, ax=self.ax1, cmap='Pastel2', fraction=0.046, pad=0.04)
		cbar1.ax.set_ylabel('Height (m)', rotation=-90, va="bottom")

		# Power grid
		im2 = self.ax2.imshow(self.ss_grids[user_idx], cmap='BuPu')
		self.ax2.scatter(measured_coords[:,1], measured_coords[:,0], c='r', s=5)
		for uav_idx in range(self.n_uavs):
			self.ax2.scatter([self.state_history[uav_idx][0][0]], [self.state_history[uav_idx][0][1]], c='g', s=10)
		self.ax2.scatter([coord[0]], [coord[1]], marker = '^', c='blue', s=40)
		self.ax2.set(xlabel='x (m)', ylabel='y (m)')
		self.ax2.set_xticks(np.arange(self.building_grid.shape[1], step = 50))
		self.ax2.set_xticklabels(np.arange(self.building_grid.shape[1], step = 50)*self.grid_len)
		self.ax2.set_yticklabels(np.arange(self.building_grid.shape[0], step = 20)*self.grid_len)
		self.ax2.set_yticks(np.arange(self.building_grid.shape[0], step = 20))
		cbar2 = self.ax2.figure.colorbar(im2, ax=self.ax2, cmap='BuPu', fraction=0.046, pad=0.04)
		cbar2.ax.set_ylabel('Signal strength (dBm)', rotation=-90, va="bottom")



if __name__ == '__main__':

	mc_iters = 1
	mean_errors = np.zeros((mc_iters, 2))
	ee = ExplorationEnvironment()
	for i, policy in enumerate(['random', 'greedy']):
		print('policy:', policy)
		for j in range(mc_iters):
			ee.policy = policy
			mean_error = ee.run()
			ee.reset()
			mean_errors[j, i] = mean_error

	print(np.mean(mean_errors, axis = 0))

	np.save('entropy.np', mean_errors)

	plt.show()
