import numpy as np
import random
from utils import get_session, plot_grid, plot_confusion_matrix
import pandas as pd
from matplotlib import pyplot as plt
from predictor import Predictor, models
import itertools
from multiprocessing import Pool

class PredictorWrapper:
	def __init__(self, model_file, ds):
		config_file = "/home/enesk/repos/spatial-prediction/logs/config-%s.config" % model_file
		config = pd.read_csv(config_file, sep = '\t').to_dict(orient = 'records')
		model_name = ([str(record['value']) for record in config if record['parameter'] == 'model'][0])
		regression = ([str(record['value']) for record in config if record['parameter'] == 'regression'][0]) == 'True'
		probabilistic = ([str(record['value']) for record in config if record['parameter'] == 'probabilistic'][0]) == 'True'
		no_maps = ([str(record['value']) for record in config if record['parameter'] == 'no_maps'][0]) == 'True'
		random_walk = ([str(record['value']) for record in config if record['parameter'] == 'random_walk'][0]) == 'True'
		try:
			model_scale = ([int(record['value']) for record in config if record['parameter'] == 'model_scale'][0])
		except IndexError:
			model_scale = 1
		try:
			mc_dropout = ([str(record['value']) for record in config if record['parameter'] == 'mc_dropout'][0]) == 'True'
		except IndexError:
			mc_dropout = False

		session = get_session()

		model_checkpoint = "/home/enesk/repos/spatial-prediction/models/model-%s.cpkt" % model_file

		self.ds = ds

		self.predictor = Predictor(
				ds = ds,
				model = models[model_name],
				session = session,
				model_checkpoint = model_checkpoint,
				session_name = 'test',
				model_scale = model_scale,
				random_walk = random_walk,
				regression = regression,
				probabilistic = probabilistic,
				mc_dropout = mc_dropout,
				test = True
				)

	def predict_ss(self, state_history, uav_idx, city_ob, pwr_ob, outdoor):
		measured_coordinates = [s for s in state_history[uav_idx]]
		measured_coordinates = np.asarray(measured_coordinates, dtype = np.int)
		ob = self.ds.get_obs_from_meas_and_grid(measured_coordinates,  pwr_ob, city_ob)
		predictions = self.predictor.forward_pass([ob])
		raw_value_pred = self.ds.scaled_to_raw(predictions[0]).reshape(predictions[0].shape[0:-1])
		variance = self.predictor.get_variance([ob])
		sd = np.sqrt(variance)
		sd = self.ds.scale_sd_to_raw(sd).reshape(predictions[0].shape[0:-1])
		sd = np.minimum(sd, np.ones(sd.shape) * 100)
		sd[measured_coordinates[:, 0], measured_coordinates[:, 1]] = 0
		sd[outdoor] = 0
		raw_value_pred[outdoor] = -250
		return raw_value_pred, sd


class ActionSpace:
	def __init__(self, n_uavs, flying_altitude, repeat_probability):
		self.n_uavs = n_uavs
		self.size = 4
		self.step_size = 1
		self.repeat_probability = repeat_probability
		self.flying_altitude = flying_altitude

	def get_highest_sd_action_2(self, state, state_history, sd, uav_idx):

		if random.random() <= 0.0:
			return self.get_random_action(state, uav_idx)

		# Collect visited coordinates for this UAV
		measured_coords = [s for s in state_history[uav_idx]]
		measured_coords = np.asarray(measured_coords, dtype = np.int32)

		best_action = -1
		highest_sd = -np.inf
		pass_fail = []
		must_repeat_action = False
		while (1):
			for action in range(self.size):
				dx, dy = self.get_displacement(action)
				new_coordinates = np.asarray([state.coordinates[uav_idx, 0] + dx, state.coordinates[uav_idx, 1] + dy])
				if new_coordinates[0] < 0 or new_coordinates[0] > (state.xsize-1):
					pass_fail.append("fail1")
					pass
				elif new_coordinates[1] < 0 or new_coordinates[1] > (state.ysize-1):
					pass_fail.append("fail2")
					pass
				elif state.building_grid[new_coordinates[1], new_coordinates[0]] > self.flying_altitude :
					pass_fail.append("fail3")
					pass
				elif not(must_repeat_action) and (np.min(np.sum(np.abs(measured_coords - new_coordinates), axis = -1)) == 0): 
					# We visited this state
					pass_fail.append("fail4")
					action_to_visited_state = action
					pass
				elif sd[new_coordinates[1], new_coordinates[0]] > highest_sd:
					highest_sd = sd[new_coordinates[1], new_coordinates[0]]
					best_action = action

			if best_action == -1:
				# Must repeat action
				must_repeat_action = True
			else:
				# We found a non-repeated action
				break
		return best_action


	def get_highest_sd_action(self, state, state_history, sd, uav_idx):

		if random.random() <= 0.0:
			return self.get_random_action(state, uav_idx)
		
		# Location with highest SD
		indmax = np.argmax(sd)
		coord = np.unravel_index(indmax, sd.shape)
		#coord = [coord[1], coord[0]]

		# Collect visited coordinates for this UAV
		measured_coords = [s for s in state_history[uav_idx]]
		measured_coords = np.asarray(measured_coords, dtype = np.int32)

		best_action = -1
		lowest_distance = np.inf
		pass_fail = []
		must_repeat_action = False
		while (1):
			for action in range(self.n):
				dx, dy = self.get_displacement(action)
				new_coordinates = np.asarray([state.coordinates[uav_idx, 0] + dx, state.coordinates[uav_idx, 1] + dy])
				if new_coordinates[0] < 0 or new_coordinates[0] > (state.xsize-1):
					pass_fail.append("fail1")
					pass
				elif new_coordinates[1] < 0 or new_coordinates[1] > (state.ysize-1):
					pass_fail.append("fail2")
					pass
				elif state.building_grid[new_coordinates[1], new_coordinates[0]] > self.flying_altitude :
					pass_fail.append("fail3")
					pass
				elif not(must_repeat_action) and np.min(np.sum(np.abs(measured_coords - new_coordinates), axis = -1)) == 0: 
					# We visited this state
					pass_fail.append("fail4")
					action_to_visited_state = action
					pass
				elif np.linalg.norm(np.asarray([new_coordinates[1], new_coordinates[0]]) - np.asarray(coord)) < lowest_distance:
					pass_fail.append("fail5")
					lowest_distance = np.linalg.norm(np.asarray([new_coordinates[1], new_coordinates[0]]) - np.asarray(coord))
					best_action = action

			if best_action == -1:
				# Must repeat action
				must_repeat_action = True
			else:
				# We found a non-repeated action
				break

		return best_action


	def get_random_paths(self, env_state, uav_idx, n_paths, path_length, stride_size = 1):
		states_per_path = []
		actions_per_path = []
		nn = 0
		while (nn < n_paths):
			state = [env_state.coordinates[uav_idx, 0], env_state.coordinates[uav_idx, 1]]
			last_action = env_state.last_actions[uav_idx]
			actions_on_this_path = []
			states_on_this_path = [tuple(state),]
			# It is impossible to move from certain starting locations given some stride size
			invalid_starting_location = False
			for ll in range(path_length // stride_size + 1):

				possible_actions = list(range(0, self.size))
				random.shuffle(possible_actions)

				# Repeat last action at random
				if (random.random() < (self.repeat_probability/stride_size)) and (last_action != -1):
					action = last_action
					possible_actions = [action, ] + possible_actions

				# Search over random actions and check which one is valid
				for action in possible_actions:
					new_coordinates = [state[0], state[1]]
					resulting_new_states = []
					dx, dy = self.get_displacement(action)
					valid = True
					for rr in range(stride_size):
						new_coordinates = np.asarray([new_coordinates[0] + dx, new_coordinates[1] + dy])
						resulting_new_states.append((new_coordinates[0], new_coordinates[1]))
						if new_coordinates[0] < 0 or new_coordinates[0] > (env_state.xsize-1):
							valid = False
						elif new_coordinates[1] < 0 or new_coordinates[1] > (env_state.ysize-1):
							valid = False
						elif env_state.building_grid[new_coordinates[1], new_coordinates[0]] > self.flying_altitude :
							valid = False
					if valid:
						break
				
				if valid == False:
					v = env_state.building_grid.copy()
					print(states_on_this_path)
					v[state[1], state[0]] = -10
					#plot_grid(v, spacing=1)
					#plt.show()
					print(state)
					print("How can this happen?")
					#stride_size = stride_size - 1
					return [], []
					invalid_starting_location = True


				last_action = action
				state = new_coordinates	
				for rr in range(stride_size):			
					actions_on_this_path.append(action)
				states_on_this_path = states_on_this_path + resulting_new_states

			states_per_path.append(states_on_this_path[0:path_length])
			actions_per_path.append(actions_on_this_path[0:path_length])
			nn += 1
			

		return states_per_path, actions_per_path	



	def get_random_action(self, state, state_history, uav_idx):
		possible_actions = list(range(0, self.size))
		pass_fail = []
		random.shuffle(possible_actions)

		# Repeat last action at random
		if random.random() < self.repeat_probability and state.last_actions[uav_idx] != -1:
			action = state.last_actions[uav_idx]
			possible_actions = [action, ] + possible_actions

		# Collect visited coordinates for this UAV
		measured_coords = [s for s in state_history[uav_idx]]
		measured_coords = np.asarray(measured_coords, dtype = np.int32)

		for action in possible_actions:
			dx, dy = self.get_displacement(action)
			new_coordinates = np.asarray([state.coordinates[uav_idx, 0] + dx, state.coordinates[uav_idx, 1] + dy])
			if new_coordinates[0] < 0 or new_coordinates[0] > (state.xsize-1):
				pass_fail.append("fail1")
				pass
			elif new_coordinates[1] < 0 or new_coordinates[1] > (state.ysize-1):
				pass_fail.append("fail2")
				pass
			elif state.building_grid[new_coordinates[1], new_coordinates[0]] > self.flying_altitude :
				pass_fail.append("fail3")
				pass
			# elif np.min(np.sum(np.abs(measured_coords - new_coordinates), axis = -1)) == 0: 
			# 	# We visited this state
			# 	pass_fail.append("fail4")
			# 	action_to_visited_state = action
			# 	pass
			else:
				return action

		# We have to go back to a state we visited since other actions are not an option
		return action
	
		raise ValueError("Could not find a viable random action", print(pass_fail), state.coordinates[uav_idx])


	def get_viable_actions(self, state, uav_idx):
		# Returns a one-hot vector denoting if an action is viable or not
		viable_actions = []
		for action in range(self.size):
			dx, dy = self.get_displacement(action)
			if (state.coordinates[uav_idx, 0] + dx) < 0 or (state.coordinates[uav_idx, 0] + dx) > (state.xsize-1):
				continue
			elif (state.coordinates[uav_idx, 1] + dy) < 0 or (state.coordinates[uav_idx, 1] + dy) > (state.ysize-1):
				continue
			elif state.building_grid[state.coordinates[uav_idx, 1] + dy, state.coordinates[uav_idx, 0] + dx] > self.flying_altitude :
				continue
			viable_actions.append(action)
		return viable_actions
	

	def n_step_random_search(self, state, uav_idx, covar, sd, indoor):
		planning_steps = 100
		used_steps = 20

		# Current location
		uav_coordinates = state.coordinates[uav_idx, :]

		# Calculate the distance grid
		distance_grid = np.zeros(sd.shape)
		y_coords, x_coords = np.meshgrid(np.arange(sd.shape[0]), np.arange(sd.shape[1]))
		distance_to_current_loc = np.abs((y_coords - uav_coordinates[1])) + np.abs((x_coords - uav_coordinates[0]))
		distance_grid[y_coords, x_coords] = distance_to_current_loc

		# Initial state
		inital_state = (uav_coordinates[1], uav_coordinates[0])

		# Highest entropy
		highest_det = -np.inf
		
		# Number of random paths we will search
		for mc in range(2000):
			#print("Random path", mc)
			
			path = [inital_state, ]
			actions = []
			state = inital_state

			# Value iteration
			previous_action = -1
			for steps in range(planning_steps):
				
				us = [0,1,2,3]
				random.shuffle(us)
				completed = False
				
				for ii in range(2):
					
					for u in us:

						# Get random action 
						if (np.random.rand() < 0.5) and previous_action != -1 and ii==0:
							u = previous_action
						dx, dy = self.get_displacement(u)

						# Get new state
						new_state = (state[0]+dy, state[1]+dx)



						if (new_state[0] < 0) or (new_state[1] < 0) or (new_state[0] >= sd.shape[0]) or (new_state[1] >= sd.shape[1]):
							# Invalid previous state
							continue

						# Ensure the state is reachable
						if (indoor[new_state]):
							continue

						# Enforce moving away from the starting location
						if (distance_grid[new_state] < distance_grid[state]) and ii==0:
							continue
						


						state = new_state
						path.append(state)
						actions.append(u)
						previous_action = u
						completed = True
						break
					if completed:
						break
						
			
			# Find the entropy
			new_path = ([p[0] for p in path], [p[1] for p in path])
			# previous_path = (list(measured_coords[:, 0]), list(measured_coords[:, 1]))
			combined_path = (new_path[0], new_path[1])
			path_state_indices = np.ravel_multi_index(combined_path, dims = sd.shape)
			path_mask = np.ones(covar.shape[0]) == 0 # This will take care of repeated states
			path_mask[path_state_indices] = True
			path_covar = covar[path_mask, :][:, path_mask]
			#non_path = [kk for kk in range(covar.shape[0]) if not(kk in path_state_indices)]
			# non_path_mask = np.ones(covar.shape[0]) == 1
			# non_path_mask[path_state_indices] = False
			# non_path_covar = covar[non_path_mask, :][:, non_path_mask]
			#print(path_covar.shape, non_path_covar.shape, len(np.unique(path_state_indices)))
			det = self.mvn_entropy(path_covar)
			if det > highest_det:
				highest_det = det
				optimal_action_sequence = actions

		
		return optimal_action_sequence[0:used_steps]

	def mvn_entropy(self, covar):
		# a = 0.5 * np.log(np.linalg.det(covar))
		# b = (1+np.log(2*np.pi)) * covar.shape[0] / 2
		return np.linalg.det(covar)

	def ravel_state(self, set_of_states, grid_shape):
		"""
		set_of_states: a tuple of tuples per each agent
		grid_shape: shape of the grid on which agents move
		"""
		assert type(set_of_states) == tuple
		set_of_states = np.asarray(set_of_states)
		raveled_states = np.ravel_multi_index((set_of_states[:, 0], set_of_states[:, 1]), grid_shape)
		return raveled_states


	def unravel_state(self, set_of_ravel_states, grid_shape):
		"""
		set_of_ravel_states: a tuple of ints per each agent
		grid_shape: shape of the grid on which agents move
		"""
		set_of_ravel_states = list(set_of_ravel_states)
		return [np.unravel_index(s, grid_shape) for s in set_of_ravel_states]


	def forward_value_iteration_multiagent(self, initial_state, distance_grids, all_states, all_states_per_uav, covar_matrix, uav_coordinates, exclude_states, planning_steps, stride_size):	
		# Movement grid shape
		grid_shape = distance_grids[0].shape
		assert len(grid_shape) == 2

		# Initialize the value of starting state to 0 and inf for others
		values = np.ones([np.prod(grid_shape) for _ in range( uav_coordinates.shape[0] )]) * np.inf
		initial_raveled_state = self.ravel_state(initial_state, distance_grids[0].shape)
		values[tuple(initial_raveled_state)]= 0

		# All actions
		achievable_actions_per_uav = [[0,1,2,3] for _ in range( uav_coordinates.shape[0] )]
		all_actions = [x for x in  itertools.product(  *achievable_actions_per_uav)]


		# Optimal actions for each state 
		optimal_u = {}

		# Use this to store the final optimal path - used for plotting
		optimal_path = []
#


		# Value iteration
		for ttt in range(planning_steps+3):
			print("ttt", ttt)
			for sss, state in enumerate(all_states):
				#print(sss, "/", len(all_states))
				raveled_state = self.ravel_state(state, distance_grids[0].shape)
				for action_vector in all_actions:
					# Get to previous state
					prev_state = list(state) + []
					for temp_uav_idx in range(uav_coordinates.shape[0]):
						# Get the previous state for this action
						dx, dy = self.get_displacement(action_vector[temp_uav_idx])
						# Invert the action
						dx = -dx * stride_size
						dy = -dy * stride_size
						prev_state[temp_uav_idx] = (state[temp_uav_idx][0]+dy, state[temp_uav_idx][1]+dx)
					prev_state = tuple(prev_state)
					

					# Skip invalid states
					invalid_prev_state = False
					for uav_idx in range(uav_coordinates.shape[0]):
						if (prev_state[uav_idx][0] < 0) or (prev_state[uav_idx][1] < 0) or (prev_state[uav_idx][0] >= grid_shape[0]) or (prev_state[uav_idx][1] >= grid_shape[1]):
							invalid_prev_state = True
							break
					if invalid_prev_state:
						continue
					
					# Ravel state from tuple to index
					raveled_prev_state = self.ravel_state(prev_state, grid_shape)
					
					
					# Update value and steps count 
					# We add a negative constant in case all SDs on a 
					# particular map have 0 value, then the longer path is still better
					new_value = values[tuple(raveled_prev_state)] - 1
					covar_curr_state = covar_matrix[np.ix_(raveled_prev_state, raveled_prev_state)]
					new_value = new_value - (np.linalg.det(covar_curr_state))
					#print("new_value", new_value)

					
					# Directed acyclical graph constraint
					for uav_idx in range(uav_coordinates.shape[0]):
						if distance_grids[uav_idx][prev_state[uav_idx]] > distance_grids[uav_idx][state[uav_idx]]:
							new_value = np.infty

					# If the new value is better, store it		
					if (new_value < values[tuple(raveled_state)]):
						values[tuple(raveled_state)] = new_value
						optimal_u[state] = action_vector



		# Find and optionnally plot the optimal path
		end_point = np.unravel_index(values.argmin(), values.shape)
		end_point = tuple(self.unravel_state(end_point, grid_shape))
		optimal_action_sequence = []
		state = end_point
		while(1):
			try:
				action_vector = optimal_u[state] 
			except:
				print("optimal_u", optimal_u)
				plot_grid(distance_grids[0])
				raise ValueError("STOP")
			# Add to the optimal action sequence
			for _ in range(stride_size):
				optimal_action_sequence.insert(0, action_vector)

			# Find the previous state on this path
			prev_state = list(state)
			for uav_idx in range(uav_coordinates.shape[0]):
				dx, dy = self.get_displacement(action_vector[uav_idx])
				# Invert the action
				dx = -dx  * stride_size 
				dy = -dy  * stride_size
				
				prev_state[uav_idx] = (state[uav_idx][0]+dy, state[uav_idx][1]+dx)

				if (prev_state[uav_idx][0] < 0) or (prev_state[uav_idx][1] < 0) or (prev_state[uav_idx][0] >= grid_shape[0]) or (prev_state[uav_idx][1] >= grid_shape[1]):
					# Invalid previous state
					raise ValueError("Somehow reached invalid state")
			prev_state = tuple(prev_state)
			state = prev_state
			optimal_path.append(state)
			if (state == initial_state):
				break

		# plot_grid(optimal_path)
		return optimal_action_sequence
	
	def forward_value_iteration_multiagent_parallel(self, initial_state_idx, distance_grids, all_states, all_states_per_uav, covar_matrix, uav_coordinates, exclude_states, planning_steps, stride_size):	
		# Movement grid shape
		grid_shape = distance_grids[0].shape
		assert len(grid_shape) == 2

		# Entropy at current states
		entropy = np.zeros((len(all_states), 1))
		for ss, state in enumerate(all_states):
			# Ravel state from tuple to index
			raveled_state = self.ravel_state(state, grid_shape)

			# Get covar for this state
			covar_curr_state = covar_matrix[np.ix_(raveled_state, raveled_state)]
			entropy[ss] = (np.linalg.det(covar_curr_state))	

		# All states np
		all_states_np = np.asarray(all_states)
		all_states_np = all_states_np.reshape((len(all_states), -1))

		# Initialize the value of starting state to 0 and inf for others
		# Initialize the value of starting state to 0 and inf for others
		value_shape = []
		for _ in range( uav_coordinates.shape[0] ):
			value_shape.append(grid_shape[0] // stride_size)
			value_shape.append(grid_shape[1] // stride_size)
		values = np.ones(value_shape) * np.inf
		values[tuple(all_states_np[initial_state_idx, :] // stride_size)]= 0



		# All actions
		achievable_actions_per_uav = [[0,1,2,3] for _ in range( uav_coordinates.shape[0] )]
		all_actions = [x for x in  itertools.product(  *achievable_actions_per_uav)]



		# Previous states np
		previous_states_np = np.ones(all_states_np.shape+(len(all_actions),), dtype=np.int32) 
		for a_idx, action_vector in enumerate(all_actions):
			all_states_np_cp = all_states_np.copy()
			
			for temp_uav_idx in range(uav_coordinates.shape[0]):
				# Get the previous state for this action
				dx, dy = self.get_displacement(action_vector[temp_uav_idx])
				# Invert the action
				dx = -dx * stride_size
				dy = -dy * stride_size
				all_states_np_cp[:, temp_uav_idx*2 + 0] += dy	
				all_states_np_cp[:, temp_uav_idx*2 + 1] += dx		
			previous_states_np[:,  :, a_idx] = all_states_np_cp

		# Find invalid previous states based on whether they left the grid
		invalid_previous_states =  np.zeros((all_states_np.shape[0], len(all_actions)), dtype=np.bool) 
		invalid_coords = np.logical_or(previous_states_np < 0,  previous_states_np >= grid_shape[0])
		invalid_previous_states[:, :] = np.any(invalid_coords, axis = 1)

		# Clip the previous states
		previous_states_np = np.minimum(previous_states_np, grid_shape[0]-1)
		previous_states_np = np.maximum(previous_states_np, 0)

		# Distance from the start at the current state
		distance_current_state = np.zeros((all_states_np.shape[0], uav_coordinates.shape[0])) 
		for temp_uav_idx in range(uav_coordinates.shape[0]):
			distance_current_state[:,temp_uav_idx] += distance_grids[temp_uav_idx][all_states_np[:, temp_uav_idx*2], all_states_np[:, temp_uav_idx*2+1]]


		# Distance from the start at the previous state
		distance_prev_state = np.zeros((all_states_np.shape[0], uav_coordinates.shape[0], len(all_actions))) 
		for a_idx, action_vector in enumerate(all_actions):
			for temp_uav_idx in range(uav_coordinates.shape[0]):
				distance_prev_state[:,temp_uav_idx,a_idx] += distance_grids[temp_uav_idx][previous_states_np[:, temp_uav_idx*2 + 0, a_idx], previous_states_np[:, temp_uav_idx*2 + 1, a_idx]]

		# Find invalid states based on whether they are colliding with buildings or other reasons 
		for a_idx, action_vector in enumerate(all_actions):
			for temp_uav_idx in range(uav_coordinates.shape[0]):
				invalid_previous_states[:, a_idx] = np.logical_or(invalid_previous_states[:, a_idx], 
						      	exclude_states[temp_uav_idx][previous_states_np[:, temp_uav_idx*2 + 0, a_idx], previous_states_np[:, temp_uav_idx*2 + 1, a_idx]])

		# Find invalid current states based on whether they are colliding with buildings or other reasons
		invalid_current_states =  np.zeros((all_states_np.shape[0], 1), dtype=np.bool)
		for temp_uav_idx in range(uav_coordinates.shape[0]):
			invalid_current_states[:, 0] = np.logical_or(invalid_current_states[:, 0], 
							exclude_states[temp_uav_idx][all_states_np[:, temp_uav_idx*2 + 0], all_states_np[:, temp_uav_idx*2 + 1]])


		# Expand invalid states based on whether the UAVs are moving away from agent
		for temp_uav_idx in range(uav_coordinates.shape[0]):
			invalid_previous_states  = np.logical_or(invalid_previous_states,  
					    (distance_prev_state[:,temp_uav_idx,:] > distance_current_state[:,temp_uav_idx].reshape((-1,1))).reshape(invalid_previous_states.shape))



		# Optimal previous actions for each state
		optimal_actions =  np.ones((all_states_np.shape[0], 1)) * (-1)


		# Value iteration
		for ttt in range(planning_steps//stride_size+stride_size):
			# Values from previous state
			value_from_prev_state = np.zeros((previous_states_np.shape[0], previous_states_np.shape[-1]))
			for u, action_vector in enumerate(all_actions):
				if uav_coordinates.shape[0] == 1:
					# Hardcode this for now since numpy is hard
					value_from_prev_state[:, u] = values[previous_states_np[:, 0, u] // stride_size, previous_states_np[:, 1, u] // stride_size, ]
				elif uav_coordinates.shape[0] == 2:
					# Hardcode this for now since numpy is hard
					value_from_prev_state[:, u] = values[previous_states_np[:, 0, u] // stride_size, previous_states_np[:, 1, u] // stride_size, 
					  						previous_states_np[:, 2, u] // stride_size, previous_states_np[:, 3, u] // stride_size]
				elif uav_coordinates.shape[0] == 3:
					# Hardcode this for now since numpy is hard
					value_from_prev_state[:, u] = values[previous_states_np[:, 0, u] // stride_size, previous_states_np[:, 1, u] // stride_size, 
					  						previous_states_np[:, 2, u] // stride_size, previous_states_np[:, 3, u] // stride_size,
											previous_states_np[:, 4, u] // stride_size, previous_states_np[:, 5, u] // stride_size,]
				else:
					raise ValueError("Invalid number of UAVs")
			

			
			# Can't transition from illegal states
			value_from_prev_state[invalid_previous_states] = np.inf


			

			# Update values and optimal acions
			new_values_at_curr_states = (value_from_prev_state - entropy - 1)
			optimal_actions[:, 0] = np.argmin(new_values_at_curr_states, axis = -1)


			# Out of bound locations should remain inf
			new_values_at_curr_states = np.min(new_values_at_curr_states, axis = -1)
			new_values_at_curr_states[invalid_current_states[:, 0]] = np.inf		
			optimal_actions[invalid_current_states[:, 0]] = np.inf


			
			# The minimum is there to avoid change the value of the start state to inf
			if uav_coordinates.shape[0] == 1:
				values[all_states_np[:, 0] // stride_size, all_states_np[:, 1] // stride_size] =  np.minimum(
								values[all_states_np[:, 0] // stride_size, all_states_np[:, 1] // stride_size], 
							    new_values_at_curr_states) 
			elif uav_coordinates.shape[0] == 2:
				values[all_states_np[:, 0] // stride_size, all_states_np[:, 1] // stride_size, all_states_np[:, 2] // stride_size, all_states_np[:, 3] // stride_size] =  np.minimum(
								values[all_states_np[:, 0] // stride_size, all_states_np[:, 1] // stride_size, all_states_np[:, 2] // stride_size, all_states_np[:, 3] // stride_size], 
							    new_values_at_curr_states) 
			elif uav_coordinates.shape[0] == 3:
				values[all_states_np[:, 0] // stride_size, all_states_np[:, 1] // stride_size, all_states_np[:, 2] // stride_size, all_states_np[:, 3] // stride_size, all_states_np[:, 4] // stride_size, all_states_np[:, 5] // stride_size] =  np.minimum(
								values[all_states_np[:, 0] // stride_size, all_states_np[:, 1] // stride_size, all_states_np[:, 2] // stride_size, all_states_np[:, 3] // stride_size, all_states_np[:, 4] // stride_size, all_states_np[:, 5] // stride_size], 
							    new_values_at_curr_states) 
			else:
				raise ValueError("Invalid number of UAVs")
			
			#### FOR DEBUGGGING #####
			# print(previous_states_np[list(range(previous_states_np.shape[0])), :, optimal_actions.flatten().astype(np.int32)])
			# print(all_states_np)
			# print(values[all_states_np[:, 0], all_states_np[:, 1], all_states_np[:, 2], all_states_np[:, 3]])
			# print(all_states_np[values[all_states_np[:, 0], all_states_np[:, 1], all_states_np[:, 2], all_states_np[:, 3]] < np.inf, :][:],  
	 		# 	np.sum(values[all_states_np[:, 0], all_states_np[:, 1], all_states_np[:, 2], all_states_np[:, 3]] < np.inf))			
			#### FOR DEBUGGGING #####
	

		# Find and optionally plot the optimal path	
		end_point = np.unravel_index(np.argmin(values, axis=None), values.shape)
		# Upscale by stride size
		end_point = tuple(np.asarray(end_point)*stride_size)
		optimal_action_sequence = []
		state = end_point
		optimal_path = [state]

		#### FOR DEBUGGGING ####
		values_on_path = []
		distances_on_path = []
		invalid_previous = []
		#### FOR DEBUGGGING #####

		# Find the ending location
		state_np = np.asarray(state)
		dist = np.abs(all_states_np - state_np)
		dist = np.linalg.norm(dist.reshape((dist.shape[0], -1)), axis = -1, ord=1)
		state_idx = np.argmin(dist)
		

		ii = 0
		while(state_idx != initial_state_idx):
			#### FOR DEBUGGGING #####
			values_on_path.insert(0,values[tuple(all_states_np[state_idx, :] // stride_size)]/ 100000)
			distances_on_path.insert(0,tuple(distance_current_state[state_idx, :]))
			invalid_previous.insert(0, invalid_previous_states[state_idx, int(optimal_actions[state_idx][0])])
			assert np.sum(all_states_np[state_idx, :] - state_np) == 0
			#### FOR DEBUGGGING #####

			# Get the optimal action
			try:
				action_idx = int(optimal_actions[state_idx][0])
				action_vector = all_actions[action_idx]
			except Exception as e:
				print(e)
				raise ValueError("STOP")
			
			# Add to the optimal action sequence
			for _ in range(stride_size):
				optimal_action_sequence.insert(0, action_vector)

			# Find the previous state on this path
			prev_state = list(state)
			for uav_idx in range(uav_coordinates.shape[0]):
				dx, dy = self.get_displacement(action_vector[uav_idx])
				# Invert the action
				dx = -dx  * stride_size 
				dy = -dy  * stride_size
				
				prev_state[uav_idx*2] = state[uav_idx*2]+dy
				prev_state[uav_idx*2+1] = state[uav_idx*2+1]+dx

				#### FOR DEBUGGGING #####
				if ((prev_state[uav_idx*2] < 0) or (prev_state[uav_idx*2+1] < 0) or (prev_state[uav_idx*2] >= grid_shape[0]) or (prev_state[uav_idx*2+1] >= grid_shape[1]) or
					exclude_states[uav_idx][prev_state[uav_idx*2], prev_state[uav_idx*2+1]]):
					# Invalid previous state
					for uav_idx in range(uav_coordinates.shape[0]):
						es = exclude_states[uav_idx]
						es[all_states[initial_state_idx][uav_idx][0], all_states[initial_state_idx][uav_idx][1]] = 5
						for st in optimal_path:
							es[st[uav_idx*2], st[uav_idx*2+1]] = -5
						plot_grid(exclude_states[uav_idx], spacing=1, title = "Starting location for UAV %d" % uav_idx)
					print("start location", all_states_np[initial_state_idx, :], values[tuple(all_states_np[initial_state_idx, :] // stride_size)])
					print("values", values_on_path)
					print("optimal_path", optimal_path)
					print("distances_on_path", distances_on_path)
					print("invalid previous", invalid_previous)
					raise ValueError("Somehow reached invalid state", prev_state)
				#### FOR DEBUGGGING #####
			
			prev_state = tuple(prev_state)
			state = prev_state
			optimal_path.insert(0, state)

			# Find the next location index
			state_np = np.asarray(state)
			dist = np.abs(all_states_np - state_np)
			dist = np.linalg.norm(dist.reshape((dist.shape[0], -1)), axis = -1, ord=1)
			state_idx = np.argmin(dist)

			
			#### FOR DEBUGGGING #####
			ii = ii + 1
			if (ii > 2*planning_steps) or 0:
				# Could not find path to starting state
				for uav_idx in range(uav_coordinates.shape[0]):
					es = exclude_states[uav_idx]
					es[all_states[initial_state_idx][uav_idx][0], all_states[initial_state_idx][uav_idx][1]] = 5
					for st in optimal_path:
						es[st[uav_idx*2], st[uav_idx*2+1]] = -5
					print("start location", all_states_np[initial_state_idx, :], values[tuple(all_states_np[initial_state_idx, :] // stride_size)])
					print("values", values_on_path)
					print("optimal_path", optimal_path)
					print("distances_on_path", distances_on_path)
					print("invalid previous", invalid_previous)
					plot_grid(exclude_states[uav_idx], spacing=1, title = "Starting location for UAV %d" % uav_idx)
					plot_grid(distance_grids[uav_idx], spacing=1, title = "Distance to starting location for UAV %d" % uav_idx)
				raise ValueError("Could not find path to start state")
			#### FOR DEBUGGGING #####

		return optimal_action_sequence, optimal_path


	def entropy_kriging(self, env_state, covar_matrix, indoor, planning_steps, used_steps, exclude_states_input = [], stride_size = 4):

		print("planning steps for entropy kriging", planning_steps)



		# Achievable states per UAV
		achievable_states_per_uav = []
		exclude_states_per_uav = []
		# Measure the distance in terms of the number of discrete steps
		# Distance is measured in numpy array coordinates
		distance_grids= []
		for uav_idx in range(env_state.n_uavs):
			uav_coordinates = env_state.coordinates[uav_idx, :]
			distance_grid = np.zeros(indoor.shape)
			y_coords, x_coords = np.meshgrid(np.arange(indoor.shape[0]), np.arange(indoor.shape[1]))
			distance_to_current_loc = np.abs((y_coords - uav_coordinates[1])) + np.abs((x_coords - uav_coordinates[0]))
			distance_grid[y_coords, x_coords] = distance_to_current_loc

			# Exclude these states from value iterations
			if len(exclude_states_input) == 0:
				exclude_states = (distance_grid > (planning_steps//stride_size+2)*stride_size) | (indoor)
			else:
				raise ValueError("Not implemented")
				exclude_states = exclude_states | (distance_grid > planning_steps) | (indoor)

			achievable_states_per_this_uav = []
			exclude_states_per_uav.append(exclude_states)

			for iii in range(indoor.shape[0]):
				for jjj in range(indoor.shape[1]):
					# If stride size is greater than 1, exclude certain states
					if not(exclude_states[iii, jjj]) and (iii % stride_size == 0) and (jjj % stride_size == 0):
						achievable_states_per_this_uav.append((iii, jjj))

			achievable_states_per_uav.append(achievable_states_per_this_uav)
			distance_grids.append(distance_grid)

		
		# All possible reachable states
		# It's a product of reachable states for each UAV
		all_states = [x for x in  itertools.product(  *achievable_states_per_uav )]

		
		# Current UAV coordinates
		all_uav_coordinates = env_state.coordinates

		# Initial state
		initial_state = ()
		for uav_idx in range(all_uav_coordinates.shape[0]):
			initial_state = initial_state + ((all_uav_coordinates[uav_idx, 1], all_uav_coordinates[uav_idx, 0]), )
			
		# Round the locations of the UAV for stride bigger than 1
		# And also find the starting location idx
		# Measure the distance in terms of the number of discrete steps if stride size is larger than 1
		distance_grids= []
		initial_states_np = np.asarray(initial_state)
		all_states_np  = np.asarray(all_states)
		try:
			dist = np.abs(all_states_np - initial_states_np)
		except:
			print(all_states_np, initial_states_np)
			raise ValueError("STOP")
		dist = np.linalg.norm(dist.reshape((dist.shape[0], -1)), axis = -1, ord=1)
		initial_state_idx = np.argmin(dist)
		initial_state = all_states[initial_state_idx]
		distance_grids= []
		for uav_idx in range(env_state.n_uavs):
			uav_coordinates = env_state.coordinates[uav_idx, :]
			distance_grid = np.zeros(indoor.shape)
			y_coords, x_coords = np.meshgrid(np.arange(indoor.shape[0]), np.arange(indoor.shape[1]))
			# Distance is measured in numpy array coordinates
			distance_to_current_loc = np.abs((y_coords - initial_state[uav_idx][0])) + np.abs((x_coords - initial_state[uav_idx][1]))
			distance_grid[y_coords, x_coords] = distance_to_current_loc
			distance_grids.append(distance_grid)


		return self.forward_value_iteration_multiagent_parallel(initial_state_idx, distance_grids, all_states, achievable_states_per_uav, covar_matrix, all_uav_coordinates, exclude_states_per_uav, planning_steps, stride_size)






	def n_step_greedy_multiagent(self, env_state, sd, indoor, exclude_states_input = []):
		planning_steps = 3
		used_steps = 40
		
		
		# Measure the distance in terms of the number of discrete steps
		achievable_states_per_uav = []
		distance_grids= []
		for uav_idx in range(env_state.n_uavs):
			uav_coordinates = env_state.coordinates[uav_idx, :]
			distance_grid = np.zeros(sd.shape)
			y_coords, x_coords = np.meshgrid(np.arange(sd.shape[0]), np.arange(sd.shape[1]))
			distance_to_current_loc = np.abs((y_coords - uav_coordinates[1])) + np.abs((x_coords - uav_coordinates[0]))
			distance_grid[y_coords, x_coords] = distance_to_current_loc
			

			# Exclude these states from value iterations
			if len(exclude_states_input) == 0:
				exclude_states = (distance_grid > planning_steps) | (indoor)
			else:
				raise ValueError("Not implemented")
				exclude_states = exclude_states | (distance_grid > planning_steps) | (indoor)

			achievable_states_per_this_uav = []

			for iii in range(sd.shape[0]):
				for jjj in range(sd.shape[1]):
					if not(exclude_states[iii, jjj]):
						achievable_states_per_this_uav.append((iii, jjj))

			achievable_states_per_uav.append(achievable_states_per_this_uav)
			distance_grids.append(distance_grid)
		
		# All possible reachable states
		all_states = [x for x in  itertools.product(  *achievable_states_per_uav )]

		
		# Current UAV coordinates
		uav_coordinates = env_state.coordinates

		# Initial state
		initial_state = ()
		for uav_idx in range(uav_coordinates.shape[0]):
			initial_state = initial_state + ((uav_coordinates[uav_idx, 1], uav_coordinates[uav_idx, 0]), )
		

		def forward_value_iteration(initial_state, distance_grids, all_states, sd, uav_coordinates, exclude_states):			
			# Initialize the value of starting state to 0 and inf for others
			values = {}
			for state in all_states:
				values[state] = np.inf
			values[initial_state]= 0


			# Optimal actions for each state 
			optimal_u = {}

			# Use this to store the final optimal path - used for plotting
			optimal_path = []
#
			

			# Value iteration
			for ttt in range(planning_steps+10):
				for state in all_states:
					optimal_action = [0 for i in range(uav_coordinates.shape[0])]
					for uav_idx in range(uav_coordinates.shape[0]):
						for u in [0,1,2,3]:
							current_action = [] + optimal_action
							current_action[uav_idx] = u

							# Get to previous state
							prev_state = list(state)
							for temp_uav_idx in range(uav_coordinates.shape[0]):
								# Get the previous state for this action
								dx, dy = self.get_displacement(current_action[temp_uav_idx])
								# Invert the action
								dx = -dx  
								dy = -dy
								prev_state[temp_uav_idx] = (state[temp_uav_idx][0]+dy, state[temp_uav_idx][1]+dx)
							prev_state = tuple(prev_state)

							if (prev_state[uav_idx][0] < 0) or (prev_state[uav_idx][1] < 0) or (prev_state[uav_idx][0] >= sd.shape[0]) or (prev_state[uav_idx][1] >= sd.shape[1]):
								# Invalid previous state
								continue
							

							
							# Update value and steps count 
							# We add a negative constant in case all SDs on a 
							# particular map have 0 value, then the longer path is still better
							new_value = values[prev_state] - 1
							for uav_idx in range(uav_coordinates.shape[0]):
								new_value = new_value - sd[state[uav_idx]]  # Add 
							
							# Directed acyclical graph constraint
							if distance_grids[uav_idx][prev_state[uav_idx]] > distance_grids[uav_idx][state[uav_idx]]:
								new_value = np.infty

							# If the new value is better, store it		
							if (new_value < values[state]):
								values[state] = new_value
								optimal_u[state] = u



			# Find and optionnally plot the optimal path
			end_point = min(values, key=values.get)
			optimal_action_sequence = []
			state = end_point
			while(1):
				try:
					u = optimal_u[state] 
				except:
					print("state", state)
					print("optimal_u", optimal_u)
					plot_grid(distance_grid)
					plot_grid(sd)

					raise ValueError("STOP")
				optimal_action_sequence.insert(0, u)
				dx, dy = self.get_displacement(u)
				# Invert the action
				dx = -dx  
				dy = -dy
				prev_state = list(state)
				prev_state[uav_idx] = (state[uav_idx][0]+dy, state[uav_idx][1]+dx)
				if (prev_state[uav_idx][0] < 0) or (prev_state[uav_idx][1] < 0) or (prev_state[uav_idx][0] >= sd.shape[0]) or (prev_state[uav_idx][1] >= sd.shape[1]):
					# Invalid previous state
					continue
				prev_state = tuple(prev_state)
				state = prev_state
				optimal_path.append(state)
				if (state == initial_state):
					break

			# plot_grid(optimal_path)
			return optimal_action_sequence
		
		forward_value_iteration(initial_state, distance_grids, all_states, sd, uav_coordinates, exclude_states)


	def n_step_greedy(self, env_state, uav_idx, sd, indoor, pred, 
		   true_signal_strength, exclude_states = [], planning_steps = 100, used_steps = 40):

		
		# User location
		user_loc = np.unravel_index(np.argmax(true_signal_strength, axis=None), true_signal_strength.shape)

		
		# Measure the distance in terms of the number of discrete steps
		uav_coordinates = env_state.coordinates[uav_idx, :]
		initial_uav_coordinates = env_state.initial_coordinates[uav_idx, :]
		distance_grid = np.zeros(sd.shape)
		y_coords, x_coords = np.meshgrid(np.arange(sd.shape[0]), np.arange(sd.shape[1]))
		distance_to_current_loc = np.abs((y_coords - uav_coordinates[1])) + np.abs((x_coords - uav_coordinates[0]))
		distance_grid[y_coords, x_coords] = distance_to_current_loc
		
		distance_grid_to_initial = np.zeros(sd.shape)
		distance_to_initial_loc = np.abs((y_coords - initial_uav_coordinates[1])) + np.abs((x_coords - initial_uav_coordinates[0]))
		distance_grid_to_initial[y_coords, x_coords] = distance_to_initial_loc


		distance_grid_to_user = np.zeros(sd.shape)
		distance_to_user = np.abs((y_coords - user_loc[0])) + np.abs((x_coords - user_loc[1]))
		distance_grid_to_user[y_coords, x_coords] = distance_to_user

		# Exclude these states from value iterations
		if len(exclude_states) == 0:
			exclude_states = (distance_grid > planning_steps) | (indoor)
		else:
			exclude_states = exclude_states | (distance_grid > planning_steps) | (indoor)
			

		optimal_action_sequence, optimal_sequence_states = self.forward_value_iteration_parallel(planning_steps, distance_grid, distance_grid_to_initial, distance_grid_to_user, 
						    				sd, uav_coordinates, exclude_states)
		return optimal_action_sequence[0:used_steps], optimal_sequence_states[0:used_steps]


	def forward_value_iteration_parallel(self, planning_steps, distance_grid, distance_grid_to_initial, distance_grid_to_user, sd, uav_coordinates, exclude_states):
		# Define all possible states for this UAV 
		y_coords, x_coords = np.meshgrid(np.arange(sd.shape[0]), np.arange(sd.shape[1]))
		y_coords = y_coords.flatten()
		x_coords = x_coords.flatten()
		yx_coords = np.stack([y_coords, x_coords], axis = -1)
		all_states = list(map(tuple, yx_coords))
		
		# Store the lowest value obtained for each state
		values = np.ones(sd.shape) * np.inf
		values[uav_coordinates[1], uav_coordinates[0]] = 10

		# Optimal actions for each state 
		optimal_u = np.ones(sd.shape) * (-1)
			

		# Value iteration
		all_states = np.asarray(all_states, dtype=np.int16)
		displacements = np.zeros(all_states.shape + (4,), dtype=np.int16)
		for u in [0,1,2,3]:
			dx, dy = self.get_displacement(u)
			# Invert the action
			dx = -dx  
			dy = -dy
			displacements[:, 0, u] = dy
			displacements[:, 1, u] = dx
		
		# Initialize common matrices
		# Only do this once
		prev_states = np.expand_dims(all_states, axis = -1) + displacements
		clipped_prev_states = np.minimum(sd.shape[0]-1, prev_states)
		clipped_prev_states = np.maximum(0, clipped_prev_states)
		excluded_prev_states = (np.any(prev_states < 0, axis = 1)) | (np.any(prev_states >= sd.shape[1], axis = 1))
		distance_at_curr_state = np.zeros((prev_states.shape[0], prev_states.shape[-1]))
		distance_at_prev_state = np.zeros((prev_states.shape[0], prev_states.shape[-1]))
		sds_at_current_state = np.zeros((prev_states.shape[0], prev_states.shape[-1]))
		for u in [0,1,2,3]:
			distance_at_prev_state[:, u] = distance_grid[clipped_prev_states[:, 0, u], clipped_prev_states[:, 1, u]]
		distance_at_curr_state[:, :] = np.expand_dims(distance_grid[all_states[:, 0], all_states[:, 1]], axis = -1)
		sds_at_current_state[:, :] = np.expand_dims(sd[all_states[:, 0], all_states[:, 1]], axis = -1)
				
		for ttt in range(planning_steps+10):
			# if ttt % 10 == 0:
			# 	plot_grid(values)
			# 	plot_grid(sd)
			# 	plt.show()



			# Initialize common matrices
			value_from_prev_state = np.zeros((prev_states.shape[0], prev_states.shape[-1]))
			


			for u in [0,1,2,3]:
				value_from_prev_state[:, u] = values[clipped_prev_states[:, 0, u], clipped_prev_states[:, 1, u]]

			
			# Can't transition from illegal states
			value_from_prev_state[excluded_prev_states] = np.inf

			# Set value to inf if moving closer to the start
			value_from_prev_state[distance_at_prev_state > distance_at_curr_state] = np.inf

			# Update values and optimal acions
			new_values_at_curr_states = value_from_prev_state - sds_at_current_state - 1
			optimal_u_prev = np.argmin(new_values_at_curr_states, axis = -1)
			optimal_u[all_states[:, 0], all_states[:, 1]] = optimal_u_prev
			# The minimum is there to avoid change the value of the start state to inf
			values[all_states[:, 0], all_states[:, 1]] =  np.minimum(values[all_states[:, 0], all_states[:, 1]], 
							    np.min(new_values_at_curr_states, axis = -1)) 

			# Out of bound locations should remain inf
			values[exclude_states] = np.inf			

	

		end_point = np.unravel_index(np.argmin(values, axis=None), values.shape)
		optimal_action_sequence = []
		state = end_point
		optimal_sequence_states = [state,]
		
		# print("Exited while loop")
		# plot_grid(values, spacing=1)
		# plot_grid(optimal_u, spacing=1)
		# plot_grid(distance_grid, spacing=1)
		# plt.show()
		# plot_grid(sd)
		# plot_grid(values, spacing=1)
		# plt.show()
		optimal_path = np.zeros(values.shape)
		iii = 0
		while(1):
			if exclude_states[state] == 1:
				print("values[state]", values[state])
				plot_grid(values, spacing = 1)
				plot_grid(sd, spacing = 1)
				plot_grid(exclude_states, spacing = 1)
				plot_grid(distance_grid, spacing=1)
				raise ValueError("STOP")
				
			optimal_path[state] = 1
			u = optimal_u[state] 
			optimal_action_sequence.insert(0, u)
			dx, dy = self.get_displacement(u)
			# Invert the action
			dx = -dx  
			dy = -dy
			prev_state = (state[0]+dy, state[1]+dx)
			state = prev_state
			optimal_sequence_states.insert(0, state)
			if (state[0] == uav_coordinates[1]) and (state[1] == uav_coordinates[0]):
				break
			iii = iii + 1
			# if iii > 30:
			# 	plot_grid(optimal_path)
			# 	raise ValueError("STOP")
				



		return optimal_action_sequence, optimal_sequence_states




	def forward_value_iteration(self, planning_steps, distance_grid, distance_grid_to_initial, distance_grid_to_user, sd, uav_coordinates, exclude_states):
		# Define all possible states for this UAV 
		y_coords, x_coords = np.meshgrid(np.arange(sd.shape[0]), np.arange(sd.shape[1]))
		y_coords = y_coords.flatten()
		x_coords = x_coords.flatten()
		yx_coords = np.stack([y_coords, x_coords], axis = -1)
		all_states = list(map(tuple, yx_coords))
		
		# Store the lowest value obtained for each state
		values = np.ones(sd.shape) 

		# Optimal actions for each state 
		optimal_u = {}

		# Use this to store the step count used to reach this state
		steps_count = np.ones(sd.shape) * np.infty

		# Use this to store the final optimal path - used for plotting
		optimal_path = np.ones(sd.shape) * np.infty
		
		# Initialize the costs
		for state in all_states:
			# Initialize the value of starting state to 0 and inf for others
			if (state[0] == uav_coordinates[1]) and (state[1] == uav_coordinates[0]):
				values[state] = 0
				steps_count[state] = 0
			else:
				values[state] = np.infty
				steps_count[state] = np.infty
			
			# Used for plotting purposes
			#v = np.copy(values)
		#v[v > 10] = None
		# plot_grid(v)

		# Value iteration
		for ttt in range(planning_steps+10):
			for state in all_states:
				# If this state is unreachable, we don't need to update it
				if (exclude_states[state]):							
					continue

				for u in [0,1,2,3]:
					# Get the previous state for this action
					dx, dy = self.get_displacement(u)
					# Invert the action
					dx = -dx  
					dy = -dy
					prev_state = (state[0]+dy, state[1]+dx)
					if (prev_state[0] < 0) or (prev_state[1] < 0) or (prev_state[0] >= sd.shape[0]) or (prev_state[1] >= sd.shape[1]):
						# Invalid previous state
						continue
					
					# Update value and steps count 
					# We add a negative constant in case all SDs on a 
					# particular map have 0 value, then the longer path is still better
					new_value = values[prev_state] - sd[state] - 1 # Add 
					new_steps_count = steps_count[prev_state] + 1
					
					# Directed acyclical graph constraint
					if distance_grid[prev_state] > distance_grid[state]:
						new_value = np.infty

					# If the new value is better, store it		
					if (new_value < values[state]):
						values[state] = new_value
						steps_count[state] = new_steps_count
						optimal_u[state] = u

			# For plotting
			#v = np.copy(values)
			#v[v > 10] = None
			# plot_grid(v)
		# plot_grid(v)
		# plot_grid(distance_grid)
		# plot_grid(sd)

		# Find and optionnally plot the optimal path
		v2 = np.copy(values)
		#values[steps_count<planning_steps] = np.inf  #"We only want the full paths"
		end_point = np.unravel_index(np.argmin(values, axis=None), values.shape)
		optimal_path[end_point] = 0
		optimal_action_sequence = []
		state = end_point
		while(1):
			try:
				u = optimal_u[state] 
			except:
				print(state)
				print(optimal_u)
				print(values[state])
				print(indoor[state])
				# For plotting
				v = np.copy(values)
				#v[v > 10] = None
				plot_grid(v2, title="v2")
				plot_grid(v, title="v")
				plot_grid(distance_grid)
				plot_grid(distance_grid_to_user)
				plot_grid(sd)
				plot_grid(true_signal_strength)

				raise ValueError("STOP")
			optimal_action_sequence.insert(0, u)
			dx, dy = self.get_displacement(u)
			# Invert the action
			dx = -dx  
			dy = -dy
			prev_state = (state[0]+dy, state[1]+dx)
			state = prev_state
			optimal_path[state] = 0
			if (state[0] == uav_coordinates[1]) and (state[1] == uav_coordinates[0]):
				break

		optimal_path[optimal_path > 10] = None
		# plot_grid(optimal_path)
		return optimal_action_sequence



	def get_displacement(self, action):
		if action == 0:
			# UP
			dx = 0; dy = -self.step_size
		elif action == 1:
			# DOWN
			dx = 0; dy = self.step_size
		elif action == 2:
			# LEFT
			dx = -self.step_size; dy = 0
		elif action == 3:
			# RIGHT
			dx = self.step_size; dy = 0
		else:
			raise ValueError('Ilegal action: ', action)

		return dx, dy


	

class EnviornmentState:
	def __init__(self, flying_altitude, building_grid, sinr_grids, user_locations,
	      			max_episode_length, n_uavs, n_users, initial_coordinates = None, uav_starting_proximity = False):
		self.xsize = sinr_grids[0].shape[1] # All the sinr grids will have the same shape
		self.ysize = sinr_grids[0].shape[0]
		self.n_uavs = n_uavs
		self.n_users = n_users
		self.flying_altitude = flying_altitude
		self.max_episode_length = max_episode_length
		self.user_association = np.arange(n_users) # The index of this list corresponds to the index of the UAV
		self.coordinates = np.zeros((n_uavs, 2), dtype = np.int) # Coordinates for each UAV in the environment
		self.snr_estimation_errors = {}
		self.uav_starting_proximity = uav_starting_proximity

		# Randomize the starting locations of the UAVs
		if initial_coordinates is None:
			for i in range(n_uavs):
				while(1):
					# Generate a state that is located in a valid location
					coordinates = [random.randint(0, self.xsize-1), random.randint(0, self.ysize-1)]
					# TODO: Make sure that we are selecting the right building grid
					if building_grid[coordinates[1], coordinates[0]] <=  self.flying_altitude: # Means there is no building here
						#if any(sinr_grids[:, coordinates[1], coordinates[0]] > (SINR_OF_AN_INACCESSIBLE_FIELD)): # Means that the UAV has some reception
						#	if all(sinr_grids[:, coordinates[1], coordinates[0]] < STOPPING_SINR): # Means that the UAV will have to move towards stopping SINR

						if i == 0:
							break
						elif self.uav_starting_proximity:
							if (abs(coordinates[0] - self.coordinates[0, 0])+abs(coordinates[1] - self.coordinates[0, 1]))<10:
								break
						else:
							break

				self.coordinates[i, :] = coordinates
		else:
			self.coordinates = np.copy(initial_coordinates)
			
		self.n_steps = np.zeros(n_uavs, dtype = np.int) # Each UAV counts its own number of updates
		self.last_actions = np.ones(n_uavs) * (-1) # Use -1 to denote there were no prior actions
		self.initial_coordinates = np.copy(self.coordinates)
		self.sinr_grids = sinr_grids
		self.building_grid = building_grid
		self.user_locations = user_locations


	def update(self, action_space, action, uav_idx, indoor):
		dx, dy = action_space.get_displacement(action)
		viable_actions = action_space.get_viable_actions(self, uav_idx)
		self.last_actions[uav_idx] = action

		# Update the coordinates

		self.coordinates[uav_idx, 0] = self.coordinates[uav_idx, 0] + dx
		self.coordinates[uav_idx,1] = self.coordinates[uav_idx, 1] + dy

		# Make sure there is no building collision
		if indoor[self.coordinates[uav_idx,1], self.coordinates[uav_idx,0]]:
			raise ValueError("Building collision")


def divide_area_among_uavs(env_state : EnviornmentState):
	
	# Measure the distance in terms of the number of discrete steps
	area_shape = env_state.building_grid.shape
	distance_grids = np.zeros((area_shape[0], area_shape[1], env_state.n_uavs))
	uav_coords_so_far = []
	for uav_idx in range(env_state.n_uavs):
		uav_coordinates = env_state.coordinates[uav_idx, :]
		

		
		distance_grid = np.zeros(area_shape)
		y_coords, x_coords = np.meshgrid(np.arange(area_shape[0]), np.arange(area_shape[1]))
		distance_to_current_loc = np.square((y_coords - uav_coordinates[1])) + np.square((x_coords - uav_coordinates[0]))
		distance_grid[y_coords, x_coords] = distance_to_current_loc

		# We have to do this in case two or more UAVs are at the same location
		if (uav_coordinates[0], uav_coordinates[1]) in uav_coords_so_far:
			slicing_direction =  np.random.randint(0, 2)
			if slicing_direction == 0:
				distance_grid[y_coords[y_coords < uav_coordinates[1]], x_coords[y_coords < uav_coordinates[1]]] -= 1
			else:
				distance_grid[y_coords[x_coords < uav_coordinates[0]], x_coords[x_coords < uav_coordinates[0]]] -= 1



		distance_grids[:, :, uav_idx] = distance_grid
		uav_coords_so_far.append((uav_coordinates[0], uav_coordinates[1]))


	closest_uav_idx = np.argmin(distance_grids, axis = 2)

	return closest_uav_idx, None

class ObservationSpace:
	def __init__(self, shape):
		self.shape = shape
