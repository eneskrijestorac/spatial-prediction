from data_set import Dataset
from predictor import Predictor, models
from predictor_utils import PiecewiseSchedule, OptimizerSpec
from utils import get_session, plot_grid, plot_confusion_matrix
import os
import pandas as pd
import numpy as np
import pandas as pd



def compare_model_to_benchmark(model_name, start, stop, n_points, n_samples, save_file, benchmark = False, force_random_walk = False, kriging = False):
    session_name = model_name
    meas_rates = np.linspace(start, stop, n_points)
    comparsion_data = {}
    comparsion_data['model'] = []
    comparsion_data['model_sd'] = []
    comparsion_data['benchmark'] = []
    comparsion_data['benchmark_sd'] = []
    comparsion_data['meas_rate'] = []

    config_file = "/home/enesk/repos/spatial-prediction/logs/config-%s.config" % session_name
    config = pd.read_csv(config_file, sep = '\t').to_dict(orient = 'records')
    model_name = ([str(record['value']) for record in config if record['parameter'] == 'model'][0])
    regression = ([str(record['value']) for record in config if record['parameter'] == 'regression'][0]) == 'True'
    probabilistic = ([str(record['value']) for record in config if record['parameter'] == 'probabilistic'][0]) == 'True'
    no_maps = ([str(record['value']) for record in config if record['parameter'] == 'no_maps'][0]) == 'True'
    random_walk = ([str(record['value']) for record in config if record['parameter'] == 'random_walk'][0]) == 'True' or force_random_walk
    try:
        model_scale = ([int(record['value']) for record in config if record['parameter'] == 'model_scale'][0])
    except IndexError:
        model_scale = 1
    try:
        mc_dropout = ([str(record['value']) for record in config if record['parameter'] == 'mc_dropout'][0]) == 'True'
    except IndexError:
        mc_dropout = False

    session = get_session()

    model_checkpoint = "/home/enesk/repos/spatial-prediction/models/model-%s.cpkt" % session_name


    ds = Dataset(measured_rate = 0.001, no_maps = no_maps)

    predictor = Predictor(
            ds = ds,
            model = models[model_name],
            session = session,
            model_checkpoint = model_checkpoint,
            session_name = session_name,
            model_scale = model_scale,
            random_walk = random_walk,
            regression = regression,
            probabilistic = probabilistic,
            mc_dropout = mc_dropout,
            test = True
            )



    for meas_rate in meas_rates:
        print('Evaluating for meas rate:', meas_rate)

        ds.set_meas_rate(meas_rate)


        if benchmark or kriging:
            print('Evaluating benchmark.')
            obs, labels, outdoors, non_scaled_maps, reconstruct_means, reconstruct_probs = ds.sample_whole_maps(n_samples, train = False,
                                            include_non_scaled  = True, include_reconstruct = benchmark, random_walk = random_walk, kriging = kriging)
        else:
            print('Evaluating model.')
            obs, labels, outdoors, non_scaled_maps = ds.sample_whole_maps(n_samples, train = False,
                                            include_non_scaled  = True, include_reconstruct = benchmark, random_walk = random_walk)
        predictions = predictor.forward_pass(obs)


        soft_loss = np.zeros((n_samples, 2))

        for i in range(n_samples):
            raw_value_pred = ds.scaled_to_raw(predictions[i]).reshape(predictions[i].shape[0:-1])
            soft_loss[i, 0] = np.sum(np.abs(raw_value_pred - non_scaled_maps[i][0]) * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / (np.sum(outdoors[i]))
            if benchmark or kriging:
                soft_loss[i, 1] = np.sum(np.abs(reconstruct_means[i] - non_scaled_maps[i][0]) * outdoors[i].reshape(non_scaled_maps[i][0].shape))  / (np.sum(outdoors[i]))
        print('')
        comparsion_data['model'].append(np.mean(soft_loss, axis = 0)[0])
        comparsion_data['model_sd'].append(np.std(soft_loss, axis = 0)[0])

        comparsion_data['benchmark'].append(np.mean(soft_loss, axis = 0)[1])
        comparsion_data['benchmark_sd'].append(np.std(soft_loss, axis = 0)[1])
        comparsion_data['meas_rate'].append(meas_rate)

    df = pd.DataFrame.from_dict(comparsion_data)
    df.to_csv('figures/comparsion_data_%s.csv' % save_file, mode = 'w', index=False, header = True)


def calculate_goodness(model_name, start, stop, n_points, n_samples, save_file, benchmark = False, force_random_walk = False, kriging = False):
    session_name = model_name
    meas_rates = np.linspace(start, stop, n_points)
    comparsion_data = {}
    comparsion_data['meas_rate'] = []
    comparsion_data['pred_within_ci'] = []
    comparsion_data['sd'] = []
    comparsion_data['loglikelihood'] = []
    comparsion_data['goodness'] = []
    comparsion_data['krig_pred_within_ci'] = []
    comparsion_data['krig_sd'] = []
    comparsion_data['krig_loglikelihood'] = []
    comparsion_data['krig_goodness'] = []

    config_file = "/home/enesk/repos/spatial-prediction/logs/config-%s.config" % session_name
    config = pd.read_csv(config_file, sep = '\t').to_dict(orient = 'records')
    model_name = ([str(record['value']) for record in config if record['parameter'] == 'model'][0])
    regression = ([str(record['value']) for record in config if record['parameter'] == 'regression'][0]) == 'True'
    probabilistic = ([str(record['value']) for record in config if record['parameter'] == 'probabilistic'][0]) == 'True'
    no_maps = ([str(record['value']) for record in config if record['parameter'] == 'no_maps'][0]) == 'True'
    random_walk = ([str(record['value']) for record in config if record['parameter'] == 'random_walk'][0]) == 'True' or force_random_walk
    try:
        model_scale = ([int(record['value']) for record in config if record['parameter'] == 'model_scale'][0])
    except IndexError:
        model_scale = 1
    try:
        mc_dropout = ([str(record['value']) for record in config if record['parameter'] == 'mc_dropout'][0]) == 'True'
    except IndexError:
        mc_dropout = False

    session = get_session()

    model_checkpoint = "/home/enesk/repos/spatial-prediction/models/model-%s.cpkt" % session_name


    ds = Dataset(measured_rate = 0.001, no_maps = no_maps)

    predictor = Predictor(
            ds = ds,
            model = models[model_name],
            session = session,
            model_checkpoint = model_checkpoint,
            session_name = session_name,
            model_scale = model_scale,
            random_walk = random_walk,
            regression = regression,
            probabilistic = probabilistic,
            mc_dropout = mc_dropout,
            test = True
            )



    for meas_rate in meas_rates:
        print('Evaluating for meas rate:', meas_rate)

        ds.set_meas_rate(meas_rate)


        if benchmark or kriging:
            print('Evaluating benchmark.')
            obs, labels, outdoors, non_scaled_maps, reconstruct_means, reconstruct_vars = ds.sample_whole_maps(n_samples, train = False,
                                            include_non_scaled  = True, include_reconstruct = benchmark, random_walk = random_walk, kriging = kriging)
        else:
            print('Evaluating model.')
            obs, labels, outdoors, non_scaled_maps = ds.sample_whole_maps(n_samples, train = False,
                                            include_non_scaled  = True, include_reconstruct = benchmark, random_walk = random_walk)
        predictions = predictor.forward_pass(obs, labels)


        variance = predictor.get_variance(obs)

        within_ci = np.zeros((n_samples, 2))
        loglikelihood = np.zeros((n_samples, 2))
        loglikelihood2 = np.zeros((n_samples, 2))
        sds = np.zeros((n_samples, 2))

        for i in range(n_samples):
            raw_value_pred = ds.scaled_to_raw(predictions[i]).reshape(predictions[i].shape[0:-1])
            sd = np.sqrt(variance[i])
            sd = ds.scale_sd_to_raw(sd).reshape(predictions[i].shape[0:-1])
            within_ci[i, 0] = np.sum(np.abs((raw_value_pred - non_scaled_maps[i][0])  <= (1.96 * sd)  ) * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / np.sum(outdoors[i])
            loglikelihood[i, 0] = np.sum((-0.5 * np.log((sd **2) * 2 * np.pi) -  ((raw_value_pred - non_scaled_maps[i][0])**2) / (2 * (sd ** 2))) * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / np.sum(outdoors[i])
            loglikelihood2[i, 0] = np.sum((((raw_value_pred - non_scaled_maps[i][0])**2) / ((sd ** 2))) * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / np.sum(outdoors[i])
            sds[i, 0] = np.sum(sd * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / np.sum(outdoors[i])
            if benchmark or kriging:
                sd = np.sqrt(reconstruct_vars[i])
                sds[i, 1] = np.sum(sd) / np.sum(outdoors[i])
                within_ci[i, 1] = np.sum(np.abs((reconstruct_means[i] - non_scaled_maps[i][0])  <= (1.96 * sd)  ) * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / np.sum(outdoors[i])
                loglikelihood[i, 1] = np.sum((-0.5 * np.log((sd **2) * 2 * np.pi) -  ((reconstruct_means[i] - non_scaled_maps[i][0])**2) / (2 * (sd ** 2))) * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / np.sum(outdoors[i])
                loglikelihood2[i, 1] = np.sum((((reconstruct_means[i] - non_scaled_maps[i][0])**2) / ((sd ** 2))) * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / np.sum(outdoors[i])
                #if loglikelihood[i,1] < -100:
                #    print('mean', 'variance', sd)
        comparsion_data['meas_rate'].append(meas_rate)
        comparsion_data['pred_within_ci'].append(np.mean(within_ci[:, 0]))
        comparsion_data['loglikelihood'].append(np.median(loglikelihood[:, 0]))
        comparsion_data['goodness'].append(np.median(loglikelihood2[:, 0]))
        comparsion_data['sd'].append(np.mean(sds[:, 0]))
        comparsion_data['krig_sd'].append(np.mean(sds[:, 1]))
        comparsion_data['krig_loglikelihood'].append(np.median(loglikelihood[:, 1]))
        comparsion_data['krig_goodness'].append(np.median(loglikelihood2[:, 1]))
        comparsion_data['krig_pred_within_ci'].append(np.mean(within_ci[:, 1]))

    df = pd.DataFrame.from_dict(comparsion_data)
    df.to_csv('figures/data_%s.csv' % save_file, mode = 'w', index=False, header = True)




if __name__ == '__main__':
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'

    #plot_training_loss('/home/enesk/repos/spatial-prediction/logs/logging-regr_cont_meas_rate.log', 'regression1')
    #plot_training_loss('/home/enesk/repos/spatial-prediction/logs/logging-regr.log', 'regression2')
    #plot_training_loss('/home/enesk/repos/spatial-prediction/logs/logging-prob2.log', 'prob')

    #compare_model_to_benchmark('prob2', 0.003, 0.05, 12, 512, 'prob2-long', kriging = True, force_random_walk = False)
    calculate_goodness('prob-rw', 0.003, 0.03, 9, 512, 'prob-rw-goodness', kriging = True, force_random_walk = False)

    #calculate_sd('prob2', 0.003, 0.03, 9, 512, 'prob2-variance', kriging = False, force_random_walk = False)
