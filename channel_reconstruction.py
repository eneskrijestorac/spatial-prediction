from utils import plot_grid
import random
import sys
import numpy as np
from os.path import isfile, join
try:
	import matplotlib.pyplot as plt
	import matplotlib
	matplotlib.use('TkAgg')
except ImportError:
	print("Import error")



# These were obtained by running fit_model.py
sigma_sh = 20.538627844634295
beta_sh = 58.2154289435845
eta_pl = 2.6380131522193353
K = 30.517450774683947
GRID_LEN = 4
z = 12

def reconstruct_channel(measured_points, unmeasured_points, power_grid):
	y_size, x_size = power_grid.shape
	n_measured_points = len(measured_points)
	n_unmeasured_points = len(unmeasured_points)


	# Index of all unmeasured coordinates
	all_points = list(range(y_size * x_size));

	# A list of all possible coordinates. Note that the coordinates are scaled by grid len
	all_coordinates = np.asarray(np.unravel_index(all_points, power_grid.shape)).T



	# All measured coordinates
	measured_coordinates = all_coordinates[measured_points, :].astype(np.int)




	# Calculate Phi_q
	Phi_q = np.kron(np.ones((1, n_measured_points, 1)), measured_coordinates.reshape((n_measured_points, 1, 2))) - \
		np.kron(np.ones((n_measured_points, 1, 1)), measured_coordinates.reshape((1, n_measured_points, 2)))
	Phi_q = (sigma_sh ** 2) * np.exp((-GRID_LEN/beta_sh) * np.linalg.norm(Phi_q, axis = 2))
	Phi_q_inv = np.linalg.pinv(Phi_q)

	# Get gamma q
	Gamma_q = power_grid[measured_coordinates[:, 0], measured_coordinates[:, 1]].reshape(n_measured_points, 1)



	# Calculate H_q
	D_q = 10 * np.log(np.linalg.norm(np.concatenate([measured_coordinates, np.ones((n_measured_points,1))* z] , axis = 1), axis = 1)).reshape((n_measured_points,1))
	H_q = np.concatenate([np.ones((n_measured_points, 1)), -D_q], axis = 1)


	# Calculate theta
	theta = np.asarray([K, eta_pl]).reshape((2, 1))


	unmeasured_coordinates = all_coordinates[unmeasured_points, :].astype(np.int)


	# Calculate Phi_r
	Phi_r = np.kron(np.ones((1, n_unmeasured_points, 1)), unmeasured_coordinates.reshape((n_unmeasured_points, 1, 2))) - \
		np.kron(np.ones((n_unmeasured_points, 1, 1)), unmeasured_coordinates.reshape((1, n_unmeasured_points, 2)))
	Phi_r = (sigma_sh ** 2) * np.exp((-GRID_LEN/beta_sh) * np.linalg.norm(Phi_r, axis = 2))

	# Calculate Psi_r_q
	Psi_r_q = np.kron(np.ones((1, n_measured_points, 1)), unmeasured_coordinates.reshape((n_unmeasured_points, 1, 2))) - \
					np.kron(np.ones((n_unmeasured_points, 1, 1)), measured_coordinates.reshape((1, n_measured_points, 2)))
	Psi_r_q = (sigma_sh ** 2) * np.exp((-GRID_LEN/beta_sh) * np.linalg.norm(Psi_r_q, axis = 2))


	# Calcaulate H_r
	D_r = 10 * np.log(np.linalg.norm(np.concatenate([unmeasured_coordinates, np.ones((n_unmeasured_points,1))* z] , axis = 1), axis = 1)).reshape((n_unmeasured_points,1))
	H_r = np.concatenate([np.ones((n_unmeasured_points, 1)), -D_r], axis = 1)

	# Calculate Gamma_r
	Psi_inv_dot_Phi = Psi_r_q.dot(Phi_q_inv)
	Gamma_r_temp = H_r.dot(theta) +  Psi_inv_dot_Phi.dot(Gamma_q - H_q.dot(theta))
	Sigma_r_temp = Phi_r - Psi_inv_dot_Phi.dot(Psi_r_q.T)

	Gamma_r = Gamma_r_temp.T
	Sigma_r = Sigma_r_temp


	return Gamma_r, Sigma_r

if __name__ == "__main__":
	sinr_grid, city_grid = load_grids() # Load grids
	power_grid = sinr_grid + NOISE_LEVEL

	Gamma_r, Sigma_r, measured_coordinates, _ = reconstruct_channel(n_measured_points, power_grid)


	reconstruct_grid = Gamma_r


	blacked_out_grid = np.empty(sinr_grid.shape)
	blacked_out_grid[:] = np.nan
	blacked_out_grid[measured_coordinates[:, 0], measured_coordinates[:, 1]] = power_grid[measured_coordinates[:, 0], measured_coordinates[:, 1]]




	plot_grid(blacked_out_grid, title = 'Measured points')
	plot_grid(reconstruct_grid, title = 'Reconstruction values')
	plot_grid(power_grid, title = 'Actual values')
	plot_grid(sinr_grid, title = 'SINR grid')
	plt.show()
