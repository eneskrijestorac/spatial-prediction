import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from shapely.geometry import Polygon, Point
from matplotlib.patches import Polygon as MatplotlibPatch
from descartes.patch import PolygonPatch
import sys
from random import shuffle
import random
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.collections import PatchCollection
import math
from functools import partial
import shapely.ops as ops
import pyproj    
import os
from matplotlib.collections import PatchCollection
from shutil import copyfile
import xml.etree.ElementTree as ET
from lxml import etree
from io import StringIO, BytesIO

OUT_DIR = '/media/enesk/optical_hdd/Enes/spatial_prediction/wi_setups_4d/'

class Building:

	def __init__(self, base, height):
		"""
		base: a Polygon object
		"""
		self.base = base
		self.height = height

	def get_base_vertices(self):
		return self.base.exterior.coords

def get_polygons(boundary_vertices, dividers_per_dim, clearance, min_separation, level, max_levels, min_clearance):
	polys = []
	x_min = np.min(boundary_vertices[:, 0])
	x_max = np.max(boundary_vertices[:, 0])
	y_min = np.min(boundary_vertices[:, 1])
	y_max = np.max(boundary_vertices[:, 1])
	length = x_max - x_min
	width = y_max - y_min
	x_dividers = [x_min, x_max]
	y_dividers = [y_min, y_max]

	in_clearance = clearance

	clearance = max(clearance, min_clearance)
	if level > 0 and np.random.random() > 0.5:
		clearance = 0

	if (min_separation+clearance)*(dividers_per_dim+1) <= (min(length, width)+1):
		last = False
	else:
		last = True

	#if (min_separation+clearance)*(dividers_per_dim+1) <= (length + 1):
		# Add more dividers
	for i in range(1000):
		divider = np.random.random() * (length) + x_min 
		if any([abs(divider - d) < (min_separation + clearance) for d in x_dividers]):
			continue
		x_dividers.append(divider)
		if len(x_dividers) == (dividers_per_dim + 2):
			break

	#if (min_separation+clearance)*(dividers_per_dim+1) <= (width+1):
	for i in range(1000):
		divider = np.random.random() * (width) + y_min
		if any([abs(divider - d) < (min_separation+clearance) for d in y_dividers]):
			continue
		y_dividers.append(divider)
		if len(y_dividers) == (dividers_per_dim + 2):
			break



	x_dividers = np.sort(x_dividers)
	y_dividers = np.sort(y_dividers)

	poly_vertices = []
	for i in range(len(x_dividers)-1):
		for j in range(len(y_dividers)-1):
			this_poly_vertices = []
			for m,n in [(0, 0), (1, 0), (1, 1), (0, 1)]:
				this_poly_vertices.append([x_dividers[i+m]+(0.5-m)*clearance, y_dividers[j+n]+(0.5-n)*clearance])
			poly_vertices.append(this_poly_vertices)
			

   
	if (level == (max_levels-1)) or last:
		for element in poly_vertices:
			polys.append(Polygon(element))
		
	else:
		for element in poly_vertices:
			outs = get_polygons(np.asarray(element), math.ceil(dividers_per_dim/2), in_clearance*2/3, min_separation, level + 1, max_levels, min_clearance)
			polys = polys + outs
	return polys

def draw_setup(length, width, polys, min_height, max_height, tx_pts, save_path):
	fig = plt.figure(dpi=90)
	fig.set_size_inches(12, 9)
	ax = fig.add_subplot(121)
	heights = np.asarray([b.height for b in buildings])

	patches = [MatplotlibPatch(np.asarray(b.base.exterior.coords), closed = True) for b in buildings]
	cmap = plt.get_cmap()
	colors = cmap((heights-min_height) / (max_height - min_height)) # convert nfloors to colors that we can use later

	sm = matplotlib.cm.ScalarMappable(matplotlib.colors.Normalize(vmin=min_height, vmax=max_height, clip=False), cmap)

	collection = PatchCollection(patches)
	col = ax.add_collection(collection)
	collection.set_color(colors)

	fig.colorbar(sm, orientation = 'horizontal', ax = ax)
	
	# Draw transmitters and receivers
	ax.scatter([p[0] for p in tx_pts], [p[1] for p in tx_pts])
	if len(tx_pts):
		ax.legend(['Transmitter locations'])
	ax.set_xlim(0, length)
	ax.set_xlabel('x (m)')
	ax.set_ylabel('y (m)')
	ax.set_ylim(0, width)

	fig.savefig(save_path + '.png')
	fig.savefig(save_path + '.pdf')

def convert_building_into_faces(building):
	out = []
	out.append('begin_<sub_structure>\n')
	base_vertices = building.get_base_vertices()
	out.append('begin_<face> 1\n')
	out.append('Material 5\n')
	out.append('nVertices %d\n' % (len(base_vertices)-1))
	for j in range(len(base_vertices)-1):
		out.append('%f %f %f\n' % (base_vertices[j][0], base_vertices[j][1], building.height))
	out.append('end_<face>\n')

	n_faces = 1
	for j in range(len(base_vertices)-1):
		vs = base_vertices[j:j+2]
		out.append('begin_<face> %d\n' % (n_faces+1))
		out.append('Material 5\n')
		out.append('nVertices %d\n' % 4)
		out.append('%f %f %f\n' % (base_vertices[j][0], base_vertices[j][1], building.height))
		out.append('%f %f %f\n' % (base_vertices[j+1][0], base_vertices[j+1][1], building.height))
		out.append('%f %f %f\n' % (base_vertices[j+1][0], base_vertices[j+1][1], 0))
		out.append('%f %f %f\n' % (base_vertices[j][0], base_vertices[j][1], 0))
		out.append('end_<face>\n')
		n_faces = n_faces + 1
	out.append('end_<sub_structure>\n')
	
	return out

def convert_building_into_city_file(buildings, template_file, out_file):
	templ_file = open(template_file, 'r')
	templ_file = templ_file.readlines()
	addition = []
	addition.append('begin_<structure_group>\n')
	for i, building in enumerate(buildings):
		addition.append('begin_<structure> %d\n' % i)
		faces = convert_building_into_faces(building)
		addition = addition + faces
		addition.append('end_<structure>\n')
	addition.append('end_<structure_group>\n')
	config = templ_file[0:-1] + addition + [templ_file[-1]]
	out_file = open(out_file, "w")
	out_file.writelines(config)
	out_file.close()



def draw_polygons(length, width, polys):
	fig = plt.figure(1, dpi=90)
	fig.set_size_inches(4, 4)
	ax = fig.add_subplot(121)
	for poly in polys:
		patch = PolygonPatch(poly, alpha=0.5, edgecolor = 'black', zorder=2)
		ax.add_patch(patch)
	ax.set_xlim(0, length)
	ax.set_ylim(0, width)


def get_tx_location(width, length, n_transmitters_per_dim, clearance, transmitter_height):
	x_vertices = np.linspace(clearance, length-clearance, n_transmitters_per_dim[0])
	y_vertices = np.linspace(clearance, width-clearance, n_transmitters_per_dim[1])

	vertices = []
	for x in x_vertices:
		for y in y_vertices:
			invalid = any([b.base.contains(Point(x, y)) for b in buildings]) # The transmitter cannot be inside a building
			if not(invalid):
				vertices.append((x, y, transmitter_height))		

	return vertices

def get_rx_location(width, length, max_height, spacing, clearance, min_height):
	x_vertices = np.arange(clearance, length-clearance, spacing)
	y_vertices = np.arange(clearance, width-clearance, spacing)
	z_vertices = np.arange(min_height, max_height+0.000000001, spacing)

	print("RX heights:", z_vertices)
	print("RX grid shape", [len(x_vertices), len(y_vertices), len(z_vertices)])

	vertices = []
	for x in x_vertices:
		for y in y_vertices:
			for z in z_vertices:
				# TODO: Skip locations that are inside building
				vertices.append((x, y, z))		

	return vertices


def get_terrain_file(width, length, template_file, out_file):
	templ_file = open(template_file, 'r')
	templ_file = templ_file.readlines()
	start_index = templ_file.index('<INSERT_VERTICES_HERE>\n') # Find the index with the placeholder

	# nVertices 4
	# -5.0000000000 -5.0000000000 0.0000000000
	# -5.0000000000 405.0000000000 0.0000000000
	# 405.0000000000 405.0000000000 0.0000000000
	# 405.0000000000 -5.0000000000 0.0000000000

	templ_file.pop(start_index)
	templ_file.insert(start_index, 'nVertices %d\n' % 4) # Only one transmitter hardcoded
	templ_file.insert(start_index+1, '%f %f %f\n' % (-5, -5, 0))	
	templ_file.insert(start_index+2, '%f %f %f\n' % (-5, width+5, 0))	
	templ_file.insert(start_index+3, '%f %f %f\n' % (length+5, width+5, 0))
	templ_file.insert(start_index+4, '%f %f %f\n' % (length+5, -5, 0))

	out_file = open(out_file, "w")
	out_file.writelines(templ_file)
	out_file.close()

def get_setup_files(width, length, buildings, tx_points, rx_points, 
		    			use_gpu, template_file_dir, template_file_str, template_file_xml_str, 
							out_file, out_file_xml, carr_freq = "5000000000"):

	folder_counter = len([name for name in os.listdir(OUT_DIR) if os.path.isdir(os.path.join(OUT_DIR, name))])
	if folder_counter:
		scenario_counter = max([int(name.split('-')[0]) for name in os.listdir(OUT_DIR) if os.path.isdir(os.path.join(OUT_DIR, name))]) + 1
	else:
		scenario_counter = 0

	# Open the .txrx and .xml file
	templ_file = open(template_file_str, 'r')	
	templ_file = templ_file.readlines()
	templ_file_xml = open(template_file_xml_str, 'r')	
	templ_file_xml = templ_file_xml.readlines()

	# Find the line with carrier frequency and replace
	for i, xml_line in enumerate(templ_file_xml):
		if '<INSERT_CARR_FREQ_HERE>' in xml_line:
			templ_file_xml[i] = templ_file_xml[i].replace('<INSERT_CARR_FREQ_HERE>', carr_freq)  

	# Insert the RX locations in the .txrx and .xml file
	print("Insert the RX locations in the .txrx and .xml file (%d points)" % len(rx_points))
	start_index = templ_file.index('<INSERT_RX_VERTICES_HERE>\n') # Find the index with the placeholder
	start_index_xml = templ_file_xml.index('<INSERT_RX_VERTICES_HERE>\n') # Find the index with the placeholder
	templ_file.pop(start_index) # Remove the placeholder
	templ_file_xml.pop(start_index_xml) # Remove the placeholder
	for idx, rx_point in enumerate(rx_points):
		templ_file.insert(start_index, '%f %f %f\n' % (rx_point[0], rx_point[1], rx_point[2]))	
		templ_file_xml.insert(start_index_xml, '<ProjectedPoint>\n<remcom::rxapi::CartesianPoint>\n')
		templ_file_xml.insert(start_index_xml+1, '<X>\n')
		templ_file_xml.insert(start_index_xml+2, '<remcom::rxapi::Double Value="%f"/>\n' % rx_point[0])
		templ_file_xml.insert(start_index_xml+3, '</X>\n')
		templ_file_xml.insert(start_index_xml+4, '<Y>\n')
		templ_file_xml.insert(start_index_xml+5, '<remcom::rxapi::Double Value="%f"/>\n' % rx_point[1])
		templ_file_xml.insert(start_index_xml+6, '</Y>\n')
		templ_file_xml.insert(start_index_xml+7, '<Z>\n')
		templ_file_xml.insert(start_index_xml+8, '<remcom::rxapi::Double Value="%f"/>\n' % rx_point[2])
		templ_file_xml.insert(start_index_xml+9, '</Z>\n')
		templ_file_xml.insert(start_index_xml+10, '</remcom::rxapi::CartesianPoint>\n</ProjectedPoint>\n')
	templ_file.insert(start_index, 'nVertices %d\n' % len(rx_points))



	for idx, tx_point in enumerate(tx_points):
		templ_file_with_rx = templ_file.copy()
		templ_file_xml_with_rx = templ_file_xml.copy()

		# Output directory
		OUT_PATH = OUT_DIR+'/%d-%d'%(scenario_counter, idx)
		if not os.path.exists(OUT_PATH):
			os.makedirs(OUT_PATH)


		# Insert the TX location in the .txrx and .xml file
		print("Insert the TX location in the .txrx and .xml file")
		start_index = templ_file_with_rx.index('<INSERT_VERTICES_HERE>\n') # Find the index with the placeholder
		templ_file_with_rx.pop(start_index) # Remove the placeholder
		start_index_xml = templ_file_xml_with_rx.index('<INSERT_VERTICES_HERE>\n') # Find the index with the placeholder
		templ_file_xml_with_rx.pop(start_index_xml) # Remove the placeholder
		templ_file_with_rx.insert(start_index, '%f %f %f\n' % (tx_point[0], tx_point[1], tx_point[2]))	
		templ_file_with_rx.insert(start_index, 'nVertices %d\n' % 1) # Only one transmitter hardcoded
		templ_file_xml_with_rx.insert(start_index_xml, '<ProjectedPoint>\n<remcom::rxapi::CartesianPoint>\n')
		templ_file_xml_with_rx.insert(start_index_xml+1, '<X>\n')
		templ_file_xml_with_rx.insert(start_index_xml+2, '<remcom::rxapi::Double Value="%f"/>\n' % tx_point[0])
		templ_file_xml_with_rx.insert(start_index_xml+3, '</X>\n')
		templ_file_xml_with_rx.insert(start_index_xml+4, '<Y>\n')
		templ_file_xml_with_rx.insert(start_index_xml+5, '<remcom::rxapi::Double Value="%f"/>\n' % tx_point[1])
		templ_file_xml_with_rx.insert(start_index_xml+6, '</Y>\n')
		templ_file_xml_with_rx.insert(start_index_xml+7, '<Z>\n')
		templ_file_xml_with_rx.insert(start_index_xml+8, '<remcom::rxapi::Double Value="%f"/>\n' % tx_point[2])
		templ_file_xml_with_rx.insert(start_index_xml+9, '</Z>\n')
		templ_file_xml_with_rx.insert(start_index_xml+10, '</remcom::rxapi::CartesianPoint>\n</ProjectedPoint>\n')
		


		# gpu_index = templ_file_xml.index('<remcom::rxapi::Boolean Value="<INSERT_GPU_HERE>"/>\n')
		# if use_gpu:
		# 	templ_file_xml[gpu_index] = templ_file_xml[gpu_index].replace('<INSERT_GPU_HERE>', 'true')
		# else:
		# 	templ_file_xml[gpu_index] = templ_file_xml[gpu_index].replace('<INSERT_GPU_HERE>', 'false')
		


		of = open(OUT_PATH + out_file, "w")
		of.writelines(templ_file_with_rx)
		of.close()
		of = open(OUT_PATH + out_file_xml, "w")
		of.writelines(templ_file_xml_with_rx)
		of.close()

		# Copy the setup files
		copyfile('%s/setup.setup' % template_file_dir, OUT_PATH+'/setup.setup')
		copyfile('%s/setup.vw' % template_file_dir, OUT_PATH+'/setup.vw')

		# Get the buildings file
		print("Get the buildings file")
		convert_building_into_city_file(buildings, '%s/template.city' % template_file_dir,
			OUT_PATH+'/city(2).city')

		# Get the terrain file
		print("Get the terrain file")
		get_terrain_file(width, length, '%s/template.ter' % template_file_dir, OUT_PATH+'/terrain(2).ter')

		# Make the directory for outputs
		os.makedirs(OUT_PATH+'/Study Area')

		# Comment out for png visualization
		draw_setup(length, width, buildings, min_height, max_height, [tx_point], OUT_PATH+'/diagram')
		draw_setup(length, width, buildings, min_height, max_height, [], OUT_PATH+'/only_city')
	

if __name__ == '__main__':
	# Parameters that determine the shape and size of space
	min_height = 10
	max_height = 30
	width = 512
	length = 512
	dividers = 3
	clearance = 20 
	min_clearance = 15
	open_space = 0.3
	min_separation = 25
	max_levels = 20

	# Carrier frequency
	carr_freq = "6000000000"

	# Ground user parameters
	n_transmitters_per_dim = (5, 5)
	transmitter_height = 2 # m

	# Ground user parameters (6 GHz)
	n_transmitters_per_dim = (20, 20)
	transmitter_height = 2 # m

	# Controls whether WI will use GPU
	use_gpu = True

	# RX parameters
	rx_height_max = 30
	rx_clearance = 0
	rx_spacing = 4 
	rx_height_min = 10
	
	# RX parameters (6 GHz)
	rx_height_max = 30
	rx_clearance = 0
	rx_spacing = 64
	rx_height_min = 30



	boundary_vertices = np.asarray([[0, width], [length, width], [length, 0], [0, 0]])
	polys = get_polygons(boundary_vertices, dividers, clearance, min_separation, 0, max_levels, min_clearance)
	polys = random.sample(polys,  int((1-open_space) * len(polys)))

	#draw_polygons(length, width, polys)

	buildings = []
	min_height = np.inf
	max_height = 0
	for poly in polys:
		height = np.random.random() * (max_height - min_height) + min_height
		height = poly.area * 1.5 / 100
		min_height = min(min_height, height)
		max_height = max(max_height, height)
		buildings.append(Building(poly, height))


	# Get TX locations
	tx_pts = get_tx_location(width, length, n_transmitters_per_dim, clearance+10, transmitter_height)


	# Get TX locations
	rx_pts = get_rx_location(width, length, rx_height_max, rx_spacing, rx_clearance, rx_height_min)



	# Get the transmitters receivers filesetup.Study\ Area.xml
	get_setup_files(width, length, buildings, tx_pts, rx_pts, use_gpu, 'templates_3d', 'templates_3d/setup.txrx', 
			'templates_3d/setup.Study Area.xml','/setup.txrx', '/setup.Study Area.xml', carr_freq=carr_freq)



	#plt.show()