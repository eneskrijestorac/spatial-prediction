import numpy as np
import random
from utils import plot_grid
from os import listdir
import pandas as pd
from os.path import isfile, join
import matplotlib.pyplot as plt
import matplotlib
from channel_reconstruction import reconstruct_channel
import  scipy.stats
import copy
from parse import parse
import time
from semivar import semivar_fit, estimate_eta, kriging_fit

class Dataset:
	def __init__(self, data_dir, n_uavs = 1, measured_rate = 0.1, random_walk_momentum = 0.9, 
	      			no_maps = False, cont_meas_rate = False, variable_rw_momentum = False, use_location = False,
						train_file_pfx = 'train', test_file_pfx = 'test', clip_gains = False):
		summary = pd.read_csv('%s/summary.txt' % data_dir, sep = '\t').to_dict(orient = 'records')
		self.PWR_GRIDS_DIRECTORY = '%s/pwr_grids' % data_dir
		self.CITY_GRIDS_DIRECTORY = '%s/city_grids' % data_dir
		self.OBSERVATION_WIDTH = 96
		self.USER_ALTITUDE = 2
		self.train_file_pfx = train_file_pfx
		self.test_file_pfx = test_file_pfx
		self.clip_gains = clip_gains
		self.NOISE_LEVEL = -104.01029 # As defined in the simulator
		self.PWR_THRESHOLD = self.NOISE_LEVEL + 10 # Used to asses the predictor in terms of thershold
		self.GRID_LEN = 4
		self.MAX_PWR = float([record['value'] for record in summary if record['parameter'] == 'MAX_POWER'][0])
		if self.clip_gains:
			self.MIN_PWR = self.NOISE_LEVEL
		else:
			self.MIN_PWR = float([record['value'] for record in summary if record['parameter'] == 'MIN_POWER'][0])
		self.MAX_HEIGHT = float([record['value'] for record in summary if record['parameter'] == 'MAX_HEIGHT'][0])
		self.MIN_HEIGHT = float([record['value'] for record in summary if record['parameter'] == 'MIN_HEIGHT'][0])
		try:
			self.UAV_ALTITUDE = float([record['value'] for record in summary if record['parameter'] == 'UAV_ALTITUDE'][0])
		except:
			print("WARNING: Using default UAV altitude of 20m.")
			self.UAV_ALTITUDE = 20.0




		self.pwr_train_grids, self.city_train_grids, self.user_coordinates_train, self.user_location_grids_train = self.load_grids(test = False)
		print("There are %d training grids." % len(self.pwr_train_grids))
		self.pwr_test_grids, self.city_test_grids, self.user_coordinates_test, self.user_location_grids_test = self.load_grids(test = True)
		print("There are %d testing grids." % len(self.pwr_test_grids))
		self.measured_rate = measured_rate
		self.random_walk_momentum = random_walk_momentum
		self.variable_rw_momentum = variable_rw_momentum
		self.no_maps = no_maps
		self.cont_meas_rate = cont_meas_rate
		self.n_uavs = n_uavs
		self.use_location = use_location
		self.clip_gains = clip_gains # Clip channel gains to be up to the noise floor

		if no_maps:
			self.obs_shape = (self.OBSERVATION_WIDTH, self.OBSERVATION_WIDTH, 1)
		elif use_location and no_maps:
			raise ValueError("Not implemented")
			self.obs_shape = (self.OBSERVATION_WIDTH, self.OBSERVATION_WIDTH, 2)
		elif use_location:
			self.obs_shape = (self.OBSERVATION_WIDTH, self.OBSERVATION_WIDTH, 3)
		else:
			self.obs_shape = (self.OBSERVATION_WIDTH, self.OBSERVATION_WIDTH, 2)


	def set_meas_rate(self, meas_rate):
		self.measured_rate = meas_rate

	def load_grids(self, test):
		

		# The dateset is split into train vs test
		if test:
			suffix = '/' + self.test_file_pfx
		else:
			suffix = '/' + self.train_file_pfx

		# Initiate the dictionaries to store the grids
		pwr_grids = {}
		city_grids = {}
		user_location_grids = {}
		user_coordinates = {}


		# Load the stored grids into numpy arrays
		# join(self.CITY_GRIDS_DIRECTORY+suffix, f)
		all_city_grids = [f for f in listdir(self.CITY_GRIDS_DIRECTORY+suffix) if (isfile(join(self.CITY_GRIDS_DIRECTORY+suffix, f))  )] # The grids use to be rotated which is why																								 # we had different city files
		all_pwr_grids = [f for f in listdir(self.PWR_GRIDS_DIRECTORY+suffix) if (isfile(join(self.PWR_GRIDS_DIRECTORY+suffix, f)) )]
		city_grid_file = join(self.CITY_GRIDS_DIRECTORY+suffix, random.choice(all_city_grids))
		all_city_grids.sort()
		all_pwr_grids.sort()


		for i,f in enumerate(all_pwr_grids):

			# Get the scenario and ground user ID
			parse_result = parse("pwr_grid-{:d}-{:d}.txt", f)
			scenario = parse_result[0] 
			transmitter = parse_result[1]

			# Get the grids corresponding to this scenario/transmitter
			pwr_grid_file = join(self.PWR_GRIDS_DIRECTORY+suffix, f)	
			city_grid_file = "city_grid-%d-%d.txt" % (scenario, transmitter)
			city_grid_file = join(self.CITY_GRIDS_DIRECTORY+suffix, city_grid_file)	
			pwr_grid = np.loadtxt(pwr_grid_file, dtype=float)
			city_grid = np.loadtxt(city_grid_file, dtype=float)
			user_location_grid = np.zeros(city_grid.shape)
			user_location_grid[np.unravel_index(np.argmax(pwr_grid, axis=None), pwr_grid.shape)] = 1
			if self.clip_gains:
				pwr_grid = np.clip(pwr_grid, self.NOISE_LEVEL, np.inf)
			pwr_grids[scenario, transmitter] = pwr_grid
			city_grids[scenario, transmitter] = city_grid
			user_coordinates[scenario, transmitter] = np.unravel_index(np.argmax(pwr_grid, axis=None), pwr_grid.shape) * self.GRID_LEN
			user_location_grids[scenario, transmitter] = user_location_grid

		return pwr_grids, city_grids, user_coordinates, user_location_grids

	def plot_all_grids(self, train = True):
		if train:
			pwr_grids = np.copy(self.pwr_train_grids)
			city_grids = copy.deepcopy(self.city_train_grids)
			user_coordinates = self.user_coordinates_train
		else:
			pwr_grids = np.copy(self.pwr_test_grids)
			city_grids = copy.deepcopy(self.city_test_grids)
			user_coordinates = self.user_coordinates_test
		n_grids = pwr_grids.shape[0]

		print(n_grids)

		for grid_idx in range(1):
			pwr_grid = pwr_grids[grid_idx, :, :]
			city_grid = city_grids[grid_idx, :, :]

			user_location = (user_coordinates[grid_idx][1], user_coordinates[grid_idx][0], 'Transmitter')
			plot_grid(pwr_grid, title = 'Power observation', dots = [user_location])
			#plot_grid(city_grid, title = 'City obeservation', dots = [user_location])


	def sample_maps_per_scen(self, scenario_idx = None, transmitter_idx = None, test_dataset = True):
		# Signal gain maps and user locations
		sg = []
		user_locations = []

		# All avaialable test signal maps and environments 
		if test_dataset:
			pwr_grids = (self.pwr_test_grids)
			city_grids = (self.city_test_grids)
			all_user_locations = self.user_coordinates_test
		else:
			pwr_grids = (self.pwr_train_grids)
			city_grids = (self.city_train_grids)
			all_user_locations = self.user_coordinates_train
		
	
		# Select a random scenario to collect siganl gain maps for
		# Each signal gain map corresponds to one user
		grid_keys = list(pwr_grids.keys())
		random.shuffle(grid_keys)
		grid_key = grid_keys[0]
		if scenario_idx == None:
			scenario = grid_key[0]
		else:
			scenario = scenario_idx
		


		if transmitter_idx == None:
			keys_with_the_same_scen = [k for k in grid_keys if k[0] == scenario]
		else:
			keys_with_the_same_scen = [(scenario_idx, transmitter_idx)]
		

		# Take out only a square of size 140x140 from the map
		width = pwr_grids[grid_key].shape[0]
		length = pwr_grids[grid_key].shape[1]
		if length > self.OBSERVATION_WIDTH:
			user_loc = np.unravel_index(np.argmax(pwr_grids[grid_key], axis=None), pwr_grids[grid_key].shape)
			x_start = np.maximum(0, (user_loc[1] - self.OBSERVATION_WIDTH + 1))
		else:
			x_start = 0
		#x_start = np.random.randint(0, (length - self.OBSERVATION_WIDTH))
		x_stop = x_start + self.OBSERVATION_WIDTH
		if width > self.OBSERVATION_WIDTH:
			y_start = np.maximum(0, (user_loc[0] - self.OBSERVATION_WIDTH + 1))
		else:
			y_start = 0
		#y_start = np.random.randint(0, (width - self.OBSERVATION_WIDTH))
		y_stop = y_start + self.OBSERVATION_WIDTH


		for jj, grid_key in enumerate(keys_with_the_same_scen):

			# Sample random map
			pwr_grid = np.copy(pwr_grids[grid_key])
			city_grid = np.copy(city_grids[grid_key])




			# Take out only a square of size OWxOW from the map
			pwr_ob = pwr_grid[y_start:y_stop, x_start:x_stop]
			city_ob = city_grid[y_start:y_stop, x_start:x_stop]
			city_map = np.copy(city_ob)
			pwr_ob_all = np.copy(pwr_ob)
			sg.append(pwr_ob_all)
			user_locations.append(all_user_locations[grid_key])

			if jj > 1:
				assert np.sum(city_map-previous_map) == 0
			previous_map = city_map



		#print("grid_key",grid_key)


		return sg, city_map, user_locations


	# def sample_entire_maps(self, n_maps):
	# 	cg = []
	# 	sg = []

	# 	for i in range(n_maps):
	# 		# All grids
	# 		pwr_grids = np.copy(self.pwr_test_grids)
	# 		city_grids = copy.deepcopy(self.city_test_grids)
		

	# 		# Sample grid index
	# 		n_grids = pwr_grids.shape[0]
	# 		grid_idx = np.random.randint(0, int(n_grids))

	# 		# Sample random map
	# 		pwr_grid = pwr_grids[grid_idx, :, :]
	# 		city_grid = city_grids[grid_idx, :, :]
	# 		random_rotation = 0
	# 		pwr_grid = np.rot90(pwr_grid, k = random_rotation)
	# 		city_grid = np.rot90(city_grid, k = random_rotation)
	# 		pwr_grid_all = np.copy(pwr_grid)


	# 		# Select a section of the map
	# 		width = pwr_grid.shape[0]
	# 		length = pwr_grid.shape[1]

	# 		# Take out only a square of size OWxOW from the map
	# 		x_start = np.random.randint(0, (length - self.OBSERVATION_WIDTH))
	# 		x_stop = x_start + self.OBSERVATION_WIDTH
	# 		y_start = np.random.randint(0, (width - self.OBSERVATION_WIDTH))
	# 		y_stop = y_start + self.OBSERVATION_WIDTH

	# 		pwr_ob = pwr_grid[y_start:y_stop, x_start:x_stop]
	# 		city_ob = city_grid[y_start:y_stop, x_start:x_stop]
	# 		city_map = np.copy(city_ob)
	# 		pwr_ob_all = np.copy(pwr_ob)
	# 		cg.append(city_map)
	# 		sg.append(pwr_ob_all)

	# 	return sg, city_map

	def get_obs_from_meas_and_grid(self, measured_coordinates,  pwr_ob, city_ob):
		# Get the shape of the power grid
		y_size, x_size = pwr_ob.shape

		# Masked observation
		measured_coordinates = np.asarray(measured_coordinates, dtype = np.int)
		masked_ob = -np.ones(pwr_ob.shape) * (self.MAX_PWR - self.MIN_PWR) + self.MIN_PWR
		masked_ob[measured_coordinates[:, 0], measured_coordinates[:, 1]] = pwr_ob[measured_coordinates[:, 0], measured_coordinates[:, 1]]

		# Normalize the observations
		city_ob = (city_ob - self.UAV_ALTITUDE) / (self.MAX_HEIGHT - self.UAV_ALTITUDE)
		masked_ob = (masked_ob - self.MIN_PWR) / (self.MAX_PWR - self.MIN_PWR)


		if self.no_maps:
			ob = np.stack([masked_ob], axis = 2)
		else:
			ob = np.stack([city_ob, masked_ob], axis = 2)
		return ob

	def sample_whole_maps(self, n_maps, train = True, include_non_scaled = False,
							include_reconstruct = False, display = False, random_walk = False,
								boolean = False, kriging = False, detrend = False):

		if self.cont_meas_rate:
			rate = np.random.rand() * (0.03 - 0.003) + 0.003
		else:
			rate = self.measured_rate

		assert not(kriging and include_reconstruct)

		obs = []
		labels = []
		outdoors = []
		non_scaled_maps = []
		reconstruct_means = []
		reconstruct_probs = []

		# Get all scenario-transmitter keys
		if train:
			pwr_grids = (self.pwr_train_grids)
			city_grids = (self.city_train_grids)
			user_loc_grids = self.user_location_grids_train
			user_coordinates = self.user_coordinates_train
		else:
			pwr_grids = (self.pwr_test_grids)
			city_grids = (self.city_test_grids)
			user_coordinates = self.user_coordinates_test
			user_loc_grids = self.user_location_grids_test
		grid_keys = list(pwr_grids.keys())
		random.shuffle(grid_keys)

		for i in range(n_maps):
			
			grid_key = grid_keys[i%len(grid_keys)]
			pwr_grid = np.copy(pwr_grids[grid_key])
			city_grid = np.copy(city_grids[grid_key])
			user_loc_grid = np.copy(user_loc_grids[grid_key])
			if train:
				random_rotation = random.randint(0, 3)
			else:
				random_rotation = 0
			pwr_grid = np.rot90(pwr_grid, k = random_rotation)
			city_grid = np.rot90(city_grid, k = random_rotation)
			user_loc_grid = np.rot90(user_loc_grid, k = random_rotation)
			pwr_grid_all = np.copy(pwr_grid)



			width = pwr_grid.shape[0]
			length = pwr_grid.shape[1]

			# Take out only a square of size 140x140 from the map
			if length > self.OBSERVATION_WIDTH:
				user_loc = np.unravel_index(np.argmax(user_loc_grid, axis=None), user_loc_grid.shape)
				x_start = np.maximum(0, (user_loc[1] - self.OBSERVATION_WIDTH + 1))
			else:
				x_start = 0
			x_stop = x_start + self.OBSERVATION_WIDTH
			if width > self.OBSERVATION_WIDTH:
				y_start = np.maximum(0, (user_loc[0] - self.OBSERVATION_WIDTH + 1))
			else:
				y_start = 0
			y_stop = y_start + self.OBSERVATION_WIDTH

			# Normalize the SINR observation
			pwr_ob = pwr_grid[y_start:y_stop, x_start:x_stop]
			city_ob = city_grid[y_start:y_stop, x_start:x_stop]
			user_loc_ob = user_loc_grid[y_start:y_stop, x_start:x_stop]


			city_map = np.copy(city_ob)
			pwr_ob_all = np.copy(pwr_ob)

			# Only retain a certain number of measurements
			pwr_ob, measured_coordinates, unmeasured_coordinates, measured_points, unmeasured_points = self.mask_observation(
				rate, pwr_ob, city_ob, user_coordinates[grid_key], x_start, y_start, random_walk)


			if kriging:
				K, eta = estimate_eta(measured_coordinates, pwr_ob_all[np.unravel_index(measured_points, pwr_ob_all.shape)], show_plot = display)
                                                                                                                 
				#print(K, eta)

				params = semivar_fit(measured_coordinates, pwr_ob_all[np.unravel_index(measured_points, pwr_ob_all.shape)], K, eta, show_plot = display)

				#if params[]
				#print(params)

				pred_pwr, pred_var = kriging_fit(measured_coordinates, unmeasured_coordinates, pwr_ob_all[np.unravel_index(measured_points, pwr_ob_all.shape)], K, eta, *params, show_plot = display)

				kriging_pwr = np.copy(pwr_ob_all)
				kriging_var = np.zeros(pwr_ob_all.shape) + 0.01

				kriging_pwr[np.unravel_index(unmeasured_points, pwr_ob_all.shape)] = pred_pwr.reshape(len(unmeasured_points), )
				kriging_var[np.unravel_index(unmeasured_points, pwr_ob_all.shape)] = pred_var.reshape(len(unmeasured_points), )

				reconstruct_means.append(kriging_pwr)

				reconstruct_probs.append(kriging_var)

			if include_reconstruct:
				# Use channel reconstruction to predict SINR
				Gamma_r, Sigma_r = reconstruct_channel(measured_points, unmeasured_points, pwr_ob_all)
				reconstruct_pwr = np.copy(pwr_ob_all)
				reconstruct_pwr[np.unravel_index(unmeasured_points, pwr_ob_all.shape)] = Gamma_r.reshape(len(unmeasured_points), )
				reconstruct_means.append(reconstruct_pwr)
				probability_grid_nan = np.empty(pwr_ob_all.shape)
				probability_grid_nan[:] = np.nan
				probability_grid = np.ndarray.astype(pwr_ob_all >= self.PWR_THRESHOLD, dtype = np.float)
				probability_grid[np.unravel_index(unmeasured_points, pwr_ob_all.shape)] = 1 - scipy.stats.norm(loc = Gamma_r.ravel(), scale = np.diagonal(Sigma_r)).cdf(np.ones(len(unmeasured_points)) * self.PWR_THRESHOLD)
				probability_grid_nan[np.unravel_index(unmeasured_points, pwr_ob_all.shape)] = 1 - scipy.stats.norm(loc = Gamma_r.ravel(), scale = np.diagonal(Sigma_r)).cdf(np.ones(len(unmeasured_points)) * self.PWR_THRESHOLD)
				reconstruction_prediction = np.ndarray.astype(pwr_ob_all >= self.PWR_THRESHOLD, dtype = np.float)
				reconstruction_prediction[np.unravel_index(unmeasured_points, pwr_ob_all.shape)] = Gamma_r.ravel() >= self.PWR_THRESHOLD
				reconstruct_probs.append([probability_grid, probability_grid_nan, reconstruction_prediction])

			if detrend:
				K, eta = estimate_eta(measured_coordinates, pwr_ob_all[np.unravel_index(measured_points, pwr_ob_all.shape)], show_plot = display)
				pwr_ob = self.detrend(pwr_ob, measured_coordinates, unmeasured_coordinates, measured_points, unmeasured_points, K, eta, measured_only = True)
				pwr_ob_all = self.detrend(pwr_ob_all, measured_coordinates, unmeasured_coordinates, measured_points, unmeasured_points, K, eta)


			# Normalize the observations
			city_ob = (city_ob - self.UAV_ALTITUDE) / (self.MAX_HEIGHT - self.UAV_ALTITUDE)
			pwr_ob = (pwr_ob - self.MIN_PWR) / (self.MAX_PWR - self.MIN_PWR)

			if self.no_maps:
				ob = np.stack([pwr_ob], axis = 2)
				obs.append(ob)
			elif self.use_location:
				ob = np.stack([city_ob, pwr_ob, user_loc_ob], axis = 2)
				obs.append(ob)
			else:
				ob = np.stack([city_ob, pwr_ob], axis = 2)
				obs.append(ob)

			label = pwr_grid_all[y_start:y_stop, x_start:x_stop]
			pwr_map = pwr_grid_all[y_start:y_stop, x_start:x_stop]
			measurements_map = np.empty(pwr_map.shape)
			measurements_map[:] = np.nan
			measurements_map[np.unravel_index(measured_points, pwr_ob_all.shape)] = pwr_map[np.unravel_index(measured_points, pwr_ob_all.shape)]

			if boolean:
				label = label >= self.PWR_THRESHOLD
				label = label.astype(np.int8)
			else:
				if detrend:
					label = self.detrend(label, measured_coordinates, unmeasured_coordinates, measured_points, unmeasured_points, K, eta)
				label = (label - self.MIN_PWR) / (self.MAX_PWR - self.MIN_PWR)
				label = label.reshape(label.shape + (1,))
			labels.append(label)
			outdoors.append(np.asarray(city_ob < 0, np.int).reshape(pwr_ob.shape + (1,)))

			if detrend:
				pwr_map = self.detrend(pwr_map, measured_coordinates, unmeasured_coordinates, measured_points, unmeasured_points, K, eta)

			non_scaled_maps.append([pwr_map, city_map, measurements_map])

			if display:
				plot_grid(pwr_ob, title = 'Signal strength input')
				print(user_coordinates[grid_idx][0]-y_start, user_coordinates[grid_idx][1]-x_start)
				plot_grid(pwr_ob_all,  title = 'Full power')
				if include_reconstruct:
					plot_grid(reconstruct_pwr, title = 'Reconstructed Power')
				if kriging:
					plot_grid(kriging_pwr, title = 'Kriging Power')
				plot_grid(city_ob, title = '3D Map Input')
				plot_grid(user_loc_ob, title = 'User location grid')
				plot_grid(label.reshape(city_ob.shape), title = 'Label')
				plot_grid(np.asarray(city_ob < 0, np.int), title = 'Outdoors')

		if include_non_scaled and (include_reconstruct or kriging):
			return obs, labels, outdoors, non_scaled_maps, reconstruct_means, reconstruct_probs
		elif include_non_scaled:
			return obs, labels, outdoors, non_scaled_maps
		elif (include_reconstruct or kriging):
			return obs, reconstruct_means, reconstruct_probs
		else:
			return obs, labels, outdoors

	def detrend(self, pwr_ob, measured_coordinates, unmeasured_coordinates, measured_points, unmeasured_points, K, eta, measured_only = False):
		new_grid = np.copy(pwr_ob)

		# Detrend the measured coordinates first
		M = len(measured_points)
		h = -10 * np.log10(np.linalg.norm(measured_coordinates, axis = 1)).reshape((M, 1))
		A = np.concatenate([np.ones((M,1)), h], axis = 1)
		pl = A.dot(np.asarray([K, eta]).reshape((2, 1)))
		new_grid[np.unravel_index(measured_points, pwr_ob.shape)] = pwr_ob[np.unravel_index(measured_points, pwr_ob.shape)] - pl.T

		if not(measured_only):
			# Detrend the unmeasured coordinates
			M = len(unmeasured_points)
			h = -10 * np.log10(np.linalg.norm(unmeasured_coordinates, axis = 1)).reshape((M, 1))
			A = np.concatenate([np.ones((M,1)), h], axis = 1)
			pl = A.dot(np.asarray([K, eta]).reshape((2, 1)))
			new_grid[np.unravel_index(unmeasured_points, pwr_ob.shape)] = pwr_ob[np.unravel_index(unmeasured_points, pwr_ob.shape)] - pl.T

		return new_grid

	def get_power_from_sinr(self, sinr):
		return sinr + self.NOISE_LEVEL

	def scaled_to_raw(self, values):
		return values * (self.MAX_PWR - self.MIN_PWR) + self.MIN_PWR

	def scale_var_to_raw(self, values):
		return values * ((self.MAX_PWR - self.MIN_PWR)**2)

	def scale_sd_to_raw(self, values):
		return values * ((self.MAX_PWR - self.MIN_PWR))

	def get_sinr_from_power(self, power):
		return power - self.NOISE_LEVEL

	def mask_observation(self, rate, pwr_ob, city_ob, user_location, x_start = 0, y_start = 0, random_walk =  False,
		      				measured_coords = []):
		# rate: percentage of points that will be retained
		y_size, x_size = pwr_ob.shape

		# Index of all unmeasured coordinates
		all_points = list(range(y_size * x_size))

		# A list of all possible coordinates. Note that the coordinates are scaled by grid len
		all_coordinates = np.asarray(np.unravel_index(all_points, pwr_ob.shape)).T
		
		if len(measured_coords):
			measured_points = []
			unmeasured_points = []

			for p in all_points:
				coords = np.asarray(np.unravel_index(p, city_ob.shape))
				#print(measured_coords.shape, coords.shape, (measured_coords - coords).shape, "A")
				if np.min(np.sum(np.abs(measured_coords - coords), axis = -1)) == 0:
					measured_points.append(p)				

			for p in all_points:
				if not(p in measured_points):
					unmeasured_points.append(p)


		elif random_walk:
			measured_points, unmeasured_points = self.get_random_walk_trace(y_size, x_size, rate, city_ob, self.n_uavs)
		else:
			random.shuffle(all_points)

			n_measured_points = int(np.sum(city_ob <= self.UAV_ALTITUDE) * rate)

			measured_points = []
			unmeasured_points = []

			for p in all_points:
				coords = np.unravel_index(p, city_ob.shape)

				if city_ob[coords] <= self.UAV_ALTITUDE:
					measured_points.append(p)

				if len(measured_points) == n_measured_points:
					break

			for p in all_points:
				if not(p in measured_points):
					unmeasured_points.append(p)

		# All measured coordinates and visible signal map
		measured_coordinates = all_coordinates[measured_points, :].astype(np.int)
		masked_ob = -np.ones(pwr_ob.shape) * (self.MAX_PWR - self.MIN_PWR) + self.MIN_PWR
		masked_ob[measured_coordinates[:, 0], measured_coordinates[:, 1]] = pwr_ob[measured_coordinates[:, 0], measured_coordinates[:, 1]]

		# We know that all the indoor mesasurements are
		#masked_ob[city_ob > self.UAV_ALTITUDE] = pwr_ob[city_ob > self.UAV_ALTITUDE]


		# All measured coordinates in meters
		z = self.UAV_ALTITUDE - self.USER_ALTITUDE # z-coordinates
		all_coordinates[:, 0] = all_coordinates[:, 0] + y_start - user_location[0]
		all_coordinates[:, 1] = all_coordinates[:, 1] + x_start - user_location[1]
		all_coordinates = all_coordinates * self.GRID_LEN
		all_coordinates= np.concatenate([all_coordinates, np.ones((y_size*x_size, 1))*z], axis = 1) # Add z-coordinates


		measured_coordinates = all_coordinates[measured_points, :].astype(np.int)

		# All unmeasured coordinates in meters
		unmeasured_coordinates = all_coordinates[unmeasured_points, :].astype(np.int)



		return masked_ob, measured_coordinates, unmeasured_coordinates, measured_points, unmeasured_points

	def get_random_walk_trace(self, y_size, x_size, rate, city_ob, n_uavs):
		if self.variable_rw_momentum:
			random_walk_momentum = np.random.rand() * self.random_walk_momentum
		else:
			random_walk_momentum = self.random_walk_momentum

		measured_points = []
		unmeasured_points = []


		# Index of all unmeasured coordinates
		all_points = list(range(y_size * x_size))

		path_length = int(rate * y_size * x_size)

		# A list of all possible coordinates. Note that the coordinates are scaled by grid len
		all_coordinates = np.asarray(np.unravel_index(all_points, city_ob.shape)).T

		for _ in range(n_uavs):
			# Start from a random location instead of the middle of the map
			start = [random.randint(0, x_size-1), random.randint(0, y_size-1)]
			while 1:
				if city_ob[start[1], start[0]] < self.UAV_ALTITUDE:
					break
				else:
					start = [random.randint(0, x_size-1), random.randint(0, y_size-1)]

			current_point = start
			path = []
			past_action = random.randint(0, 3)
			tried_past_action = False
			while(1):
				if (random.random() <= random_walk_momentum) and not(tried_past_action):
					action = past_action
					tried_past_action = True
				else:
					action =random.randint(0, 3)

				if action == 0:
					# UP
					dx = 0; dy = -1
				elif action == 1:
					# DOWN
					dx = 0; dy = 1
				elif action == 2:
					# LEFT
					dx = -1; dy = 0
				elif action == 3:
					# RIGHT
					dx = 1; dy = 0

				if (current_point[0] + dx) < 0 or (current_point[0] + dx) > (x_size-1):
					pass
				elif (current_point[1] + dy) < 0 or (current_point[1] + dy) > (y_size-1):
					pass
				elif city_ob[current_point[1] + dy, current_point[0] + dx] > self.UAV_ALTITUDE:
					pass
				else:
					path.append([current_point[1], current_point[0]])
					measured_points.append(current_point[1] * x_size + current_point[0])

					if len(path) >= path_length:
						break
					current_point[0] = current_point[0] + dx
					current_point[1] = current_point[1] + dy
					past_action = action
					tried_past_action = False


		for p in all_points:
			if not(p in measured_points):
				unmeasured_points.append(p)


		return measured_points, unmeasured_points

if __name__ == '__main__':
	ds = Dataset()

	#ds.plot_all_grids()

	t = time.time()
	ds.sample_whole_maps(1, train = False, include_non_scaled = True, include_reconstruct = False,
						display = True, random_walk = True, boolean = False, kriging = False, detrend = False)
	print(time.time()-t)


	plt.show()
