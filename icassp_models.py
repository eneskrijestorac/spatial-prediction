import tensorflow as tf
import numpy as np

class UnetEncodeLayer(tf.keras.layers.Layer):
	def __init__(self, output_size, dropout_rate, activation_fn):
		super(UnetEncodeLayer, self).__init__()
		self.conv1a = tf.keras.layers.Conv2D(output_size, strides=1, kernel_size=3, activation=activation_fn, padding='same')
		self.drop1a = tf.keras.layers.Dropout(dropout_rate)
		self.batch1a = tf.keras.layers.BatchNormalization()
		self.conv2a = tf.keras.layers.Conv2D(output_size, strides=1, kernel_size=3, activation=activation_fn, padding='same')
		self.drop2a = tf.keras.layers.Dropout(dropout_rate)
		self.batch2a = tf.keras.layers.BatchNormalization()
		self.maxpool = tf.keras.layers.MaxPool2D(pool_size = (4,4), strides=(4,4), padding='same')
		#self.batch3a = tf.keras.layers.BatchNormalization()

	def call(self, input_tensor, training = False):
		x = self.conv1a(input_tensor)
		x = self.drop1a(x)
		x = self.batch1a(x, training = training)
		x = self.conv2a(x)
		x = self.drop2a(x)
		x = self.batch2a(x)
		y = self.maxpool(x)
		#y = self.batch3a(y)
		return x, y
	

class UnetDenseLayer(tf.keras.layers.Layer):
	def __init__(self, output_size_1, output_size_2, activation_fn, dropout_rate):
		super(UnetDenseLayer, self).__init__()
		self.flatten1a = tf.keras.layers.Flatten()
		self.dense1a = tf.keras.layers.Dense(output_size_1, activation=activation_fn)
		self.drop1a = tf.keras.layers.Dropout(dropout_rate)
		self.batch1a = tf.keras.layers.BatchNormalization()
		self.dense2a = tf.keras.layers.Dense(output_size_2, activation=activation_fn)
		self.drop2a = tf.keras.layers.Dropout(dropout_rate)
		self.batch2a = tf.keras.layers.BatchNormalization()

	def call(self, input_tensor, training = False):
		x = self.flatten1a(input_tensor)
		x = self.dense1a(x)
		x = self.drop1a(x)
		x = self.batch1a(x, training = training)
		x = self.dense2a(x)
		x = self.drop2a(x)
		x = self.batch2a(x, training = training) # TODO: Reconsider the placement of these batchnormalization layers

		return x


class UnetDecodeLayer(tf.keras.layers.Layer):
	def __init__(self, output_size_1, output_size_2, dropout_rate, activation_fn):
		super(UnetDecodeLayer, self).__init__()
		self.output_size_2 = output_size_2
		self.upsample1a = tf.keras.layers.UpSampling2D((4,4))
		self.concat1a = tf.keras.layers.Concatenate(axis = -1)
		self.conv1a = tf.keras.layers.Conv2D(output_size_1, strides=1, kernel_size=3, activation=activation_fn, padding='same')
		self.drop1a = tf.keras.layers.Dropout(dropout_rate)
		self.batch1a = tf.keras.layers.BatchNormalization()
		self.conv2a = tf.keras.layers.Conv2D(output_size_2, strides=1, kernel_size=3, activation=activation_fn, padding='same')
		self.drop2a = tf.keras.layers.Dropout(dropout_rate)
		self.batch2a = tf.keras.layers.BatchNormalization()


	def call(self, input_tensor_1, input_tensor_2,training = False):
		x = self.upsample1a(input_tensor_1)
		x = self.concat1a([x, input_tensor_2])
		#x = x # 1
		#x = input_tensor_2 # 2
		x = self.conv1a(x)
		x = self.drop1a(x)
		x = self.batch1a(x, training = training)
		x = self.conv2a(x)
		x = self.drop2a(x)
		x = self.batch2a(x)
		return x

	def compute_output_shape(self, input_shape):
		input_shape_2 = input_shape[1].as_list()
		input_shape_2[-1] = self.output_size_2
		return input_shape_2

class UnetFinalLayer(tf.keras.layers.Layer):
	def __init__(self, output_size_1, output_size_2, activation_fn, sd):
		super(UnetFinalLayer, self).__init__()
		self.sd = sd 
		self.conv1a = tf.keras.layers.Conv2D(output_size_1, strides=1, kernel_size=3, activation=activation_fn, padding='same')
		self.conv2a = tf.keras.layers.Conv2D(output_size_2, strides=1, kernel_size=3, activation=None, padding='same')

	def call(self, input_tensor_1, training = False):
		x = self.conv1a(input_tensor_1)
		x = self.conv2a(x)
		return x

class UnetModel(tf.keras.Model):
	def __init__(self, input_shape, sd = False, dropout_rate = 0.0, scale = 1, activation_fn = tf.nn.relu, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.activation_fn = activation_fn
		self.sd = sd
		self.dropout_rate = dropout_rate
		self.scale = scale

		# Encode layers
		print("Unet Layers Shapes:")
		print(input_shape)
		self.encode_layer_1 = UnetEncodeLayer(int(64 * self.scale), dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		encode_layer_1_output_shape = self.encode_layer_1.compute_output_shape(input_shape = list(input_shape))
		print(encode_layer_1_output_shape)
		self.encode_layer_2 = UnetEncodeLayer(int(128 * self.scale), dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		encode_layer_2_output_shape = self.encode_layer_2.compute_output_shape(input_shape = encode_layer_1_output_shape[1])
		print(encode_layer_2_output_shape)
		self.encode_layer_3 = UnetEncodeLayer(int(256 * self.scale), dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		encode_layer_3_output_shape = self.encode_layer_3.compute_output_shape(input_shape = encode_layer_2_output_shape[1])
		print(encode_layer_3_output_shape)

		# Dense layer
		self.dense_layer= UnetDenseLayer(output_size_1=int(512 * self.scale), output_size_2=np.prod(encode_layer_3_output_shape[1][1:].as_list()), dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		dense_layer_output_shape = self.dense_layer.compute_output_shape(input_shape = encode_layer_3_output_shape[1])
		print(dense_layer_output_shape)

		# Reshape layer
		self.reshape_layer = tf.keras.layers.Reshape(encode_layer_3_output_shape[1][1:])
		reshape_layer_output_shape = self.reshape_layer.compute_output_shape(input_shape = dense_layer_output_shape)
		print(reshape_layer_output_shape)


		# Decode layers
		self.decode_layer_1 = UnetDecodeLayer(int(128 * self.scale), int(128 * self.scale), dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		decode_layer_1_output_shape = self.decode_layer_1.compute_output_shape((reshape_layer_output_shape, encode_layer_3_output_shape[0]))
		print(decode_layer_1_output_shape)

		self.decode_layer_2 = UnetDecodeLayer(int(64 * self.scale), int(64 * self.scale), dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		decode_layer_2_output_shape = self.decode_layer_2.compute_output_shape((decode_layer_1_output_shape, encode_layer_2_output_shape[0]))
		print(decode_layer_2_output_shape)

		self.decode_layer_3 = UnetDecodeLayer(int(32 * self.scale), int(32 * self.scale), dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		decode_layer_3_output_shape = self.decode_layer_3.compute_output_shape((decode_layer_2_output_shape, encode_layer_1_output_shape[0]))

		# Final layers
		self.final_layer = UnetFinalLayer(int(8 * self.scale), 1, sd=sd, activation_fn=self.activation_fn)
		final_layer_shape = self.final_layer.compute_output_shape(decode_layer_3_output_shape)
		print(final_layer_shape)
		self.final_layer_shape = final_layer_shape

	def call(self, input, training=False):
		# Encode layers
		encode_layer_1_out = self.encode_layer_1(input, training=training)
		encode_layer_2_out = self.encode_layer_2(encode_layer_1_out[1], training=training)
		encode_layer_3_out = self.encode_layer_3(encode_layer_2_out[1], training=training)

		# Dense layer
		dense_layer_out = self.dense_layer(encode_layer_3_out[1], training=training)

		# Reshape layer
		reshape_layer_out = self.reshape_layer(dense_layer_out, training=training)

		# Decode layers
		decode_layer_1_out = self.decode_layer_1(reshape_layer_out, encode_layer_3_out[0], training=training)
		decode_layer_2_out = self.decode_layer_2(decode_layer_1_out, encode_layer_2_out[0], training=training)
		decode_layer_3_out = self.decode_layer_3(decode_layer_2_out, encode_layer_1_out[0], training=training)

		# Final layers
		final_layer_out = self.final_layer(decode_layer_3_out, training=training)

		return final_layer_out
