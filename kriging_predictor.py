from semivar import *
import pickle

class KrigingPredictor:

    def __init__(self, ds, use_pretrained_params = False):
        self.use_pretrained_params = False
        self.ds = ds

    def convert_coords_to_cartesian(self, coords):
        coords_cartesian = coords * self.ds.GRID_LEN # Multiply coordinates by grid size
        z = self.ds.UAV_ALTITUDE - self.ds.USER_ALTITUDE # z-coordinates
        coords_z = np.ones((coords_cartesian.shape[0], 1)) * z
        coords_cartesian = np.concatenate([coords_cartesian, coords_z], axis = 1)
        return coords_cartesian
    
    def train_pathloss_params(self, n_maps = 40, n_points = 400, save_params = True, load_params = False):
        if not(load_params):
            Ks = []
            etas = []
            shadowing_params_1 = []
            shadowing_params_2 = []
            for _ in range(n_maps):
                # Sample some measurements from each map
                all_pwr_measurements = []
                all_measured_coords = []

                # Sample a random map
                ss_grids, _, _ = self.ds.sample_maps_per_scen(scenario_idx = None , transmitter_idx = None, test_dataset = False)
                pwr_grid = ss_grids[0]

                # Sample random measurements
                measured_coords = np.random.randint(0, pwr_grid.shape[0], (n_points,2))

                # Power measurements
                pwr_measurements = pwr_grid[measured_coords[:, 0], measured_coords[:, 1]]

                # Store the coords and measurements
                user_location = np.unravel_index(pwr_grid.argmax(), pwr_grid.shape)
                measured_coords_offset = measured_coords - user_location
                all_pwr_measurements = all_pwr_measurements + list(pwr_measurements)
                all_measured_coords.append(measured_coords_offset)

                # Convert all coords into cartesian
                all_measured_coords = np.concatenate(all_measured_coords, axis = 0)
                measured_coords_cartesian = self.convert_coords_to_cartesian(all_measured_coords)

                print(measured_coords_cartesian.shape, np.asarray(all_pwr_measurements).shape)

                K, eta = estimate_eta(measured_coords_cartesian, np.asarray(all_pwr_measurements), show_plot = True)
                
                Ks.append(K)
                etas.append(eta)

                # Estimate the Kriging fit parameters
                shadowing_params = semivar_fit(measured_coords_cartesian, np.asarray(all_pwr_measurements), K, eta, show_plot=True)
                shadowing_params_1.append(shadowing_params[0])
                shadowing_params_2.append(shadowing_params[1])

                self.K = np.mean(Ks)
                self.eta = np.mean(etas)
                print("K, eta", self.K, self.eta)
            self.shadowing_params = (np.mean(shadowing_params_1), np.mean(shadowing_params_2))
            pathloss_params = (self.K, self.eta)

            if save_params:
                with open('pickles/pathloss_params.pickle', 'wb') as handle:
                    pickle.dump(pathloss_params, handle, protocol=pickle.HIGHEST_PROTOCOL)
                with open('pickles/shadowing_params.pickle', 'wb') as handle:
                    pickle.dump(self.shadowing_params, handle, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            with open('pickles/pathloss_params.pickle', 'rb') as handle:
                self.K, self.eta = pickle.load(handle)
            with open('pickles/shadowing_params.pickle', 'rb') as handle:
                self.shadowing_params = pickle.load(handle)

    def predict_signal_strength(self, measured_coords, true_pwr_grid):
        # Args:
        # measured_coords : measured coords where first each column correspond to the axis of power grid
        # true_pwr_grid : true power grid


        # Find the unmeasured coords
        unmeasured_coords = []
        for ii in range(true_pwr_grid.shape[0]):
            for jj in range(true_pwr_grid.shape[1]):
                unmeasured_coords.append([ii, jj])
        unmeasured_coords = np.asarray(unmeasured_coords)
        distance_to_measured = np.linalg.norm(np.expand_dims(unmeasured_coords, axis = 2) - np.expand_dims(measured_coords.T, axis=0), axis = 1)
        min_distance_to_measured = np.min(distance_to_measured, axis = 1)
        unmeasured_coords = unmeasured_coords[min_distance_to_measured > 0, :]

        # Power measurements
        pwr_measurements = true_pwr_grid[measured_coords[:, 0], measured_coords[:, 1]]

        # User location
        user_location = np.unravel_index(true_pwr_grid.argmax(), true_pwr_grid.shape)

        # Ofset coords by user location
        measured_coords_offset = measured_coords - user_location
        unmeasured_coords_offset = unmeasured_coords - user_location
        
        # Get cartesian coords
        unmeasured_coords_cartesian = self.convert_coords_to_cartesian(unmeasured_coords_offset)
        measured_coords_cartesian = self.convert_coords_to_cartesian(measured_coords_offset)

        

        # Predict the power at the unmeasured coordinates
        pred_pwr, pred_var, pred_covar_matrix = kriging_fit(measured_coords_cartesian, unmeasured_coords_cartesian, pwr_measurements, 
                                         self.K, self.eta, *self.shadowing_params, show_plot = False)

        raw_value_pred = np.copy(true_pwr_grid)
        raw_value_pred[unmeasured_coords[:, 0], unmeasured_coords[:, 1]] = pred_pwr.flatten()
        variance = np.zeros(true_pwr_grid.shape)
        variance[unmeasured_coords[:, 0], unmeasured_coords[:, 1]] = pred_var.flatten()

        # The above covariance matrix is only for unmeasured locations
        # Pad the measured locations with zeros
        assert len(true_pwr_grid.shape) == 2 # Assert 2D
        full_covar_matrix = np.zeros((np.prod(true_pwr_grid.shape), np.prod(true_pwr_grid.shape)))
        ravel_unmeasured_coords = np.ravel_multi_index((unmeasured_coords[:, 0], unmeasured_coords[:, 1]), true_pwr_grid.shape)
        full_covar_matrix[np.ix_(ravel_unmeasured_coords, ravel_unmeasured_coords)] = pred_covar_matrix



        # Use this for debugging
        covar_matrix_for_single_point = np.zeros(true_pwr_grid.shape)
        covar_matrix_for_single_point[unmeasured_coords[:, 0], unmeasured_coords[:, 1]] = pred_covar_matrix[15, :]


        return raw_value_pred, variance, full_covar_matrix, covar_matrix_for_single_point

    