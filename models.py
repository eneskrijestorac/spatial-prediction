import tensorflow as tf
import numpy as np
import tensorflow_probability as tfp

class UnetEncodeLayer(tf.keras.layers.Layer):
	def __init__(self, output_size, dropout_rate, activation_fn, kernel_size = 3, pooling_size = 2):
		super(UnetEncodeLayer, self).__init__()
		self.conv1a = tf.keras.layers.Conv2D(output_size, strides=1, kernel_size=kernel_size, activation=activation_fn, padding='same', kernel_regularizer=tf.keras.regularizers.L2(0.000))
		self.drop1a = tf.keras.layers.Dropout(dropout_rate)
		self.batch1a = tf.keras.layers.BatchNormalization()
		self.conv2a = tf.keras.layers.Conv2D(output_size, strides=1, kernel_size=kernel_size, activation=activation_fn, padding='same', kernel_regularizer=tf.keras.regularizers.L2(0.000))
		self.drop2a = tf.keras.layers.Dropout(dropout_rate)
		self.batch2a = tf.keras.layers.BatchNormalization()
		self.maxpool = tf.keras.layers.MaxPool2D(pool_size = (pooling_size,pooling_size), strides=(pooling_size,pooling_size), padding='same')
		#self.batch3a = tf.keras.layers.BatchNormalization()

	def call(self, input_tensor, training = False):
		x = self.conv1a(input_tensor)
		x = self.drop1a(x)
		x = self.batch1a(x, training = training)
		x = self.conv2a(x)
		x = self.drop2a(x)
		x = self.batch2a(x)
		y = self.maxpool(x)
		#y = self.batch3a(y)
		return x, y


class UnetDenseLayer(tf.keras.layers.Layer):
	def __init__(self, output_size_1, output_size_2, activation_fn, dropout_rate):
		super(UnetDenseLayer, self).__init__()
		self.flatten1a = tf.keras.layers.Flatten()
		self.dense1a = tf.keras.layers.Dense(output_size_1, activation=activation_fn, kernel_regularizer=tf.keras.regularizers.L2(0.000))
		self.drop1a = tf.keras.layers.Dropout(dropout_rate)
		self.batch1a = tf.keras.layers.BatchNormalization()
		self.dense2a = tf.keras.layers.Dense(output_size_2, activation=activation_fn, kernel_regularizer=tf.keras.regularizers.L2(0.000))
		self.drop2a = tf.keras.layers.Dropout(dropout_rate)
		self.batch2a = tf.keras.layers.BatchNormalization()

	def call(self, input_tensor, training = False):
		x = self.flatten1a(input_tensor)
		x = self.dense1a(x)
		x = self.drop1a(x)
		x = self.batch1a(x, training = training)
		x = self.dense2a(x)
		x = self.drop2a(x)
		x = self.batch2a(x, training = training) # TODO: Reconsider the placement of these batchnormalization layers

		return x


class UnetCovarFinalLayer(tf.keras.layers.Layer):
	def __init__(self, output_size_2, activation_fn, dropout_rate):
		super(UnetCovarFinalLayer, self).__init__()
		self.flatten1a = tf.keras.layers.Flatten()
		self.conv2a = tf.keras.layers.Convolution2D(output_size_2, strides=1, kernel_size=3, activation=None, padding='same')

	def call(self, input_tensor, training = False):
		input_tensor = self.conv2a(input_tensor)

		# Cast to float64 to avoid rounding errors
		input_tensor = tf.cast(input_tensor, tf.float64)
		
		# Calculate the distance matrix
		#x = tf.transpose(input_tensor, [0,1,2,3])
		x = input_tensor
		x = tf.reshape(x, [x.shape[0],x.shape[1]*x.shape[2], -1])
		r = tf.reduce_sum(tf.square(x), 2)
		r = tf.reshape(r, [r.shape[0], -1, 1])
		r_T = tf.transpose(r, [0,2,1])
		D = r + r_T
		inner_product = 2 * tf.linalg.matmul(x, x, transpose_b=True)
		D = r + r_T - inner_product


		# Use the calculated distance matrix to calculate the covariance
		covariance = tf.exp((-1/2)*(D)) + 0.1*tf.eye(D.shape[1], batch_shape=[D.shape[0]], dtype=tf.float64)
		return covariance

	# def compute_output_shape(self, input_shape):
	# 	input_shape_2 = input_shape[1].as_list()
	# 	input_shape_2[-1] = self.output_size_2
	# 	return input_shape_2

class UnetDecodeLayer(tf.keras.layers.Layer):
	def __init__(self, output_size_1, output_size_2, dropout_rate, activation_fn, kernel_size = 3, pooling_size = 2):
		super(UnetDecodeLayer, self).__init__()
		self.output_size_2 = output_size_2
		self.upsample1a = tf.keras.layers.UpSampling2D((pooling_size,pooling_size))
		self.concat1a = tf.keras.layers.Concatenate(axis = -1)
		self.conv1a = tf.keras.layers.Conv2D(output_size_1, strides=1, kernel_size=kernel_size, activation=activation_fn, padding='same', kernel_regularizer=tf.keras.regularizers.L2(0.000))
		self.drop1a = tf.keras.layers.Dropout(dropout_rate)
		self.batch1a = tf.keras.layers.BatchNormalization()
		self.conv2a = tf.keras.layers.Conv2D(output_size_2, strides=1, kernel_size=kernel_size, activation=activation_fn, padding='same', kernel_regularizer=tf.keras.regularizers.L2(0.000))
		self.drop2a = tf.keras.layers.Dropout(dropout_rate)
		self.batch2a = tf.keras.layers.BatchNormalization()


	def call(self, input_tensor_1, input_tensor_2,training = False):
		x = self.upsample1a(input_tensor_1)
		x = self.concat1a([x, input_tensor_2])
		#x = x # 1
		#x = input_tensor_2 # 2
		x = self.conv1a(x)
		x = self.drop1a(x)
		x = self.batch1a(x, training = training)
		x = self.conv2a(x)
		x = self.drop2a(x)
		x = self.batch2a(x)
		return x

	def compute_output_shape(self, input_shape):
		input_shape_2 = input_shape[1].as_list()
		input_shape_2[-1] = self.output_size_2
		return input_shape_2


class UnetFinalLayerSD(tf.keras.layers.Layer):
	def __init__(self, output_size_1, output_size_2, activation_fn, pooling_size = 2):
		super(UnetFinalLayerSD, self).__init__()
		self.pooling_size = pooling_size
		self.upsample1a = tf.keras.layers.UpSampling2D((pooling_size,pooling_size))
		self.conv1a = tf.keras.layers.Conv2D(output_size_1, strides=1, kernel_size=3, activation=activation_fn, padding='same', kernel_regularizer=tf.keras.regularizers.L2(0.000))
		self.conv2a = tf.keras.layers.Conv2D(output_size_2, strides=1, kernel_size=3, activation=None, padding='same', kernel_regularizer=tf.keras.regularizers.L2(0.000))

	def call(self, input_tensor_1, training = False):
		
		x = self.conv1a(input_tensor_1)
		x = self.conv2a(x)
		x = self.upsample1a(x)
		return x

class UnetFinalLayer(tf.keras.layers.Layer):
	def __init__(self, output_size_1, output_size_2, activation_fn, sd):
		super(UnetFinalLayer, self).__init__()
		self.sd = sd 
		self.conv1a = tf.keras.layers.Conv2D(output_size_1, strides=1, kernel_size=3, activation=activation_fn, padding='same', kernel_regularizer=tf.keras.regularizers.L2(0.000))
		self.conv2a = tf.keras.layers.Conv2D(output_size_2, strides=1, kernel_size=3, activation=None, padding='same', kernel_regularizer=tf.keras.regularizers.L2(0.000))

	def call(self, input_tensor_1, training = False):
		x = self.conv1a(input_tensor_1)
		x = self.conv2a(x)
		return x


class CovarFinalLayer(tf.keras.layers.Layer):
	def __init__(self,):
		super(CovarFinalLayer, self).__init__()
	
	def call(self, input_tensor):
		# Cast to float64 to avoid rounding errors
		input_tensor = tf.cast(input_tensor, tf.float64)
		
		# Calculate the distance matrix
		#x = tf.transpose(input_tensor, [0,1,2,3])
		print(input_tensor.shape, "input_tensor.shape")
		x = input_tensor[:, :, :, 1:]
		x = tf.reshape(x, [x.shape[0],x.shape[1]*x.shape[2], -1])
		r = tf.reduce_sum(tf.square(x), 2)
		r = tf.reshape(r, [r.shape[0], -1, 1])
		r_T = tf.transpose(r, [0,2,1])
		D = r + r_T
		inner_product = 2 * tf.linalg.matmul(x, x, transpose_b=True)
		D = r + r_T - inner_product

		# y = input_tensor[:, :, :, 0]
		# y = tf.reshape(y, [y.shape[0],y.shape[1]*y.shape[2]])
		# diag_uncertainty = tf.linalg.diag(y)

		#print(diag_uncertainty.shape, "diag_uncertainty.shape")

		# Use the calculated distance matrix to calculate the covariance
		covariance = tf.exp((-1/2)*(D)/ 0.1) + 0.1*tf.eye(D.shape[1], batch_shape=[D.shape[0]], dtype=tf.float64)# + diag_uncertainty
		return covariance


class CovarFinalLayer(tf.keras.layers.Layer):
	def __init__(self,):
		super(CovarFinalLayer, self).__init__()
	
	def call(self, input_tensor):
		# Cast to float64 to avoid rounding errors
		input_tensor = tf.cast(input_tensor, tf.float64)
		
		# Calculate the distance matrix
		#x = tf.transpose(input_tensor, [0,1,2,3])
		print(input_tensor.shape, "input_tensor.shape")
		x = input_tensor[:, :, :, 1:]
		x = tf.reshape(x, [x.shape[0],x.shape[1]*x.shape[2], -1])
		r = tf.reduce_sum(tf.square(x), 2)
		r = tf.reshape(r, [r.shape[0], -1, 1])
		r_T = tf.transpose(r, [0,2,1])
		D = r + r_T
		inner_product = 2 * tf.linalg.matmul(x, x, transpose_b=True)
		D = r + r_T - inner_product

		# y = input_tensor[:, :, :, 0]
		# y = tf.reshape(y, [y.shape[0],y.shape[1]*y.shape[2]])
		# diag_uncertainty = tf.linalg.diag(y)

		#print(diag_uncertainty.shape, "diag_uncertainty.shape")

		# Use the calculated distance matrix to calculate the covariance
		covariance = tf.exp((-1/2)*(D)/ 0.1) + 0.1*tf.eye(D.shape[1], batch_shape=[D.shape[0]], dtype=tf.float64)# + diag_uncertainty
		return covariance
class UnetModel(tf.keras.Model):
	def __init__(self, input_shape, sd = False, dropout_rate = 0.0, scale = 1, activation_fn = tf.nn.relu, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.activation_fn = activation_fn
		self.sd = sd
		self.dropout_rate = dropout_rate
		self.scale = scale

		# Encode layers
		print("Unet Layers Shapes:")
		print(input_shape)
		self.encode_layer_1 = UnetEncodeLayer(64 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		encode_layer_1_output_shape = self.encode_layer_1.compute_output_shape(input_shape = list(input_shape))
		print(encode_layer_1_output_shape)
		self.encode_layer_2 = UnetEncodeLayer(128 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		encode_layer_2_output_shape = self.encode_layer_2.compute_output_shape(input_shape = encode_layer_1_output_shape[1])
		print(encode_layer_2_output_shape)
		self.encode_layer_3 = UnetEncodeLayer(256 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		encode_layer_3_output_shape = self.encode_layer_3.compute_output_shape(input_shape = encode_layer_2_output_shape[1])
		print(encode_layer_3_output_shape)

		# Dense layer
		self.dense_layer= UnetDenseLayer(output_size_1=512, output_size_2=96*96, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		dense_layer_output_shape = self.dense_layer.compute_output_shape(input_shape = encode_layer_3_output_shape[1])
		print(dense_layer_output_shape)

		# Reshape layer
		self.reshape_layer = tf.keras.layers.Reshape(encode_layer_3_output_shape[1][1:])
		reshape_layer_output_shape = self.reshape_layer.compute_output_shape(input_shape = dense_layer_output_shape)
		print(reshape_layer_output_shape)


		# Decode layers
		self.decode_layer_1 = UnetDecodeLayer(128 * self.scale, 128 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		decode_layer_1_output_shape = self.decode_layer_1.compute_output_shape((reshape_layer_output_shape, encode_layer_3_output_shape[0]))
		print(decode_layer_1_output_shape)

		self.decode_layer_2 = UnetDecodeLayer(64 * self.scale, 64 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		decode_layer_2_output_shape = self.decode_layer_2.compute_output_shape((decode_layer_1_output_shape, encode_layer_2_output_shape[0]))
		print(decode_layer_2_output_shape)

		if not(sd):
			self.decode_layer_3 = UnetDecodeLayer(32 * self.scale, 32 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
			decode_layer_3_output_shape = self.decode_layer_3.compute_output_shape((decode_layer_2_output_shape, encode_layer_1_output_shape[0]))

			# Final layers
			self.final_layer = UnetFinalLayer(8 * self.scale, 1, sd=sd, activation_fn=self.activation_fn)
			final_layer_shape = self.final_layer.compute_output_shape(decode_layer_3_output_shape)
			print(final_layer_shape)
		else:
			self.final_layer = UnetFinalLayerSD(8 * self.scale, 1, sd=sd, activation_fn=self.activation_fn, pooling_size=po)
			final_layer_shape = self.final_layer.compute_output_shape(decode_layer_2_output_shape)
			print(final_layer_shape)			



	def call(self, input, training=False):
		# Encode layers
		encode_layer_1_out = self.encode_layer_1(input, training=training)
		encode_layer_2_out = self.encode_layer_2(encode_layer_1_out[1], training=training)
		encode_layer_3_out = self.encode_layer_3(encode_layer_2_out[1], training=training)

		# Dense layer
		dense_layer_out = self.dense_layer(encode_layer_3_out[1], training=training)

		# Reshape layer
		reshape_layer_out = self.reshape_layer(dense_layer_out, training=training)

		# Decode layers
		decode_layer_1_out = self.decode_layer_1(reshape_layer_out, encode_layer_3_out[0], training=training)
		decode_layer_2_out = self.decode_layer_2(decode_layer_1_out, encode_layer_2_out[0], training=training)
		decode_layer_3_out = self.decode_layer_3(decode_layer_2_out, encode_layer_1_out[0], training=training)

		# Final layers
		final_layer_out = self.final_layer(decode_layer_3_out, training=training)

		return final_layer_out


class UnetModelDeep(tf.keras.Model):
	def __init__(self, input_shape, sd = False, dropout_rate = 0.0, covar = False, scale = 1, output_layers = 1, activation_fn = 'relu', *args, **kwargs):
		super().__init__(*args, **kwargs)
		if activation_fn == "relu":
			self.activation_fn = tf.nn.relu
		elif activation_fn == "tanh":
			self.activation_fn = tf.nn.tanh
		self.sd = sd
		self.dropout_rate = dropout_rate
		self.scale = scale
		self.covar = covar
		self.output_layers = output_layers
		kernel_size = 4
		pooling_size = 2



		# Encode layers
		print("Unet Layers Shapes:")
		print(input_shape)
		self.encode_layer_1 = UnetEncodeLayer(16 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		encode_layer_1_output_shape = self.encode_layer_1.compute_output_shape(input_shape = list(input_shape))
		print(encode_layer_1_output_shape)
		self.encode_layer_2 = UnetEncodeLayer(32 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		encode_layer_2_output_shape = self.encode_layer_2.compute_output_shape(input_shape = encode_layer_1_output_shape[1])
		print(encode_layer_2_output_shape)
		self.encode_layer_3 = UnetEncodeLayer(64 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		encode_layer_3_output_shape = self.encode_layer_3.compute_output_shape(input_shape = encode_layer_2_output_shape[1])
		print(encode_layer_3_output_shape)
		self.encode_layer_4 = UnetEncodeLayer(128 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		encode_layer_4_output_shape = self.encode_layer_4.compute_output_shape(input_shape = encode_layer_3_output_shape[1])
		print(encode_layer_4_output_shape)


		# Dense layer
		self.dense_layer= UnetDenseLayer(output_size_1=512, output_size_2=np.prod(encode_layer_4_output_shape[1][1:].as_list()), dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		dense_layer_output_shape = self.dense_layer.compute_output_shape(input_shape = encode_layer_4_output_shape[1])
		print(dense_layer_output_shape)

		# Reshape layer
		self.reshape_layer = tf.keras.layers.Reshape(encode_layer_4_output_shape[1][1:])
		reshape_layer_output_shape = self.reshape_layer.compute_output_shape(input_shape = dense_layer_output_shape)
		print(reshape_layer_output_shape, "reshape_layer_output_shape")


		# Decode layers
		self.decode_layer_0 = UnetDecodeLayer(128 * self.scale, 128 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		decode_layer_0_output_shape = self.decode_layer_0.compute_output_shape((reshape_layer_output_shape, encode_layer_4_output_shape[0]))
		print(decode_layer_0_output_shape)

		self.decode_layer_1 = UnetDecodeLayer(64 * self.scale, 64 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		decode_layer_1_output_shape = self.decode_layer_1.compute_output_shape((decode_layer_0_output_shape, encode_layer_3_output_shape[0]))
		print(decode_layer_1_output_shape)

		if not(sd):
			self.decode_layer_2 = UnetDecodeLayer(32 * self.scale, 32 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
			decode_layer_2_output_shape = self.decode_layer_2.compute_output_shape((decode_layer_1_output_shape, encode_layer_2_output_shape[0]))
			print(decode_layer_2_output_shape)

			self.decode_layer_3 = UnetDecodeLayer(16 * self.scale, 16 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
			decode_layer_3_output_shape = self.decode_layer_3.compute_output_shape((decode_layer_2_output_shape, encode_layer_1_output_shape[0]))
			print(decode_layer_3_output_shape)

			# Final layers
			self.final_layer = UnetFinalLayer(8 * self.scale, self.output_layers, sd=sd, activation_fn=self.activation_fn)
			final_layer_shape = self.final_layer.compute_output_shape(decode_layer_3_output_shape)
			print(final_layer_shape)
		else:
			self.final_layer = UnetFinalLayerSD(8 * self.scale, self.output_layers, activation_fn=self.activation_fn, pooling_size=pooling_size*2)
			final_layer_shape = self.final_layer.compute_output_shape(decode_layer_1_output_shape)
			print(final_layer_shape)		

		# Final layers for covar calculation
		if self.covar:
			self.covar_layer = CovarFinalLayer()

	def call(self, input, training=False):
		# Encode layers
		encode_layer_1_out = self.encode_layer_1(input, training=training)
		encode_layer_2_out = self.encode_layer_2(encode_layer_1_out[1], training=training)
		encode_layer_3_out = self.encode_layer_3(encode_layer_2_out[1], training=training)
		encode_layer_4_out = self.encode_layer_4(encode_layer_3_out[1], training=training)

		# Dense layer
		dense_layer_out = self.dense_layer(encode_layer_4_out[1], training=training)

		# Reshape layer
		reshape_layer_out = self.reshape_layer(dense_layer_out, training=training)

		# Decode layers
		decode_layer_0_out = self.decode_layer_0(reshape_layer_out, encode_layer_4_out[0], training=training)
		decode_layer_1_out = self.decode_layer_1(decode_layer_0_out, encode_layer_3_out[0], training=training)
		
		if not(self.sd):
			decode_layer_2_out = self.decode_layer_2(decode_layer_1_out, encode_layer_2_out[0], training=training)
			decode_layer_3_out = self.decode_layer_3(decode_layer_2_out, encode_layer_1_out[0], training=training)

			# Final layers
			final_layer_out = self.final_layer(decode_layer_3_out, training=training)

			# Covar final layer
			if self.covar:
				final_layer_out = self.covar_layer(final_layer_out)
		else:
			# Final layers
			final_layer_out = self.final_layer(decode_layer_1_out, training=training)	

		return final_layer_out


class UnetModelKernel(tf.keras.Model):
	def __init__(self, input_shape, sd = False, dropout_rate = 0.0, scale = 1, activation_fn = tf.nn.relu, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.activation_fn = activation_fn
		self.sd = sd
		self.dropout_rate = dropout_rate
		self.scale = scale
		kernel_size = 4
		pooling_size = 1

		# Encode layers
		print("Unet Layers Shapes:")
		print(input_shape)
		self.encode_layer_1 = UnetEncodeLayer(4 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		encode_layer_1_output_shape = self.encode_layer_1.compute_output_shape(input_shape = list(input_shape))
		print(encode_layer_1_output_shape)
		self.encode_layer_2 = UnetEncodeLayer(16 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		encode_layer_2_output_shape = self.encode_layer_2.compute_output_shape(input_shape = encode_layer_1_output_shape[1])
		print(encode_layer_2_output_shape)
		self.encode_layer_3 = UnetEncodeLayer(32 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		encode_layer_3_output_shape = self.encode_layer_3.compute_output_shape(input_shape = encode_layer_2_output_shape[1])
		print(encode_layer_3_output_shape)
		# self.encode_layer_4 = UnetEncodeLayer(64 * 64 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		# encode_layer_4_output_shape = self.encode_layer_4.compute_output_shape(input_shape = encode_layer_3_output_shape[1])
		# print(encode_layer_4_output_shape)


		# Dense layer
		self.final_layer= UnetCovarFinalLayer(output_size_2=64, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		# dense_layer_output_shape = self.final_layer.compute_output_shape(input_shape = encode_layer_4_output_shape[1])
		# print(dense_layer_output_shape)

		# # Reshape layer
		# self.reshape_layer = tf.keras.layers.Reshape(encode_layer_4_output_shape[1][1:])
		# reshape_layer_output_shape = self.reshape_layer.compute_output_shape(input_shape = dense_layer_output_shape)
		# print(reshape_layer_output_shape, "reshape_layer_output_shape")


		# # Decode layers
		# self.decode_layer_0 = UnetDecodeLayer(96 * 4 * self.scale, 96 * 4  * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		# decode_layer_0_output_shape = self.decode_layer_0.compute_output_shape((reshape_layer_output_shape, encode_layer_4_output_shape[0]))
		# print(decode_layer_0_output_shape)

		# self.decode_layer_1 = UnetDecodeLayer(96 * 8  * self.scale, 96 * 8  * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		# decode_layer_1_output_shape = self.decode_layer_1.compute_output_shape((decode_layer_0_output_shape, encode_layer_3_output_shape[0]))
		# print(decode_layer_1_output_shape)

		# self.decode_layer_2 = UnetDecodeLayer(96 * 16  * self.scale, 96 * 16  * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		# decode_layer_2_output_shape = self.decode_layer_2.compute_output_shape((decode_layer_1_output_shape, encode_layer_2_output_shape[0]))
		# print(decode_layer_2_output_shape)

		# self.decode_layer_3 = UnetDecodeLayer(96 * 32 * self.scale, 96 * 32 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		# decode_layer_3_output_shape = self.decode_layer_3.compute_output_shape((decode_layer_2_output_shape, encode_layer_1_output_shape[0]))

		# # Final layers
		# self.final_layer = UnetFinalLayer(96 * 96 * self.scale, 96 * 96, sd=sd, activation_fn=self.activation_fn)
		# final_layer_shape = self.final_layer.compute_output_shape(decode_layer_3_output_shape)
		# print(final_layer_shape)

	def call(self, input, training=False):
		# Encode layers
		encode_layer_1_out = self.encode_layer_1(input, training=training)
		encode_layer_2_out = self.encode_layer_2(encode_layer_1_out[1], training=training)
		encode_layer_3_out = self.encode_layer_3(encode_layer_2_out[1], training=training)
		#encode_layer_4_out = self.encode_layer_4(encode_layer_3_out[1], training=training)

		# Dense layer
		final_layer_out = self.final_layer(encode_layer_3_out[1], training=training)

		return final_layer_out