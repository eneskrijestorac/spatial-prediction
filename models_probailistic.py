import tensorflow as tf
import numpy as np
import tensorflow_probability as tfp

NUM_TRAIN_EXAMPLES = 600000 # TODO: Figure out what is the purpose of this number

kl_divergence_function = (lambda q, p, _: tfp.distributions.kl_divergence(q, p) /  # pylint: disable=g-long-lambda
						tf.cast(1, dtype=tf.float32))

class UnetEncodeLayer(tf.keras.layers.Layer):
	def __init__(self, output_size, dropout_rate, activation_fn, kernel_size = 3, pooling_size = 2):
		super(UnetEncodeLayer, self).__init__()
		#self.conv1a = tfp.layers.Convolution2DFlipout(output_size, strides=1, kernel_size=kernel_size, activation=activation_fn, 
		# 			kernel_divergence_fn=kl_divergence_function, padding='same')
		#self.conv2a = tfp.layers.Convolution2DFlipout(output_size, strides=1, kernel_size=kernel_size, activation=activation_fn, 
		# 			kernel_divergence_fn=kl_divergence_function, padding='same')
		self.conv1a = tf.keras.layers.Convolution2D(output_size, strides=1, kernel_size=kernel_size, 
						activation=activation_fn, padding='same')
		self.conv2a = tf.keras.layers.Convolution2D(output_size, strides=1, kernel_size=kernel_size, activation=activation_fn, 
						padding='same')
		self.maxpool = tf.keras.layers.MaxPool2D(pool_size = (pooling_size,pooling_size), strides=(pooling_size,pooling_size), padding='same')
		#self.batch3a = tf.keras.layers.BatchNormalization()

	def call(self, input_tensor, training = False):
		x = self.conv1a(input_tensor)
		x = self.conv2a(x)
		y = self.maxpool(x)
		#y = self.batch3a(y)
		return x, y


class UnetDenseLayer(tf.keras.layers.Layer):
	def __init__(self, output_size_1, output_size_2, activation_fn, dropout_rate):
		super(UnetDenseLayer, self).__init__()
		self.output_size_2 = tf.cast(output_size_2, tf.int32)

		self.flatten1a = tf.keras.layers.Flatten()
		# self.dense1a = tfp.layers.DenseFlipout(output_size_1, kernel_divergence_fn=kl_divergence_function, activation=activation_fn, dtype = tf.float32)
		# self.dense2a = tfp.layers.DenseFlipout(self.output_size_2, kernel_divergence_fn=kl_divergence_function, activation=activation_fn, dtype = tf.float32)
		self.dense1a = tf.keras.layers.Dense(output_size_1, activation=activation_fn, dtype = tf.float32)
		self.dense2a = tf.keras.layers.Dense(self.output_size_2, activation=activation_fn, dtype = tf.float32)

	def call(self, input_tensor, training = False):
		x = self.flatten1a(input_tensor)
		x = self.dense1a(x)
		x = self.dense2a(x)
		return x

	def compute_output_shape(self, input_shape):
		return [None, tf.dtypes.cast(self.output_size_2, tf.int64)]


class UnetDecodeLayer(tf.keras.layers.Layer):
	def __init__(self, output_size_1, output_size_2, dropout_rate, activation_fn, kernel_size = 3, pooling_size = 2):
		super(UnetDecodeLayer, self).__init__()
		self.output_size_2 = output_size_2
		self.upsample1a = tf.keras.layers.UpSampling2D((pooling_size,pooling_size))
		self.concat1a = tf.keras.layers.Concatenate(axis = -1)
		# self.conv1a = tfp.layers.Convolution2DFlipout(output_size_1, strides=1, kernel_size=kernel_size, activation=activation_fn, padding='same', kernel_divergence_fn=kl_divergence_function)
		# self.conv2a = tfp.layers.Convolution2DFlipout(output_size_2, strides=1, kernel_size=kernel_size, activation=activation_fn, padding='same', kernel_divergence_fn=kl_divergence_function)

		self.conv1a = tf.keras.layers.Convolution2D(output_size_1, strides=1, kernel_size=kernel_size, activation=activation_fn, padding='same')
		self.conv2a = tf.keras.layers.Convolution2D(output_size_2, strides=1, kernel_size=kernel_size, activation=activation_fn, padding='same')



	def call(self, input_tensor_1, input_tensor_2,training = False):
		x = self.upsample1a(input_tensor_1)
		x = self.concat1a([x, input_tensor_2])
		#x = x # 1
		#x = input_tensor_2 # 2
		x = self.conv1a(x)
		x = self.conv2a(x)
		return x

	def compute_output_shape(self, input_shape):
		input_shape_2 = input_shape[1].as_list()
		input_shape_2[-1] = self.output_size_2
		return input_shape_2

class UnetFinalLayer(tf.keras.layers.Layer):
	def __init__(self, output_size_1, output_size_2, activation_fn, sd):
		super(UnetFinalLayer, self).__init__()
		self.sd = sd 
		# self.conv1a = tfp.layers.Convolution2DFlipout(output_size_1, strides=1, kernel_size=3, activation=activation_fn, padding='same', kernel_divergence_fn=kl_divergence_function)
		# self.conv2a = tfp.layers.Convolution2DFlipout(output_size_2, strides=1, kernel_size=3, activation=None, padding='same', kernel_divergence_fn=kl_divergence_function)
		self.conv1a = tf.keras.layers.Convolution2D(output_size_1, strides=1, kernel_size=3, activation=activation_fn, padding='same')
		self.conv2a = tf.keras.layers.Convolution2D(output_size_2, strides=1, kernel_size=3, activation=None, padding='same')
		self.distr_layer = tfp.layers.DistributionLambda(lambda t: tfp.distributions.Normal(loc=t, scale=1))

	def call(self, input_tensor_1, training = False):
		x = self.conv1a(input_tensor_1)
		x = self.conv2a(x)
		return x

class UnetModel(tf.keras.Model):
	def __init__(self, input_shape, sd = False, dropout_rate = 0.0, scale = 1, activation_fn = tf.nn.relu, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.activation_fn = activation_fn
		self.sd = sd
		self.dropout_rate = dropout_rate
		self.scale = scale

		# Encode layers
		print("Unet Layers Shapes:")
		print(input_shape)
		self.encode_layer_1 = UnetEncodeLayer(64 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		encode_layer_1_output_shape = self.encode_layer_1.compute_output_shape(input_shape = list(input_shape))
		print(encode_layer_1_output_shape)
		self.encode_layer_2 = UnetEncodeLayer(128 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		encode_layer_2_output_shape = self.encode_layer_2.compute_output_shape(input_shape = encode_layer_1_output_shape[1])
		print(encode_layer_2_output_shape)
		self.encode_layer_3 = UnetEncodeLayer(256 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		encode_layer_3_output_shape = self.encode_layer_3.compute_output_shape(input_shape = encode_layer_2_output_shape[1])
		print(encode_layer_3_output_shape)

		# Dense layer
		self.dense_layer= UnetDenseLayer(output_size_1=512, output_size_2=np.prod(encode_layer_3_output_shape[1][1:].as_list()), dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		dense_layer_output_shape = self.dense_layer.compute_output_shape(input_shape = encode_layer_3_output_shape[1])
		print(dense_layer_output_shape)

		# Reshape layer
		self.reshape_layer = tf.keras.layers.Reshape(encode_layer_3_output_shape[1][1:])
		reshape_layer_output_shape = self.reshape_layer.compute_output_shape(input_shape = dense_layer_output_shape)
		print(reshape_layer_output_shape)


		# Decode layers
		self.decode_layer_1 = UnetDecodeLayer(128 * self.scale, 128 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		decode_layer_1_output_shape = self.decode_layer_1.compute_output_shape((reshape_layer_output_shape, encode_layer_3_output_shape[0]))
		print(decode_layer_1_output_shape)

		self.decode_layer_2 = UnetDecodeLayer(64 * self.scale, 64 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		decode_layer_2_output_shape = self.decode_layer_2.compute_output_shape((decode_layer_1_output_shape, encode_layer_2_output_shape[0]))
		print(decode_layer_2_output_shape)

		self.decode_layer_3 = UnetDecodeLayer(32 * self.scale, 32 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		decode_layer_3_output_shape = self.decode_layer_3.compute_output_shape((decode_layer_2_output_shape, encode_layer_1_output_shape[0]))

		# Final layers
		self.final_layer = UnetFinalLayer(8 * self.scale, 1, sd=sd, activation_fn=self.activation_fn)
		final_layer_shape = self.final_layer.compute_output_shape(decode_layer_3_output_shape)
		print(final_layer_shape)

	def call(self, input, training=False):
		# Encode layers
		encode_layer_1_out = self.encode_layer_1(input, training=training)
		encode_layer_2_out = self.encode_layer_2(encode_layer_1_out[1], training=training)
		encode_layer_3_out = self.encode_layer_3(encode_layer_2_out[1], training=training)

		# Dense layer
		dense_layer_out = self.dense_layer(encode_layer_3_out[1], training=training)

		# Reshape layer
		reshape_layer_out = self.reshape_layer(dense_layer_out, training=training)

		# Decode layers
		decode_layer_1_out = self.decode_layer_1(reshape_layer_out, encode_layer_3_out[0], training=training)
		decode_layer_2_out = self.decode_layer_2(decode_layer_1_out, encode_layer_2_out[0], training=training)
		decode_layer_3_out = self.decode_layer_3(decode_layer_2_out, encode_layer_1_out[0], training=training)

		# Final layers
		final_layer_out = self.final_layer(decode_layer_3_out, training=training)

		return final_layer_out


class UnetModelDeep(tf.keras.Model):
	def __init__(self, input_shape, sd = False, dropout_rate = 0.0, scale = 1, activation_fn = tf.nn.relu, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.activation_fn = activation_fn
		self.sd = sd
		self.dropout_rate = dropout_rate
		self.scale = scale
		kernel_size = 2
		pooling_size = 2

		# Encode layers
		print("Unet Layers Shapes:")
		print(input_shape)
		self.encode_layer_1 = UnetEncodeLayer(16 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		encode_layer_1_output_shape = self.encode_layer_1.compute_output_shape(input_shape = list(input_shape))
		print(encode_layer_1_output_shape, self.encode_layer_1.compute_dtype)
		self.encode_layer_2 = UnetEncodeLayer(32 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		encode_layer_2_output_shape = self.encode_layer_2.compute_output_shape(input_shape = encode_layer_1_output_shape[1])
		print(encode_layer_2_output_shape, self.encode_layer_2.compute_dtype)
		self.encode_layer_3 = UnetEncodeLayer(64 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		encode_layer_3_output_shape = self.encode_layer_3.compute_output_shape(input_shape = encode_layer_2_output_shape[1])
		print(encode_layer_3_output_shape, self.encode_layer_3.compute_dtype)
		self.encode_layer_4 = UnetEncodeLayer(128 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		encode_layer_4_output_shape = self.encode_layer_4.compute_output_shape(input_shape = encode_layer_3_output_shape[1])
		print(encode_layer_4_output_shape, self.encode_layer_4.compute_dtype)


		# Dense layer
		self.dense_layer= UnetDenseLayer(output_size_1=512, output_size_2=np.prod(encode_layer_4_output_shape[1][1:].as_list()), dropout_rate=dropout_rate, activation_fn=self.activation_fn)
		dense_layer_output_shape = self.dense_layer.compute_output_shape(input_shape = encode_layer_4_output_shape[1])
		print(dense_layer_output_shape, self.dense_layer.compute_dtype)

		# Reshape layer
		self.reshape_layer = tf.keras.layers.Reshape(encode_layer_4_output_shape[1][1:])
		reshape_layer_output_shape = self.reshape_layer.compute_output_shape(input_shape = dense_layer_output_shape)
		print(reshape_layer_output_shape, self.reshape_layer.compute_dtype, "reshape_layer_output_shape")


		# Decode layers
		self.decode_layer_0 = UnetDecodeLayer(128 * self.scale, 128 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		decode_layer_0_output_shape = self.decode_layer_0.compute_output_shape((reshape_layer_output_shape, encode_layer_4_output_shape[0]))
		print(decode_layer_0_output_shape, self.decode_layer_0.compute_dtype)

		self.decode_layer_1 = UnetDecodeLayer(64 * self.scale, 64 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		decode_layer_1_output_shape = self.decode_layer_1.compute_output_shape((decode_layer_0_output_shape, encode_layer_3_output_shape[0]))
		print(decode_layer_1_output_shape, self.decode_layer_1.compute_dtype)

		self.decode_layer_2 = UnetDecodeLayer(32 * self.scale, 32 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		decode_layer_2_output_shape = self.decode_layer_2.compute_output_shape((decode_layer_1_output_shape, encode_layer_2_output_shape[0]))
		print(decode_layer_2_output_shape, self.decode_layer_2.compute_dtype)

		self.decode_layer_3 = UnetDecodeLayer(16 * self.scale, 16 * self.scale, dropout_rate=dropout_rate, activation_fn=self.activation_fn, kernel_size=kernel_size, pooling_size=pooling_size)
		decode_layer_3_output_shape = self.decode_layer_3.compute_output_shape((decode_layer_2_output_shape, encode_layer_1_output_shape[0]))

		# Final layers
		self.final_layer = UnetFinalLayer(8 * self.scale, 1, sd=sd, activation_fn=self.activation_fn)
		final_layer_shape = self.final_layer.compute_output_shape(decode_layer_3_output_shape)
		print(final_layer_shape)

	def call(self, input, training=False):
		# Encode layers
		encode_layer_1_out = self.encode_layer_1(input, training=training)
		encode_layer_2_out = self.encode_layer_2(encode_layer_1_out[1], training=training)
		encode_layer_3_out = self.encode_layer_3(encode_layer_2_out[1], training=training)
		encode_layer_4_out = self.encode_layer_4(encode_layer_3_out[1], training=training)


		# Dense layer
		dense_layer_out = self.dense_layer(encode_layer_4_out[1], training=training)


		# Reshape layer
		reshape_layer_out = self.reshape_layer(dense_layer_out, training=training)

		# Decode layers
		
		decode_layer_0_out = self.decode_layer_0(reshape_layer_out, encode_layer_4_out[0], training=training)
		decode_layer_1_out = self.decode_layer_1(decode_layer_0_out, encode_layer_3_out[0], training=training)
		decode_layer_2_out = self.decode_layer_2(decode_layer_1_out, encode_layer_2_out[0], training=training)
		decode_layer_3_out = self.decode_layer_3(decode_layer_2_out, encode_layer_1_out[0], training=training)

		# Final layers
		final_layer_out = self.final_layer(decode_layer_3_out, training=training)

		return final_layer_out
