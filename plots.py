from data_set import Dataset
from predictor import Predictor, models
from predictor_utils import PiecewiseSchedule, OptimizerSpec
import tensorflow as tf
import datetime
from utils import get_session, plot_grid, plot_confusion_matrix
import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import rc
import matplotlib

#matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams.update({'font.size': 12})

#rc('font',**{'family': 'sans-serif','sans-serif':['Arial']})




def plot_training_loss(file, title = ''):
    data = pd.read_csv(file, sep = ', ', engine='python')
    fig, ax = plt.subplots()
    #ax.plot(data1['running_time'][0 : timesteps]/epoch_length, savgol_filter(data1['mean_sinr_improvement'][0 : timesteps], smoothing_chunk, 1), linewidth=2)
    ax.plot(data['iteration'], data['loss'], linewidth=2)
    ax.plot(data['iteration'], data['val_loss'], linewidth=2)
    try:
        ax.plot(data['iteration'], data['smoothed_val_loss'], linewidth=2)
    except KeyError:
        print("Key Error: Smoothed validation loss not available")
    ax.set(xlabel='Iterations', ylabel='Loss',
           title="min val loss: %.4f at %d" % (np.min(data['val_loss']), data['iteration'][np.argmin(data['val_loss'])]))
    ax.legend(['Training loss', 'Validation loss', 'Smoothed val. los'])
    ax.set_xlim([0, 6000])
    ax.set_ylim([-5,5])
    ax.grid()


    fig, ax = plt.subplots()
    #ax.plot(data1['running_time'][0 : timesteps]/epoch_length, savgol_filter(data1['mean_sinr_improvement'][0 : timesteps], smoothing_chunk, 1), linewidth=2)
    ax.plot(data['iteration'], data['error'], linewidth=2)
    ax.plot(data['iteration'], data['val_error'], linewidth=2)
    ax.set(xlabel='Iterations', ylabel='Error',
            title="min val error: %.2f" % np.min(data['val_error']))
    ax.legend(['Training error', 'Validation error'])
    ax.grid()
    ax.set_ylim([0, 50])
    #ax.set_xlim([0, 8000])

    fig, ax = plt.subplots()
    #ax.plot(data1['running_time'][0 : timesteps]/epoch_length, savgol_filter(data1['mean_sinr_improvement'][0 : timesteps], smoothing_chunk, 1), linewidth=2)
    ax.plot(data['iteration'], np.log(data['log_var']), linewidth=2)
    ax.plot(data['iteration'], np.log(data['val_log_var']), linewidth=2)
    ax.set(xlabel='Iterations', ylabel='Log Var',
            title="min val log var: %.2f" % np.min(data['val_log_var']))
    ax.legend(['Training error', 'Validation error'])
    ax.grid()
    #ax.set_xlim([0, 8000])

def plot_model_err_vs_var(model_name, meas_rate = 0.01, n_samples = 512):
    session_name = model_name
    config_file = "/home/enesk/repos/spatial-prediction/logs/config-%s.config" % session_name
    config = pd.read_csv(config_file, sep = '\t').to_dict(orient = 'records')
    model_name = ([str(record['value']) for record in config if record['parameter'] == 'model'][0])
    regression = ([str(record['value']) for record in config if record['parameter'] == 'regression'][0]) == 'True'
    probabilistic = ([str(record['value']) for record in config if record['parameter'] == 'probabilistic'][0]) == 'True'
    no_maps = ([str(record['value']) for record in config if record['parameter'] == 'no_maps'][0]) == 'True'
    random_walk = ([str(record['value']) for record in config if record['parameter'] == 'random_walk'][0]) == 'True'
    try:
        model_scale = ([int(record['value']) for record in config if record['parameter'] == 'model_scale'][0])
    except IndexError:
        model_scale = 1
    try:
        mc_dropout = ([str(record['value']) for record in config if record['parameter'] == 'mc_dropout'][0]) == 'True'
    except IndexError:
        mc_dropout = False
    session = get_session()

    model_checkpoint = "/home/enesk/repos/spatial-prediction/models/model-%s.cpkt" % session_name

    ds = Dataset(measured_rate = meas_rate, no_maps = no_maps)

    predictor = Predictor(
            ds = ds,
            model = models[model_name],
            session = session,
            model_checkpoint = model_checkpoint,
            session_name = session_name,
            model_scale = model_scale,
            random_walk = random_walk,
            regression = regression,
            probabilistic = probabilistic,
            mc_dropout = mc_dropout,
            test = True
            )

    obs, labels, outdoors, non_scaled_maps = ds.sample_whole_maps(n_samples, train = False,
                                    include_non_scaled  = True, include_reconstruct = False, random_walk = random_walk)


    predictions = predictor.forward_pass(obs, labels)
    variance = predictor.get_variance(obs)

    flattened_error = []
    flattened_var = []
    for i in range(n_samples):
        raw_value_pred = ds.scaled_to_raw(predictions[i]).reshape(predictions[i].shape[0:-1])
        soft_loss = np.abs(raw_value_pred - non_scaled_maps[i][0])
        current_var = variance[i].reshape(predictions[i].shape[0:-1])
        outdoor = outdoors[i].reshape(non_scaled_maps[i][0].shape)
        flattened_error = flattened_error + list(soft_loss[outdoor == 1])
        flattened_var = flattened_var + list(current_var[outdoor == 1])

    flattened_error = np.asarray(flattened_error)
    flattened_var = np.asarray(flattened_var)

    min_error = 0
    max_error = 60
    points = 20

    bins = np.linspace(min_error, max_error, points)
    print(bins)
    bin_var = np.zeros(points)
    bin_sd_error = np.zeros(points)
    for i in range(points - 1):
        start = bins[i]
        stop = bins[i + 1]
        mask = (((flattened_error >= start) * (flattened_error < stop))) == 1
        bin_var[i+1] = np.mean(np.sqrt(flattened_var[mask]))
        bin_sd_error[i+1] = np.std(np.sqrt(flattened_var[mask]))

    fig, ax = plt.subplots()
    ax.step(bins, ds.scale_sd_to_raw(bin_var))
    #ax.errorbar(bins, ds.scale_sd_to_raw(bin_var),  yerr = ds.scale_sd_to_raw(bin_sd_error), linestyle = '-', capsize=5, capthick=3, linewidth = 1)
    ax.set(xlabel='Prediction error (dBm)', ylabel='Predicted standard dev. (dBm)')
    ax.grid()





def calculate_goodness(model_name, start, stop, n_points, n_samples, save_file, benchmark = False, force_random_walk = False, kriging = False):
    session_name = model_name
    meas_rates = np.linspace(start, stop, n_points)
    comparsion_data = {}
    comparsion_data['meas_rate'] = []
    comparsion_data['pred_within_ci'] = []
    comparsion_data['sd'] = []
    comparsion_data['loglikelihood'] = []
    comparsion_data['krig_pred_within_ci'] = []
    comparsion_data['krig_sd'] = []
    comparsion_data['krig_loglikelihood'] = []

    config_file = "/home/enesk/repos/spatial-prediction/logs/config-%s.config" % session_name
    config = pd.read_csv(config_file, sep = '\t').to_dict(orient = 'records')
    model_name = ([str(record['value']) for record in config if record['parameter'] == 'model'][0])
    regression = ([str(record['value']) for record in config if record['parameter'] == 'regression'][0]) == 'True'
    probabilistic = ([str(record['value']) for record in config if record['parameter'] == 'probabilistic'][0]) == 'True'
    no_maps = ([str(record['value']) for record in config if record['parameter'] == 'no_maps'][0]) == 'True'
    random_walk = ([str(record['value']) for record in config if record['parameter'] == 'random_walk'][0]) == 'True' or force_random_walk
    try:
        model_scale = ([int(record['value']) for record in config if record['parameter'] == 'model_scale'][0])
    except IndexError:
        model_scale = 1
    try:
        mc_dropout = ([str(record['value']) for record in config if record['parameter'] == 'mc_dropout'][0]) == 'True'
    except IndexError:
        mc_dropout = False

    session = get_session()

    model_checkpoint = "/home/enesk/repos/spatial-prediction/models/model-%s.cpkt" % session_name


    ds = Dataset(measured_rate = 0.001, no_maps = no_maps)

    predictor = Predictor(
            ds = ds,
            model = models[model_name],
            session = session,
            model_checkpoint = model_checkpoint,
            session_name = session_name,
            model_scale = model_scale,
            random_walk = random_walk,
            regression = regression,
            probabilistic = probabilistic,
            mc_dropout = mc_dropout,
            test = True
            )



    for meas_rate in meas_rates:
        print('Evaluating for meas rate:', meas_rate)

        ds.set_meas_rate(meas_rate)


        if benchmark or kriging:
            print('Evaluating benchmark.')
            obs, labels, outdoors, non_scaled_maps, reconstruct_means, reconstruct_vars = ds.sample_whole_maps(n_samples, train = False,
                                            include_non_scaled  = True, include_reconstruct = benchmark, random_walk = random_walk, kriging = kriging)
        else:
            print('Evaluating model.')
            obs, labels, outdoors, non_scaled_maps = ds.sample_whole_maps(n_samples, train = False,
                                            include_non_scaled  = True, include_reconstruct = benchmark, random_walk = random_walk)
        predictions = predictor.forward_pass(obs)


        variance = predictor.get_variance(obs)

        within_ci = np.zeros((n_samples, 2))
        loglikelihood = np.zeros((n_samples, 2))
        sds = np.zeros((n_samples, 2))

        for i in range(n_samples):
            raw_value_pred = ds.scaled_to_raw(predictions[i]).reshape(predictions[i].shape[0:-1])
            sd = np.sqrt(variance[i])
            plot_grid(sd, title = 'SD')
            sd = ds.scale_sd_to_raw(sd).reshape(predictions[i].shape[0:-1])
            
            plot_grid(raw_value_pred, title = 'mean')
            within_ci[i, 0] = np.sum(np.abs((raw_value_pred - non_scaled_maps[i][0])  <= (1.96 * sd)  ) * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / np.sum(outdoors[i])
            loglikelihood[i, 0] = np.sum((-0.5 * np.log((sd **2) * 2 * np.pi) -  ((raw_value_pred - non_scaled_maps[i][0])**2) / (2 * (sd ** 2))) * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / np.sum(outdoors[i])
            sds[i, 0] = np.sum(sd * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / np.sum(outdoors[i])
            if benchmark or kriging:
                sd = np.sqrt(reconstruct_vars[i])
                sds[i, 1] = np.sum(sd) / np.sum(outdoors[i])
                within_ci[i, 1] = np.sum(np.abs((reconstruct_means[i] - non_scaled_maps[i][0])  <= (1.96 * sd)  ) * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / np.sum(outdoors[i])
                loglikelihood[i, 1] = np.sum((-0.5 * np.log((sd **2) * 2 * np.pi) -  ((reconstruct_means[i] - non_scaled_maps[i][0])**2) / (2 * (sd ** 2))) * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / np.sum(outdoors[i])

        comparsion_data['meas_rate'].append(meas_rate)
        comparsion_data['pred_within_ci'].append(np.mean(within_ci[:, 0]))
        comparsion_data['loglikelihood'].append(np.mean(loglikelihood[:, 0]))
        comparsion_data['sd'].append(np.mean(sds[:, 0]))
        comparsion_data['krig_sd'].append(np.mean(sds[:, 1]))
        comparsion_data['krig_loglikelihood'].append(np.mean(loglikelihood[:, 1]))
        comparsion_data['krig_pred_within_ci'].append(np.mean(within_ci[:, 1]))

    df = pd.DataFrame.from_dict(comparsion_data)
    df.to_csv('figures/data_%s.csv' % save_file, mode = 'w', index=False, header = True)


def plot_sd_vs_meas_rate(model_name):
    df = pd.read_csv('figures/data_%s.csv' % model_name)
    data = df.to_dict(orient = 'list')


    fig, ax = plt.subplots()
    fig.set_size_inches(4, 4.5)
    fig.tight_layout()

    #ax.plot(data['meas_rate'], data['model'], linestyle = '-', linewidth = 2)
    #ax.plot(data['meas_rate'], data['benchmark'], linestyle = '-', linewidth = 2)

    ax.plot(data['meas_rate'], data['sd'], linestyle = '-', linewidth = 2)
    ax.plot(data['meas_rate'], data['krig_sd'], linestyle = '-', linewidth = 2)

    ax.set(xlabel = 'Meas. rate', ylabel = 'Predicted sd.', title='')
    #ax.grid()
    ax.legend(['Proposed', 'Kriging'], framealpha=0.5)
    fig.savefig('figures/sd_%s.png' % model_name, pad_inches=0.1, bbox_inches = 'tight')
    fig.savefig('figures/sd_%s.pdf' % model_name, pad_inches=0.1, bbox_inches = 'tight')


def plot_pred_within_ci(model_name):
    df = pd.read_csv('figures/data_%s.csv' % model_name)
    data = df.to_dict(orient = 'list')


    fig, ax = plt.subplots()
    fig.set_size_inches(4, 4.5)
    fig.tight_layout()

    ax.plot(data['meas_rate'], data['pred_within_ci'], linestyle = '-', linewidth = 2, color = 'green')
    ax.plot(data['meas_rate'], data['krig_pred_within_ci'], linestyle = '-', linewidth = 2)

    ax.set(xlabel = 'Meas. rate', ylabel = 'Probability of true value within 95% CI', title='')
    #ax.grid()
    ax.legend(['Proposed', 'Kriging'], framealpha=0.5)
    fig.savefig('figures/pred_within_ci_%s.png' % model_name, pad_inches=0.1, bbox_inches = 'tight')
    fig.savefig('figures/pred_within_ci_%s.pdf' % model_name, pad_inches=0.1, bbox_inches = 'tight')


def plot_loglikelihood(paths, save_file, benchmark = -1):


    fig, ax = plt.subplots()
    fig.set_size_inches(4, 4.5)
    fig.tight_layout()

    for i, path in enumerate(paths):
        df = pd.read_csv('figures/data_%s.csv' % path[0])
        data = df.to_dict(orient = 'list')

        # Plot the benchmark
        if benchmark > -1 and i == benchmark:
            df = pd.read_csv('figures/data_%s.csv' % path[0])
            data = df.to_dict(orient = 'list')
            ax.plot(np.asarray(data['meas_rate'])*100, 0-np.asarray(data['krig_loglikelihood']), linestyle = '-', linewidth = 2, label = 'Kriging (requires TX location)')


        ax.plot(np.asarray(data['meas_rate'])*100, 0-np.asarray(data['loglikelihood']), linestyle = '-', linewidth = 2, label = path[1])



    #ax.set_yscale('log')



    ax.set(xlabel = 'Meas. rate (%)', ylabel = 'Negative loglikelihood', title='')
    ax.legend(framealpha=0.5)
    fig.savefig('figures/loglikelihood_%s.png' % save_file, pad_inches=0.1, bbox_inches = 'tight')
    fig.savefig('figures/loglikelihood_%s.pdf' % save_file, pad_inches=0.1, bbox_inches = 'tight')

def plot_loglikelihood_scaled(paths, save_file, benchmark = -1):


    fig, ax = plt.subplots()
    fig.set_size_inches(4, 4.5)
    fig.tight_layout()

    for i, path in enumerate(paths):
        df = pd.read_csv('figures/data_%s.csv' % path[0])
        data = df.to_dict(orient = 'list')


        ax.plot(data['meas_rate'], np.abs(1-np.asarray(data['loglikelihood2'])), linestyle = '-', linewidth = 2, label = path[1])

        # Plot the benchmark
        if benchmark > -1 and i == benchmark:
            df = pd.read_csv('figures/data_%s.csv' % path[0])
            data = df.to_dict(orient = 'list')
            ax.plot(data['meas_rate'], np.abs(1-np.asarray(data['krig_loglikelihood2'])), linestyle = '-', linewidth = 2, label = 'Kriging')

    #ax.set_yscale('log')



    ax.set(xlabel = 'Meas. rate', ylabel = 'Negative loglikelihood', title='')
    ax.legend(framealpha=0.5)
    fig.savefig('figures/loglikelihood_%s.png' % save_file, pad_inches=0.1, bbox_inches = 'tight')
    fig.savefig('figures/loglikelihood_%s.pdf' % save_file, pad_inches=0.1, bbox_inches = 'tight')

def plot_model_vs_benchmark(model_name):
    df = pd.read_csv('figures/comparsion_data_%s.csv' % model_name)
    data = df.to_dict(orient = 'list')


    fig, ax = plt.subplots()
    fig.set_size_inches(4, 4.5)
    fig.tight_layout()

    #ax.plot(data['meas_rate'], data['model'], linestyle = '-', linewidth = 2)
    #ax.plot(data['meas_rate'], data['benchmark'], linestyle = '-', linewidth = 2)

    ax.errorbar(data['meas_rate'], data['model'],  yerr = data['model_sd'], linestyle = '-', capsize=5,
                            linewidth = 2)
    ax.errorbar(data['meas_rate'], data['benchmark'],  yerr = data['benchmark_sd'], linestyle = '-', capsize=5,
                            linewidth = 2)

    ax.set(xlabel = 'Meas. rate (%)', ylabel = 'Abs. error', title='')
    ax.set_ylim(0)
    #ax.grid()
    ax.legend(['Proposed', 'Benchmark'], framealpha=0.5)
    fig.savefig('figures/comparsion_data_%s.png' % model_name, pad_inches=0.1, bbox_inches = 'tight')
    fig.savefig('figures/comparsion_data_%s.pdf' % model_name, pad_inches=0.1, bbox_inches = 'tight')


def plot_multiple_models_vs_benchmark(models, save_file, benchmark = -1, kriging = -1, skip = []):



    fig, ax = plt.subplots()
    fig.set_size_inches(4, 4.5)
    #fig.tight_layout()

    #if benchmark >= 0:
    #    legends = ['Benchmark']
    #else:
    #    legends = []

    for i, model in enumerate(models):
        model_file = model[0]
        model_name = model[1]
        df = pd.read_csv('figures/comparsion_data_%s.csv' % model_file)
        data = df.to_dict(orient = 'list')

        #ax.plot(data['meas_rate'], data['model'], linestyle = '-', linewidth = 2)
        #ax.plot(data['meas_rate'], data['benchmark'], linestyle = '-', linewidth = 2)

        if i == benchmark:
            #ax.errorbar(np.asarray(data['meas_rate']) * 100, data['benchmark'],  yerr = data['benchmark_sd'], linestyle = '-', capsize=5,
            #                    linewidth = 2, label = 'Earlier benchmark')
            ax.plot(np.asarray(data['meas_rate']) * 100, data['benchmark'],  linestyle = '-', linewidth = 2, label = 'Earlier benchmark')
        elif i == kriging:
            #ax.errorbar(np.asarray(data['meas_rate']) * 100, data['benchmark'],  yerr = data['benchmark_sd'], linestyle = '-', capsize=5,
            #                    linewidth = 2, label = 'Kriging interpolation')
            ax.plot(np.asarray(data['meas_rate']) * 100, data['benchmark'], linestyle = '-',
                                linewidth = 2, label = 'Kriging (requires TX location)')

        if not(i in skip):
            #ax.errorbar(np.asarray(data['meas_rate']) * 100, data['model'],  yerr = data['model_sd'], linestyle = '-', capsize=5,
            #                    linewidth = 2, label = model_name)
            ax.plot(np.asarray(data['meas_rate']) * 100, data['model'],  linestyle = '-',
                                linewidth = 2, label = model_name)


        ax.set(xlabel = 'Meas. rate (%)', ylabel = 'Abs. error (dBm)', title='')
        #ax.set_ylim(0)
        #ax.grid()
    ax.legend(framealpha=0.5)
    fig.savefig('figures/comparsion_data_%s.png' % save_file, pad_inches=0.1, bbox_inches = 'tight')
    fig.savefig('figures/comparsion_data_%s.pdf' % save_file, pad_inches=0.1, bbox_inches = 'tight')


if __name__ == '__main__':
    #plot_training_loss('/home/enesk/repos/spatial-prediction/logs/logging-regr_cont_meas_rate.log', 'regression1')
    #plot_training_loss('/home/enesk/repos/spatial-prediction/logs/logging-regr.log', 'regression2')
    #plot_training_loss('/home/enesk/repos/spatial-prediction/logs/logging-prob2.log', 'prob')

    #compare_model_to_benchmark('prob-rw-no-maps', 0.003, 0.03, 9, 512, 'prob-rw-no-maps', kriging = False, force_random_walk = False)

    #calculate_sd('prob2', 0.003, 0.03, 9, 512, 'prob2-variance', kriging = False, force_random_walk = False)
    calculate_goodness('prob2', 0.003, 0.03, 9, 1, 'test', kriging = True, force_random_walk = False)

    #plot_loglikelihood('prob-rw-goodness-med')
    #plot_loglikelihood('prob2-goodness-med')

    #plot_loglikelihood_scaled([('prob2-goodness', 'Proposed'),],
    #            save_file = 'goodness',benchmark = 0)

    plot_loglikelihood([('prob2-goodness-med', 'Proposed'),('prob-no-maps-goodness', '3D-map-blind')],
                save_file = 'goodness',benchmark = 0)
    plot_loglikelihood([('prob-rw-goodness-med', 'Proposed'),],
                save_file = 'rw-goodness',benchmark = 0)



    plot_multiple_models_vs_benchmark([
                ('prob2', 'Proposed'),
                ('prob-no-maps', '3D-map-blind'),
                ('regr', 'LS'),
                ],  'uniform', benchmark = -1, kriging = 0, skip = [])

    plot_multiple_models_vs_benchmark([
                ('prob2', 'Proposed'),
                ('prob-no-maps', 'Proposed without 3D maps'),
                ],  'uniform', benchmark = -1, kriging = 0, skip = [])



    plot_multiple_models_vs_benchmark([
                    ('prob-rw', 'Proposed'),
                    ('regr_cont_meas_rate_random_walk', 'LS'),
                    ],  'random_walk', benchmark = -1, kriging = 0, skip = [])


    plot_multiple_models_vs_benchmark([
                    ('prob-rw', 'Proposed'),
                    ],  'random_walk', benchmark = -1, kriging = 0, skip = [])

    #plot_model_err_vs_var('prob2')


    plt.show()
