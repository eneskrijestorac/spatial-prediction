import tensorflow as tf
import tensorflow_probability as tfp
from predictor_utils import minimize_and_clip, initialize_interdependent_variables, size_from_tf_shape, list_from_tf_shape
import numpy as np
import time
from models import UnetModel, UnetModelDeep, UnetModelKernel
from models_probailistic import UnetModel, UnetModelDeep as UnetModelMC
from icassp_models import UnetModel as UnetModelIcassp
import os

from tensorflow.python.compiler.tensorrt import trt_convert as trt




models = {}
models['unet_icc'] = UnetModel
models['unet_mc'] = UnetModelMC
models['unet_deep'] = UnetModelDeep
models['unet_icassp'] = UnetModelIcassp

class Predictor(object):
	def __init__(
        self,
        ds,
        model_name,
        session = None,
        checkpoint_interval = 1000,
        session_name = 'test',
        optimizer_spec = None,
        stopping_criterion=None,
        batch_size=64,
		test_batch_size = 64,
        grad_norm_clipping=10,
		learning_rate = 1E-3,
		test_loss_smoothing = 1, 
        dropout = 0.0,
		full_covar_gauss = False,
        batch_norm = False,
        test = False,
        regression = False,
        simp_prob = False,
        mc_dropout = False,
		use_tensor_rt = True,
		mc_dnn = False,
        probabilistic = False,
		probabilistic_with_distance_covar = False,
        random_walk = False,
        model_scale = 1,
        detrend = False,
        ):

		self.ds = ds
		self.model_name = model_name
		self.full_covar_gauss = full_covar_gauss
		self.dnn_model = models[model_name]
		self.session = session
		self.use_tensor_rt = use_tensor_rt
		self.checkpoint_interval = checkpoint_interval
		self.session_name = session_name
		self.optimizer_spec = optimizer_spec
		self.stopping_criterion = stopping_criterion
		self.batch_size = batch_size
		self.test_batch_size = test_batch_size
		self.model_checkpoint = "models/%s/" % session_name
		self.grad_norm_clipping = grad_norm_clipping
		self.dropout = dropout
		self.test_loss_smoothing = test_loss_smoothing
		self.batch_norm = batch_norm
		self.best_loss = np.Inf
		self.best_error = np.Inf
		self.best_val_loss = np.Inf
		self.best_val_error = np.Inf
		self.t = 0
		self.model_initialized = False
		self.log_every_n_steps = 10
		self.test = test
		self.regression = regression
		self.random_walk = random_walk
		self.probabilistic = probabilistic
		self.probabilistic_with_distance_covar = probabilistic_with_distance_covar
		self.mc_dropout = mc_dropout
		self.simp_prob = simp_prob
		self.detrend = detrend
		self.learning_rate=learning_rate
		self.mc_dnn = mc_dnn

		# Start time
		self.start_time = time.time()

		# Create the input placeholder
		input_shape = (None,) + ds.obs_shape
	

		
		# Create the label placeholder
		assert ((int(regression)+int(mc_dnn)+int(probabilistic)+int(full_covar_gauss)+int(probabilistic_with_distance_covar)) == 1), "Invalid mode selection"
		assert mc_dnn == (model_name == 'unet_mc'), "Invalid DNN model selected"

		if regression:
			self.mean = self.dnn_model(input_shape=input_shape, dropout_rate=self.dropout)

		elif mc_dnn:
			self.mean = self.dnn_model(input_shape=input_shape, dropout_rate=self.dropout, activation_fn=tf.nn.relu)

		elif probabilistic:
			self.mirrored_strategy = tf.distribute.MirroredStrategy()
			with self.mirrored_strategy.scope():
				self.mean = self.dnn_model(input_shape=input_shape, dropout_rate=self.dropout, activation_fn="relu")
				self.log_var = self.dnn_model(input_shape=input_shape, dropout_rate=self.dropout, activation_fn="tanh", sd = True)
				self.optimizer = tf.keras.optimizers.Adam(epsilon=1E-6, learning_rate=self.learning_rate)


		elif probabilistic_with_distance_covar:
			self.mirrored_strategy = tf.distribute.MirroredStrategy()
			
			with self.mirrored_strategy.scope():
				# Distance matrix 
				y_coords, x_coords = np.meshgrid(np.arange(ds.obs_shape[0]), np.arange(ds.obs_shape[1]))
				locations = np.stack([y_coords, x_coords], axis = -1)
				locations = locations.reshape((ds.obs_shape[0]*ds.obs_shape[1], -1))
				locations_a = np.expand_dims(locations, axis = 1)
				locations_b = np.expand_dims(locations, axis = 0)
				distance = np.sqrt(np.sum((locations_a - locations_b)**2,2))
				self.distance_matrix = tf.constant(distance, dtype=tf.float32)	

				# Variables sometimes use for distance-based covariance 
				self.var_container = tf.Module()
				self.var_container.a = tf.Variable(0.01, dtype=tf.float32, name = "a")
				self.var_container.b = tf.Variable(1, dtype=tf.float32, name = "b")


				self.mean = self.dnn_model(input_shape=input_shape, dropout_rate=self.dropout, activation_fn="relu")
				self.log_var = self.dnn_model(input_shape=input_shape, dropout_rate=self.dropout, activation_fn="tanh", sd = False)
				self.optimizer = tf.keras.optimizers.Adam(epsilon=1E-7, learning_rate=self.learning_rate)

				m_path = "/home/enesk/repos/spatial-prediction/models/rand_walk_interpol"
				self.mean = tf.keras.models.load_model(m_path+"/mu")
				self.log_var = tf.keras.models.load_model(m_path+"/sigma")

		elif full_covar_gauss:
			mirrored_strategy = tf.distribute.MirroredStrategy()


			self.mean = self.dnn_model(input_shape=input_shape, dropout_rate=self.dropout, activation_fn=tf.nn.relu)
			#self.covar = UnetModelKernel(input_shape=input_shape, dropout_rate=self.dropout, activation_fn=tf.nn.tanh)
			self.covar = self.dnn_model(input_shape=input_shape, dropout_rate=self.dropout, activation_fn=tf.nn.relu, output_layers = 5, covar = True)

		elif simp_prob:
			raise ValueError("Not updated to TF2")

		elif mc_dropout:
			raise ValueError("Not updated to TF2")

		else:
			raise ValueError("Not updated to TF2")





		# if mc_dnn:
		# 	#self.mean.build(input_shape=input_shape)
		# 	self.mean.compile(self.optimizer, loss='SumSquaredError',
        #         metrics=['SumSquaredError'], experimental_run_tf_function=False)
		# 	#
		# 	print("Compiled model since using MC sampling")


	@tf.function
	def train_step_mc(self, obs, l, outdoor):
		with tf.GradientTape() as tape:
			# training=True is only needed if there are layers with different
			# behavior during training versus inference (e.g. Dropout).
			mean = self.mean(obs, training=True)
			mean = tf.reshape(mean, l.shape)
			outdoor = tf.reshape(outdoor, l.shape)

			# Negative log-likelihood with variance 1
			loss = tf.math.reduce_sum((0.5 * tf.math.squared_difference(l, mean)) * outdoor)

			# Add KL divergence
			loss = loss + tf.math.reduce_sum(self.mean.losses)


			gradients = tape.gradient(loss, self.mean.trainable_variables)
			self.optimizer.apply_gradients(zip(gradients, self.mean.trainable_variables))


	@tf.function
	def train_step_full_covar(self, obs, l, outdoor):
		with tf.GradientTape() as tape:
			# training=True is only needed if there are layers with different
			# behavior during training versus inference (e.g. Dropout).
			mean = self.mean(obs, training=True)
			covar = self.covar(obs, training=True)

			mean = tf.reshape(mean, shape = [mean.shape[0], -1, 1])
			l = tf.reshape(l, shape = [mean.shape[0], -1, 1])

			logdet = tf.linalg.logdet(covar)
			error = mean - l
			error = tf.cast(error, tf.float64)
			projected_error =   tf.matmul(tf.linalg.inv(covar), error)
			loss = tf.matmul(tf.transpose(error, [0,2,1]), projected_error)
			loss = tf.reshape(loss, [-1]) + logdet
			loss = tf.reduce_mean(loss)
			# loss = tf.math.reduce_mean((0.5 * tf.exp(-log_var) * tf.math.squared_difference(l, mean) + 0.5 * log_var) * outdoor)
			gradients = tape.gradient(loss, self.mean.trainable_variables+self.covar.trainable_variables)
			self.optimizer.apply_gradients(zip(gradients, self.mean.trainable_variables+self.covar.trainable_variables))


	@tf.function
	def train_step_prob_distributed(self, obs, l, outdoor):
		per_replica_metrics = self.mirrored_strategy.run(self.train_step_prob, args=(obs, l, outdoor))
		reduced_metrics = self.mirrored_strategy.reduce(tf.distribute.ReduceOp.MEAN, per_replica_metrics,
                         axis=None)
		return reduced_metrics
	
	@tf.function
	def train_step_prob_distributed_with_distance_covar(self, obs, l, outdoor):
		per_replica_metrics = self.mirrored_strategy.run(self.train_step_prob_with_distance_covar, args=(obs, l, outdoor))
		reduced_metrics = self.mirrored_strategy.reduce(tf.distribute.ReduceOp.MEAN, per_replica_metrics,
                         axis=None)
		return reduced_metrics
	
	@tf.function
	def train_step_prob(self, obs, l, outdoor):
		with tf.GradientTape() as tape:
			# training=True is only needed if there are layers with different
			# behavior during training versus inference (e.g. Dropout).
			mean = self.mean(obs, training=True)
			log_var = self.log_var(obs, training=True)




			# With a diagonal matrix the training will be slower and consume more me
			# mean = tf.reshape(mean, shape = [mean.shape[0], -1, 1])
			# l = tf.reshape(l, shape = [mean.shape[0], -1, 1])
			# covar = tf.exp(log_var)
			# covar = tf.reshape(covar, shape = [mean.shape[0], -1])
			# covar = tf.linalg.diag(covar)
			# logdet = tf.linalg.logdet(covar)
			# error = mean - l
			# projected_error =   tf.matmul(tf.linalg.inv(covar), error)
			# loss = tf.matmul(tf.transpose(error, [0,2,1]), projected_error)
			# loss = tf.reshape(loss, [-1]) + logdet
			# loss = tf.reduce_mean(loss)

			loss = tf.math.reduce_mean((0.5 * (tf.exp(-log_var)/1) * tf.math.squared_difference(l, mean) + 0.5 * log_var) * outdoor)
			
			gradients = tape.gradient(loss, self.mean.trainable_variables+self.log_var.trainable_variables)
			self.optimizer.apply_gradients(zip(gradients, self.mean.trainable_variables+self.log_var.trainable_variables))
		return loss
	
	@tf.function
	def train_step_prob_with_distance_covar(self, obs, l, outdoor):
		with tf.GradientTape() as tape:
			# training=True is only needed if there are layers with different
			# behavior during training versus inference (e.g. Dropout).
			mean = self.mean(obs, training=True)
			log_var = self.log_var(obs, training=True)




			# With a diagonal matrix the training will be slower and consume more me
			mean = tf.reshape(mean, shape = [mean.shape[0], -1, 1])
			l = tf.reshape(l, shape = [mean.shape[0], -1, 1])
			covar = tf.exp(log_var)
			covar = tf.reshape(covar, shape = [mean.shape[0], -1])
			covar = tf.linalg.diag(covar)

			# Add distance covar
			distance_covar = self.var_container.a * tf.exp(-self.distance_matrix / self.var_container.b)
			covar = covar + distance_covar

			logdet = tf.linalg.logdet(covar)
			error = mean - l
			projected_error =   tf.matmul(tf.linalg.inv(covar), error)
			loss = tf.matmul(tf.transpose(error, [0,2,1]), projected_error)
			loss = tf.reshape(loss, [-1]) + logdet
			loss = tf.reduce_mean(loss)


			gradients = tape.gradient(loss, self.log_var.trainable_variables+list(self.var_container.trainable_variables))
			self.optimizer.apply_gradients(zip(gradients, self.log_var.trainable_variables+list(self.var_container.trainable_variables)))
		return loss	


	@tf.function
	def train_step_regr(self, obs, l, outdoor):
		with tf.GradientTape() as tape:
			# training=True is only needed if there are layers with different
			# behavior during training versus inference (e.g. Dropout).
			mean = self.mean(obs, training=True)
			loss = tf.math.reduce_mean(tf.math.squared_difference(l, mean) * outdoor)
			gradients = tape.gradient(loss, self.mean.trainable_variables)
			self.optimizer.apply_gradients(zip(gradients, self.mean.trainable_variables))


	@tf.function
	def get_mean_loss_full_covar(self, obs, l, outdoor):
		# training=True is only needed if there are layers with different
		# behavior during training versus inference (e.g. Dropout).
		mean = self.mean(obs, training=False)
		covar = self.covar(obs, training=False)

		mean = tf.reshape(mean, shape = [mean.shape[0], -1, 1])
		l = tf.reshape(l, shape = [mean.shape[0], -1, 1])

		logdet = tf.linalg.logdet(covar)
		error = mean - l
		error = tf.cast(error, tf.float64)
		projected_error =   tf.matmul(tf.linalg.inv(covar), error)
		loss = tf.matmul(tf.transpose(error, [0,2,1]), projected_error)
		loss = tf.reshape(loss, [-1]) + logdet
		loss = tf.reduce_mean(loss)
		
		mean_loss = loss
		mean_error = tf.reduce_mean(tf.square(mean-l))
		mean_var = tf.reduce_mean(covar)
		
		return mean_loss, mean_error, mean_var

	@tf.function
	def get_mean_loss_prob(self, obs, l, outdoor):
		mean =self.mean(obs, training=False)
		log_var = self.log_var(obs, training=False)
		mean_loss = tf.math.reduce_mean((0.5 * tf.exp(-log_var) * tf.math.squared_difference(l, mean) + 0.5 * log_var) * outdoor)
		raw_value_pred = self.ds.scaled_to_raw(mean)
		raw_label = self.ds.scaled_to_raw(l)
		mean_error = tf.math.reduce_sum( tf.abs(raw_value_pred - raw_label) * outdoor) / tf.math.reduce_sum(outdoor)
		mean_log_var = tf.math.reduce_sum( tf.exp(-log_var) * tf.math.squared_difference(l, mean) * outdoor) / tf.math.reduce_sum(outdoor)
		return mean_loss, mean_error, mean_log_var

	@tf.function
	def get_mean_loss_prob_with_distance_covar(self, obs, l, outdoor):
		mean =self.mean(obs, training=False)
		log_var = self.log_var(obs, training=False)
		raw_value_pred = self.ds.scaled_to_raw(mean)
		raw_label = self.ds.scaled_to_raw(l)
		mean_error = tf.math.reduce_sum( tf.abs(raw_value_pred - raw_label) * outdoor) / tf.math.reduce_sum(outdoor)
		mean_log_var = tf.math.reduce_sum( tf.exp(-log_var) * tf.math.squared_difference(l, mean) * outdoor) / tf.math.reduce_sum(outdoor)


		# With a diagonal matrix the training will be slower and consume more me
		mean = tf.reshape(mean, shape = [mean.shape[0], -1, 1])
		l = tf.reshape(l, shape = [mean.shape[0], -1, 1])
		covar = tf.exp(log_var)
		covar = tf.reshape(covar, shape = [mean.shape[0], -1])
		covar = tf.linalg.diag(covar)

		# Add distance covar
		distance_covar =  self.var_container.a * tf.exp(-self.distance_matrix /  self.var_container.b)
		covar = covar + distance_covar

		logdet = tf.linalg.logdet(covar)
		error = mean - l
		projected_error =   tf.matmul(tf.linalg.inv(covar), error)
		loss = tf.matmul(tf.transpose(error, [0,2,1]), projected_error)
		loss = tf.reshape(loss, [-1]) + logdet
		loss = tf.reduce_mean(loss)


		
		return loss, mean_error, mean_log_var

	@tf.function
	def get_mean_loss_prob_distributed(self, obs, l, outdoor):
		per_replica_metrics = self.mirrored_strategy.run(self.get_mean_loss_prob, args=(obs, l, outdoor))
		reduced_metrics = self.mirrored_strategy.reduce(tf.distribute.ReduceOp.MEAN, per_replica_metrics,
                         axis=None)
		return reduced_metrics 


	@tf.function
	def get_mean_loss_prob_distributed_with_distance_covar(self, obs, l, outdoor):
		per_replica_metrics = self.mirrored_strategy.run(self.get_mean_loss_prob_with_distance_covar, args=(obs, l, outdoor))
		reduced_metrics = self.mirrored_strategy.reduce(tf.distribute.ReduceOp.MEAN, per_replica_metrics,
                         axis=None)
		return reduced_metrics 

	@tf.function
	def get_mean_loss_mc(self, obs, l, outdoor):
		num_monte_carlo = 50
		preds = tf.stack([self.mean(obs)
			for _ in range(num_monte_carlo)], axis=0)
		mean = tf.reduce_mean(preds, axis=0)
		var = tf.math.reduce_variance(preds)
		log_var = tf.math.log(var)


		mean_loss = tf.math.reduce_mean((0.5 * tf.exp(-log_var) * tf.math.squared_difference(l, mean) + 0.5 * log_var) * outdoor)
		raw_value_pred = self.ds.scaled_to_raw(mean)
		raw_label = self.ds.scaled_to_raw(l)
		mean_error = tf.math.reduce_sum( tf.abs(raw_value_pred - raw_label) * outdoor) / tf.math.reduce_sum(outdoor)
		mean_log_var = tf.math.reduce_sum( tf.exp(-log_var) * tf.math.squared_difference(l, mean) * outdoor) / tf.math.reduce_sum(outdoor)
		return mean_loss, mean_error, mean_log_var


	@tf.function
	def get_mean_loss_regr(self, obs, l, outdoor):
		mean =self.mean(obs, training=False)
		mean_loss = tf.math.reduce_mean(tf.math.squared_difference(l, mean) * outdoor)
		raw_value_pred = self.ds.scaled_to_raw(mean)
		raw_label = self.ds.scaled_to_raw(l)
		mean_error = tf.math.reduce_sum( tf.abs(raw_value_pred - raw_label) * outdoor) / tf.math.reduce_sum(outdoor)
		mean_log_var = 0
		return mean_loss, mean_error, mean_log_var

	def stopping_criterion_met(self):
		return self.stopping_criterion is not None and self.stopping_criterion(self.t)

	def create_logging_file(self):
		self.logging_file = "logs/logging-" + self.session_name +'.log'
		f = open(self.logging_file, 'w')
		f.write('iteration, loss, error, val_loss, val_error, running_time, log_var, val_log_var, learning_rate, smoothed_val_loss\n')
		f.close()
		print('Created logging file:', self.logging_file)

	def restore(self, model_path):
		if (os.path.isdir(model_path+"/mu")):
			self.mean = tf.keras.models.load_model(model_path+"/mu")
			if self.use_tensor_rt:
				converter = trt.TrtGraphConverterV2(
				input_saved_model_dir=model_path+"/mu",
				precision_mode=trt.TrtPrecisionMode.FP32
				)		
				self.mean_trt = converter.convert()	
		else:
			raise ValueError("Invalid model checkpoint path.")
		if (os.path.isdir(model_path+"/sigma")):
			self.log_var = tf.keras.models.load_model(model_path+"/sigma")
			if self.use_tensor_rt:
				converter = trt.TrtGraphConverterV2(
				input_saved_model_dir=model_path+"/sigma",
				precision_mode=trt.TrtPrecisionMode.FP32
				)		
				self.log_var_trt = converter.convert()	
		if (os.path.isdir(self.model_checkpoint+"/extra_vars")):
			self.var_container = tf.saved_model.load(self.model_checkpoint+"/extra_vars")

	def update_model(self):
		obs_batch, labels, outdoor = self.ds.sample_whole_maps(self.batch_size, boolean = not(self.regression or
				self.probabilistic or self.mc_dropout or self.simp_prob), random_walk = self.random_walk, detrend = self.detrend)

		if self.mc_dnn:
			self.train_step_mc(obs = np.asarray(obs_batch), l =  np.asarray(labels, dtype=np.float32), 
				outdoor =  np.asarray(outdoor, dtype=np.float32))
		elif self.full_covar_gauss:
			self.train_step_full_covar(obs = np.asarray(obs_batch), l =  np.asarray(labels, dtype=np.float32), 
				outdoor =  np.asarray(outdoor, dtype=np.float32))
		elif self.probabilistic:
			self.train_step_prob_distributed(np.asarray(obs_batch, dtype=np.float32), np.asarray(labels, dtype=np.float32), np.asarray(outdoor, dtype=np.float32))
			# self.train_step_prob(obs = np.asarray(obs_batch), l =  np.asarray(labels, dtype=np.float32), 
			# 	outdoor =  np.asarray(outdoor, dtype=np.float32))
		elif self.probabilistic_with_distance_covar:
			self.train_step_prob_distributed_with_distance_covar(np.asarray(obs_batch), np.asarray(labels, dtype=np.float32), np.asarray(outdoor, dtype=np.float32))
		elif self.regression:
			self.train_step_regr(obs = np.asarray(obs_batch), l =  np.asarray(labels, dtype=np.float32), 
				outdoor =  np.asarray(outdoor, dtype=np.float32))
		self.t += 1

	@tf.function
	def forward_pass(self, obs):
		if self.probabilistic or self.simp_prob or self.regression or self.probabilistic_with_distance_covar:
			if self.use_tensor_rt:
				preds = self.mean_trt(obs)['output_1']
			else:
				preds = self.mean(obs, training=False)


		elif self.mc_dropout:
			raise ValueError("Not implemented in TF2")
			T = 100
			means = []
			for i in range(T):
				means.append(self.session.run(self.mean, {self.obs_t_ph: obs}))
			#samples = self.session.run(self.sample_predictions, {self.obs_t_ph: self.loss_estimate_obs_batch})

			preds = []

			for i, _ in enumerate(obs):
				pred = 0
				for j in range(T):
					pred = pred + means[j][i]
				pred = pred / T
				preds.append(pred)

		else:
			raise ValueError("Invalid option.")

		return preds

	@tf.function
	def get_covariance(self, obs):
		if self.probabilistic_with_distance_covar:
			log_var = (self.log_var(obs, training=False))
			covar = tf.exp(log_var)
			covar = tf.reshape(covar, shape = [obs.shape[0], -1])
			covar = tf.linalg.diag(covar)

			# Add distance covar
			distance_covar =  self.var_container.a * tf.exp(-self.distance_matrix /  self.var_container.b)
			covar = covar + distance_covar
		elif self.probabilistic:
			log_var = (self.log_var(obs, training=False))
			covar = tf.exp(log_var)
			covar = tf.reshape(covar, shape = [obs.shape[0], -1])
			covar = tf.linalg.diag(covar)			
		else:
			raise ValueError("Can't return covariance in this mode.")
		return covar

	@tf.function
	def get_variance(self, obs):
		if self.probabilistic  or self.probabilistic_with_distance_covar:
			#
			if self.use_tensor_rt:
				var = tf.exp(self.log_var_trt(obs)["output_1"])
			else:
				var = tf.exp(self.log_var(obs, training=False))
		elif self.mc_dropout:
			T = 100
			preds = []
			for i in range(T):
				preds.append(self.session.run(self.mean, {self.obs_t_ph: obs}))
			#samples = self.session.run(self.sample_predictions, {self.obs_t_ph: self.loss_estimate_obs_batch})

			vs = []

			for i, _ in enumerate(obs):
				pred = 0
				v = 0
				for j in range(T):
					pred = pred + preds[j][i]
					v = v + preds[j][i] * preds[j][i]
				pred = pred / T
				v = v / T - pred * pred
				vs.append(v)
		else:
			raise ValueError("Can't get variance for this type of model.")
		return var



	def get_mean_loss(self, train = True, n_samples = 256):
		obs, labels, outdoor, non_scaled_maps  = self.ds.sample_whole_maps(n_samples,
					train = train, include_non_scaled  = True, random_walk = self.random_walk, 
						boolean = False, detrend = self.detrend)
		if len(obs):

			if self.probabilistic or self.simp_prob :
				mean_loss, mean_error, mean_log_var = self.get_mean_loss_prob_distributed(obs = np.asarray(obs), l =  np.asarray(labels, dtype=np.float32), 
				outdoor =  np.asarray(outdoor, dtype=np.float32))
				# mean_loss, mean_error, mean_log_var = self.get_mean_loss_prob(obs = np.asarray(obs), l =  np.asarray(labels, dtype=np.float32), 
				# outdoor =  np.asarray(outdoor, dtype=np.float32))
			elif self.probabilistic_with_distance_covar:
				mean_loss, mean_error, mean_log_var = self.get_mean_loss_prob_distributed_with_distance_covar(obs = np.asarray(obs), l =  np.asarray(labels, dtype=np.float32), 
				outdoor =  np.asarray(outdoor, dtype=np.float32))
				# mean_loss, mean_error, mean_log_var = self.get_mean_loss_prob(obs = np.asarray(obs), l =  np.asarray(labels, dtype=np.float32), 
				# outdoor =  np.asarray(outdoor, dtype=np.float32))
			elif self.full_covar_gauss:
				mean_loss, mean_error, mean_log_var = self.get_mean_loss_full_covar(obs = np.asarray(obs), l =  np.asarray(labels, dtype=np.float32), 
				outdoor =  np.asarray(outdoor, dtype=np.float32))
			elif self.mc_dnn:
				mean_loss, mean_error, mean_log_var = self.get_mean_loss_mc(obs = np.asarray(obs), l =  np.asarray(labels, dtype=np.float32), 
				outdoor =  np.asarray(outdoor, dtype=np.float32))
			elif self.regression:
				mean_loss, mean_error, mean_log_var = self.get_mean_loss_regr(obs = np.asarray(obs), l =  np.asarray(labels, dtype=np.float32), 
				outdoor =  np.asarray(outdoor, dtype=np.float32))
			else:
				raise ValueError("Not implemented in TF2")
		else:
			raise ValueError
		#print(pred)
		return mean_loss, mean_error, mean_log_var



	def log_progress(self):
		if ((self.t-1) % self.log_every_n_steps == 0):
			if (self.t-1) == 0:
				# Create the logging file
				self.create_logging_file()

			# Find the estimate of val loss
			self.mean_loss, self.error, log_var = self.get_mean_loss(n_samples = self.test_batch_size)
			self.best_loss = min(self.best_loss, self.mean_loss)
			self.best_error = min(self.best_error, self.error)
			self.val_loss, self.val_error, val_log_var = self.get_mean_loss(train = False, n_samples = self.test_batch_size)
			if self.best_val_loss == np.inf:
				self.curr_val_loss = self.val_loss
			else:
				self.curr_val_loss = self.val_loss * (self.test_loss_smoothing) + (1-self.test_loss_smoothing) * self.curr_val_loss
			self.best_val_loss = min(self.best_val_loss, self.curr_val_loss)
			self.best_val_error = min(self.val_error, self.best_val_error)

			# If the reward has increased store the model
			if self.best_val_loss == self.curr_val_loss and self.t > 10:
				if not(os.path.isdir(self.model_checkpoint)):
					os.mkdir(self.model_checkpoint)
				if not(self.mc_dnn):
					save_path = self.mean.save(self.model_checkpoint+"/mu")
					if self.probabilistic:
						self.log_var.save(self.model_checkpoint+"/sigma")
					elif self.probabilistic_with_distance_covar:
						self.log_var.save(self.model_checkpoint+"/sigma")
						tf.saved_model.save(self.var_container, self.model_checkpoint+"/extra_vars")

				print("Model saved in path: %s" % self.model_checkpoint)

			print("Iteration %d" % (self.t,))
			print("training loss %f" % self.mean_loss)
			print("best training loss %f" % self.best_loss)
			print("best training error %f" % self.best_error)
			print("validation loss %f" % self.val_loss)
			print("smoothed validation loss %f" % self.curr_val_loss)
			print("best validation loss %f" % self.best_val_loss)
			print("best validation error %f" % self.best_val_error)
			if self.start_time is not None:
				running_time = (time.time() - self.start_time) / 60.
				print("running time %f" % (running_time))

			# Write data to a log file
			f = open(self.logging_file, 'a')
			f.write('%d, %f, %f, %f, %f, %f, %f, %f, %f, %f\n' % (self.t, self.mean_loss, self.error, 
							self.val_loss, self.val_error, running_time, log_var, val_log_var, self.learning_rate, self.curr_val_loss))
			f.close()
