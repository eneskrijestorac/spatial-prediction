import pandas as pd
import numpy as np
from geometry import get_building_grid
import matplotlib.pyplot as plt
import seaborn as sns
from os import listdir
from os.path import isfile, join
from utils import store_configuration
import random

# The column numbers for the data coming from Wireless Insite
X_COORD_COL = 1
Y_COORD_COL = 2
Z_COORD_COL = 3
SINR_COL = 9

CITY_FILE = 'ray_tracing_sim/ottawa.city'
COMM_DATA_DIR = 'ray_tracing_sim/outputs/'

def read_csv_data(filename, lineskip = 2):
	'''
	Inputs:
		filename 
		lineskip	defines the number of initial lines in the file to be skipped
	Outputs:
		data 	a Lx4 numpy array where L is the number of data points (sorted by x, y coordinates)
	'''

	print("Reading file: %s" % filename)
	file = open(filename)
	
	# Skip the header lines 
	for i in range(lineskip):
		file.readline()
	
	# Extract only the required columns
	df = pd.read_csv(file, sep = '  ', header = None, engine = 'python')
	
	df = df.sort_values(by = [1, 2])
	data = np.zeros((df.shape[0], 4))
	data[:,:] = df.loc[:, (X_COORD_COL, Y_COORD_COL, Z_COORD_COL, SINR_COL)] 
	return data

def convert_data_into_grid(data, spacing = 5):
	'''
	The data coming in has float coordinates, however we know it is in fact a rectangular grid
	'''
	#print(data)
	xvals = np.unique(data[:, 0])
	yvals = np.unique(data[:, 1])
	sinr_grid = np.zeros((yvals.size, xvals.size),)
	
	for j in range(xvals.size):
		for i in range(yvals.size):
			sinr = data[j*yvals.size + i, 3]
			sinr_grid[yvals.size - i - 1 , j] = sinr


	return sinr_grid

def main():
	MAX_SINR = -np.inf
	MIN_SINR = np.inf
	MAX_HEIGHT = -np.inf
	MIN_HEIGHT = np.inf
	summary_file = 'stored_grids/summary.txt'


	all_comm_files = [f for f in listdir(COMM_DATA_DIR) if isfile(join(COMM_DATA_DIR, f))] 
	random.shuffle(all_comm_files)
	building_grid = []

	i = 0

	for comm_file in all_comm_files:
		comm_file_path = join(COMM_DATA_DIR, comm_file)
		print('Processing file', comm_file_path)
		data = read_csv_data(comm_file_path)
		sinr_grid = convert_data_into_grid(data)
		if not(len(building_grid)):
			building_grid = get_building_grid(data, CITY_FILE)


		MAX_SINR = max(MAX_SINR, np.amax(sinr_grid))
		MIN_SINR = min(MIN_SINR, np.amin(sinr_grid))
		MAX_HEIGHT = max(MAX_HEIGHT, np.amax(building_grid))
		MIN_HEIGHT = min(MIN_HEIGHT, np.amin(building_grid))




		# Now we need to find the location of the user
		indmax = np.argmax(sinr_grid)
		coord = np.unravel_index(indmax, sinr_grid.shape)
		division = 101
		overlap = 2
		if coord[1] < division:
			test = True
			sinr_grid_half = sinr_grid[:, 0:division + overlap]
			building_grid_half = building_grid[:, 0:division + overlap]
		else:
			test = False
			sinr_grid_half = sinr_grid[:, division - overlap:]
			building_grid_half = building_grid[:, division - overlap:]


		fig, (ax1, ax2) = plt.subplots(2, 1)
		ax1.imshow(building_grid_half, cmap='plasma')
		ax1.set(xlabel='x', ylabel='y',
          title=comm_file+' Test: '+str(test))
		ax2.imshow(sinr_grid_half, cmap='plasma')
		ax2.set(xlabel='x', ylabel='y',
          title=comm_file+' Test: '+str(test))

		if test:
			np.savetxt('stored_grids/sinr_grids/test/sinr_grid-%d.txt' % (i), sinr_grid_half, fmt='%f')
			np.savetxt('stored_grids/city_grids/test/city_grid-%d.txt' % (i), building_grid_half, fmt='%f')
		else:
			np.savetxt('stored_grids/sinr_grids/train/sinr_grid-%d.txt' % (i), sinr_grid_half, fmt='%f')
			np.savetxt('stored_grids/city_grids/train/city_grid-%d.txt' % (i), building_grid_half, fmt='%f')
		i += 1	

	print('Storing summary into %s'%summary_file)
	store_configuration(summary_file,
		MAX_SINR = MAX_SINR,
		MIN_SINR = MIN_SINR,
		MAX_HEIGHT = MAX_HEIGHT,
		MIN_HEIGHT = MIN_HEIGHT)
	
	print('Completed.')

	plt.show()
if __name__ == "__main__":
	main()


#ax = sns.heatmap(sinr_grid, linewidth=0.5)
#plt.show()