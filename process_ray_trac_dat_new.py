import pandas as pd
import numpy as np
from geometry import get_building_grid
import matplotlib.pyplot as plt
# import seaborn as sns
from os import listdir
from os.path import isfile, join, isdir
from utils import store_configuration
import random

# The column numbers for the data coming from Wireless Insite
X_COORD_COL = 1
Y_COORD_COL = 2
Z_COORD_COL = 3
POWER_COL = 5
DATA_STARTS = 4 # The row in which data starts in the wireless insite outputs


SCENARIOS_DIR = 'wi_setups/'
POWER_OF_AN_INACCESSIBLE_FIELD = -250

def read_csv_data(filename, lineskip = 2):
	'''
	Inputs:
		filename 
		lineskip	defines the number of initial lines in the file to be skipped
	Outputs:
		data 	a Lx4 numpy array where L is the number of data points (sorted by x, y coordinates)
	'''

	print("Reading file: %s" % filename)
	file = open(filename)
	
	# Skip the header lines 
	for i in range(lineskip):
		file.readline()
	
	# Extract only the required columns
	df = pd.read_csv(file, sep = ' ', skiprows = 1, header = None, engine = 'python')
	df = df.sort_values(by = [1, 2])
	data = np.zeros((df.shape[0], 4))
	data[:,:] = df.loc[:, (X_COORD_COL, Y_COORD_COL, Z_COORD_COL, POWER_COL)] 
	return data

def convert_data_into_grid(data, spacing = 5):
	'''
	The data coming in has float coordinates, however we know it is in fact a rectangular grid
	'''
	#print(data)
	xvals = np.unique(data[:, 0])
	yvals = np.unique(data[:, 1])
	zvals = np.unique(data[:, 2])
	sinr_grid = np.zeros((yvals.size, xvals.size, zvals.size),)
	
	for j in range(xvals.size):
		for i in range(yvals.size):
			for k in range(zvals.size):
				sinr = data[j*yvals.size*zvals.size + i*zvals.size + k, 3]
				sinr_grid[yvals.size - i - 1 , j, zvals.size - k - 1] = sinr

				## FOR DEBUGGING ###
				# if (zvals.size - k - 1) == 2:
				# 	print('height:', data[j*yvals.size*zvals.size + i*zvals.size + k, 2])
				#####

	return sinr_grid

def main():
	MAX_POWER = -np.inf
	MIN_POWER = np.inf
	MAX_HEIGHT = -np.inf
	MIN_HEIGHT = np.inf
	summary_file = 'new_stored_grids/summary.txt'

	all_folders = [f for f in listdir(SCENARIOS_DIR) if isdir(join(SCENARIOS_DIR, f))]

	number_of_scenarios = len(set([f.split('-')[0] for f in listdir(SCENARIOS_DIR) if isdir(join(SCENARIOS_DIR, f))]))

	print('Number of scenarios:', number_of_scenarios)

	#all_comm_files = [f for f in listdir(COMM_DATA_DIR) if isfile(join(COMM_DATA_DIR, f))] 
	all_folders.sort()
	building_grid = []

	i = 0

	for folder in all_folders:
		scenario = int(folder.split('-')[0])
		transmitter = int(folder.split('-')[1])

		print('Scenario: %d, Transmitter: %d' % (scenario, transmitter))

		comm_file_path = SCENARIOS_DIR + folder + '/Study Area/setup.power.t001_01.r002.p2m'
		building_file_path = SCENARIOS_DIR + folder + '/city(2).city'
		print('Processing file', comm_file_path)
		data = read_csv_data(comm_file_path)
		power_grid = convert_data_into_grid(data)
		building_grid = get_building_grid(data, building_file_path)


		MAX_POWER = max(MAX_POWER, np.amax(power_grid))
		MIN_POWER = min(MIN_POWER, np.amin(power_grid))
		MAX_HEIGHT = max(MAX_HEIGHT, np.amax(building_grid))
		MIN_HEIGHT = min(MIN_HEIGHT, np.amin(building_grid))




		# Now we need to find the location of the user
		#indmax = np.argmax(power_grid)
		#coord = np.unravel_index(indmax, sinr_grid.shape)
		#division = 101
		#overlap = 2
		if scenario < (number_of_scenarios/3+1):
			test = True
		else:
			test = False



		#fig, (ax1, ax2) = plt.subplots(2, 1)
		#ax1.imshow(building_grid, cmap='plasma')
		#ax1.set(xlabel='x', ylabel='y',
        #  title=folder+' Test: '+str(test))
		#ax2.imshow(power_grid, cmap='plasma')
		#ax2.set(xlabel='x', ylabel='y',
        #  title=folder+' Test: '+str(test))


		if test:
			np.savetxt('new_stored_grids/pwr_grids/test/pwr_grid-%d-%d.txt' % (scenario, transmitter), power_grid, fmt='%f')
			np.savetxt('new_stored_grids/city_grids/test/city_grid-%d-%d.txt' % (scenario, transmitter), building_grid, fmt='%f')
		else:
			np.savetxt('new_stored_grids/pwr_grids/train/pwr_grid-%d-%d.txt' % (scenario, transmitter), power_grid, fmt='%f')
			np.savetxt('new_stored_grids/city_grids/train/city_grid-%d-%d.txt' % (scenario, transmitter), building_grid, fmt='%f')
		i += 1	

		print('Storing summary into %s'%summary_file)
		store_configuration(summary_file,
			MAX_POWER = MAX_POWER,
			MIN_POWER = MIN_POWER,
			MAX_HEIGHT = MAX_HEIGHT,
			MIN_HEIGHT = MIN_HEIGHT)
	
	print('Completed.')

	plt.show()
if __name__ == "__main__":
	main()


#ax = sns.heatmap(sinr_grid, linewidth=0.5)
#plt.show()