from typing import Optional
from data_set import Dataset
from predictor import Predictor, models
from predictor_utils import PiecewiseSchedule, OptimizerSpec
from utils import get_session, plot_grid, plot_confusion_matrix, downsample_matrix
import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import random
from rl_utils import get_rl_obs
from matplotlib import rc
import matplotlib
from active_sensing_environment import ExplorationEnvironment

from tf_agents.environments import py_environment
from tf_agents.environments import tf_environment
from tf_agents.environments import tf_py_environment
from tf_agents.environments import utils
from tf_agents.specs import array_spec
from tf_agents.environments import wrappers
from tf_agents.environments import suite_gym
from tf_agents.trajectories import time_step as ts
import tf_agents


class RLEnv(py_environment.PyEnvironment):
    def __init__(self, model_path : str, single_env =True, use_sd = False, 
                    clairvoyante = False, use_imitation_learning = False, use_median_reward = False,
                        step_size = 1, new_reward_function = 0, random_init =  0, n_uavs = 1, no_ob_downsample = False,
                            ee_max_episode_length = 200, mem_length = 1, test_ds_pfx = "test", train_ds_pfx = "train", train_ds = False,
                                dataset_path = None, uav_starting_proximity = True):
        # Path to the DL model. This will also be used to pull configuration information about the environment
        self.model_path = model_path
        # This will make all episodes run on a single transmitter and a single city
        self.single_env = single_env
        # Use SD observation
        self.use_sd = use_sd
        # Use supplemental observations
        self.clairvoyante = clairvoyante
        # Use imitation learning
        self.use_imitation_learning = use_imitation_learning
        # Use median reward per map
        self.use_median_reward = use_median_reward
        # Step size
        self.step_size = step_size
        # New reward functions
        self.new_reward_function = new_reward_function
        # Move randomly for first n steps
        self.random_init = 0
        # Number of UAVs
        self.n_uavs = n_uavs
        # If step size is greater than 1, do not downsample observation
        self.no_ob_downsample = no_ob_downsample
        # Episode length inside the exploration environment
        self.ee_max_episode_length = ee_max_episode_length
        # Stack the most recent N observations
        self.mem_length = mem_length
        # Prefix for dataset files
        self.test_ds_pfx = test_ds_pfx
        self.train_ds_pfx = train_ds_pfx
        self.train_ds = train_ds
        # Whether UAVs start separately or together
        self.uav_starting_proximity = uav_starting_proximity


        # Get the config file
        config_file = "/home/enesk/repos/spatial-prediction/logs/config-%s.config" % self.model_path
        config = pd.read_csv(config_file, sep = '\t').to_dict(orient = 'records')
        meas_rate = ([float(record['value']) for record in config if record['parameter'] == 'measured_rate'][0])
        model_name = ([str(record['value']) for record in config if record['parameter'] == 'model'][0])
        regression = ([str(record['value']) for record in config if record['parameter'] == 'regression'][0]) == 'True'
        no_maps = ([str(record['value']) for record in config if record['parameter'] == 'no_maps'][0]) == 'True'
        random_walk = ([str(record['value']) for record in config if record['parameter'] == 'random_walk'][0]) == 'True'
        cont_meas_rate = ([str(record['value']) for record in config if record['parameter'] == 'cont_meas_rate'][0]) == 'True'
        mc_dropout = ([str(record['value']) for record in config if record['parameter'] == 'mc_dropout'][0]) == 'True' 
        probabilistic = ([str(record['value']) for record in config if record['parameter'] == 'probabilistic'][0]) == 'True'
        try:
            probabilistic_with_distance_covar = ([str(record['value']) for record in config if record['parameter'] == 'probabilistic_with_distance_covar'][0]) == 'True'
        except:
            probabilistic_with_distance_covar = False
        try:
            dataset_dir = ([str(record['value']) for record in config if record['parameter'] == 'dataset_dir'][0])
        except:
            dataset_dir = "new_stored_grids"
        model_scale = ([float(record['value']) for record in config if record['parameter'] == 'model_scale'][0])
        if cont_meas_rate:
            meas_rate = 0.03

        # Override the dataset path
        if not(dataset_path is None):
            dataset_dir = dataset_path

        # Restore dataset and predictor
        self.ds = Dataset(data_dir=dataset_dir,measured_rate = meas_rate, 
                          no_maps = no_maps, train_file_pfx=train_ds_pfx, test_file_pfx=test_ds_pfx)
        self.predictor = Predictor(
                ds = self.ds, 
                session_name = self.model_path,
                regression = regression,
                model_name = model_name,
                probabilistic = probabilistic,
                probabilistic_with_distance_covar=probabilistic_with_distance_covar,
                mc_dropout = mc_dropout, 
                random_walk = random_walk,
                model_scale = model_scale,
                )
        self.predictor.restore("./models/%s/" % self.model_path)

        # Initiate the exploration environment
        self.ee = ExplorationEnvironment(n_uavs=self.n_uavs, predictor=self.predictor, dataset=self.ds, use_train_ds = train_ds,
                                            uav_starting_proximity=self.uav_starting_proximity)
        self.ee.policy = "rl"
        self.ee.max_episode_length = self.ee_max_episode_length
        

        # RL env parameters
        self.episode_length = self.ee.max_episode_length // self.step_size
        number_of_actions = self.ee.get_action_space_size()
        self._action_spec = array_spec.BoundedArraySpec(
        shape=(), dtype=np.int, minimum=0, maximum=number_of_actions-1, name='action')
        if not(self.no_ob_downsample):
            downsampling = self.step_size
        else:
            downsampling = 1
        if self.use_sd and (self.n_uavs == 1):
            self._observation_spec = array_spec.BoundedArraySpec(
                shape=(self.ds.obs_shape[0] // downsampling, self.ds.obs_shape[1] // downsampling, 4 * self.mem_length), dtype=np.float32, name='observation', )
        elif self.use_sd and (self.n_uavs > 1):
            self._observation_spec = array_spec.BoundedArraySpec(
                shape=(self.ds.obs_shape[0] // downsampling, self.ds.obs_shape[1] // downsampling, 4 * self.mem_length), dtype=np.float32, name='observation', )
        elif self.clairvoyante:
            self._observation_spec = array_spec.BoundedArraySpec(
                shape=(self.ds.obs_shape[0] // downsampling, self.ds.obs_shape[1] // downsampling, 3 * self.mem_length), dtype=np.float32, name='observation', )
        else:
            self._observation_spec = array_spec.BoundedArraySpec(
                shape=(self.ds.obs_shape[0] // downsampling, self.ds.obs_shape[1] // downsampling, 2 * self.mem_length), dtype=np.float32, name='observation', )     
        print(self._observation_spec)       
        self.reward_type = 'linear'

        # RL env state
        self._episode_ended = False
        self.step_idx = 0
    # @property
    # def batched(self) -> bool:
    #     return True
    
    # @property
    # def batch_size(self) -> int:
    #     return self.n_uavs

    def action_spec(self):
        return self._action_spec

    def observation_spec(self):
        return self._observation_spec
    
    def get_optimal_action_at_curr_state(self, ):
        assert self.n_uavs == 1
        uav_idx = 0
        # Get optimal action obtained through exhaustive search
        # Try to play with the level of exhaustive search for better pretraining of the DQN agent
        exhaustive_paths = np.random.randint(1, 300)
        opt_action = self.ee.get_random_exhaustive_actions(self.step_idx*self.step_size, stride = self.step_size, exhaustive_paths=exhaustive_paths)[uav_idx]
        # Have to sample multiple times for large stride size
        for _ in range(self.step_size-1):
            self.ee.get_random_exhaustive_actions(self.step_idx*self.step_size, stride = self.step_size)[uav_idx]

        return opt_action


    def _reset(self):        
        self._episode_ended = False

        if self.single_env:
            # Reset to the same scenario every time
            ss_grids, building_grid, initial_coordinates, user_locations = self.ee.reset(scenario_idx = 10, transmitter_idx=0)
        else:
            ss_grids, building_grid, initial_coordinates, user_locations = self.ee.reset()
            user_idx = 0
            if np.mean(ss_grids[user_idx]) < -200:
                return self._reset()

        # Counter for the number of steps 
        self.step_idx = 0

        # The current best error
        self.current_best_error = np.inf

        # UAV idx
        self.uav_idx = 0

        # Move randomly for some steps
        if self.random_init > 0:
            self.ee.policy = "random"
            for _ in range(self.random_init):
                for sss in range(self.step_size):
                    self.ee.step(step_idx=(self.step_idx*self.step_size+sss))
                self.step_idx = self.step_idx + 1
            self.ee.policy = "rl"

        # Predict the gain
        raw_value_pred, sd, ob, covar, pred, uav_loc_grid, true_norm_gain, uav_loc_grids_per_uav, n_measured_locs, measured_coords = self.ee.predict_ss(user_idx=0, no_full_covariance=True)

        # Initial number of measured locations
        self.past_n_measured_locs = n_measured_locs
        self.past_building_collisions = 0
        assert n_measured_locs[0] <= 1

        # Initiate observation buffer
        self.obs_buffers = []



        # Get the observation
        obs = self.get_obs(pred, sd, uav_loc_grid, true_norm_gain, n_agents =self.n_uavs, uav_loc_grids_per_uav = uav_loc_grids_per_uav, first_obs=True)


        # For imitation learning
        if self.use_imitation_learning != False:
            self.imitation_T_1 = random.randint(0, self.episode_length)
            self.imitation_T_2 = random.randint(0, self.episode_length)
        else:
            self.imitation_T_1 = None
            self.imitation_T_2 = None

        timesteps = []
        for uav_idx in range(self.n_uavs):
            timestep =  ts.restart(obs[uav_idx])
            timesteps.append(timestep)



        return timesteps
    
    def get_obs(self, pred, sd, uav_loc_grid, true_norm_gain, n_agents, uav_loc_grids_per_uav, first_obs = False):
        rl_ob = get_rl_obs(self.step_size, self.use_sd, self.clairvoyante, pred, true_norm_gain, sd, uav_loc_grid, n_agents, uav_loc_grids_per_uav, self.no_ob_downsample)
        if first_obs:
            for uav_idx in range(self.n_uavs):
                obs_buffer = []
                for _ in range(self.mem_length):
                    obs_buffer.append(rl_ob[uav_idx].astype(self._observation_spec.dtype))
                self.obs_buffers.append(obs_buffer)
        else:
            for uav_idx in range(self.n_uavs):
                obs_buffer = self.obs_buffers[uav_idx]
                for _ in range(self.mem_length):
                    obs_buffer.insert(0, rl_ob[uav_idx].astype(self._observation_spec.dtype))
                    obs_buffer.pop()
                self.obs_buffers[uav_idx] = obs_buffer



        return [np.concatenate(ob_buffer, axis = -1) for ob_buffer in self.obs_buffers]
    
    def get_reward_from_error(self, pred_error, new_measured_locs, new_collisions):
        


        if self.new_reward_function == 0:
            # Original reward function
            if self.current_best_error == np.inf:
                self.current_best_error = pred_error
            else:
                self.current_best_error = min(self.current_best_error, pred_error)
                
            self.current_error = pred_error

            
            error_bins = np.linspace(5, 35, 30)
            assoc_idx = np.sum(error_bins > self.current_best_error)
            asoc_rewards = (self.step_size/4) * np.linspace(0, 30, 31)/10 # Scale by step size for consisten eval
            reward = asoc_rewards[assoc_idx] 
        elif self.new_reward_function == 1:
            rewards = []
            # New reward functions
            for uav_idx in range(self.n_uavs):
                if self.step_idx == self.episode_length:
                    reward = 4*max(0, 25 - pred_error) 
                else:
                    reward = 0
                    
                    #reward += new_measured_locs[uav_idx] / (5 * ( self.episode_length / 10))
                rewards.append(reward)
            self.current_error = pred_error
            self.current_best_error = min(pred_error, self.current_best_error)
        elif self.new_reward_function == 2:
            # New reward functions
            if self.step_idx == self.episode_length:
                reward = max(0, 40-pred_error) / (self.episode_length)
            else:
                reward = max(0, 40-pred_error) / (self.episode_length)
                
            self.current_error = pred_error

        elif self.new_reward_function == 3:
            # New reward functions
            if self.step_idx == self.episode_length:
                reward = max(0, 40-pred_error)  / (self.episode_length / 10)
            else:
                reward = new_measured_locs / (5 * self.n_uavs * ( self.episode_length / 10))
                
            self.current_error = pred_error

        elif self.new_reward_function == 4:
            # New reward functions
            if self.step_idx == self.episode_length:
                reward = max(0, 40-pred_error)  / (self.episode_length / 10)
            else:
                reward = new_measured_locs / (5 * self.n_uavs * ( self.episode_length / 10))
                reward -= new_collisions / (15 * self.n_uavs * ( self.episode_length / 10))
                
            self.current_error = pred_error

        return rewards
    
    def _step(self, action):
        # Action should be a vector for each UAV


        if self._episode_ended:
            
            # The last action ended the episode. Ignore the current action and start
            # a new episode.
            
            return self.reset()
        
        # Step the enviornment 
        for sss in range(self.step_size):
            input_actions = {}
            for uav_idx in range(self.n_uavs):
                input_actions[uav_idx] = action[uav_idx]
            self.ee.step(step_idx=(self.step_idx*self.step_size+sss), input_actions=input_actions)

        # Predict the gain
        raw_value_pred, sd, ob, covar, pred, uav_loc_grid, true_norm_gain, uav_loc_grids_per_uav, n_measured_locs, measured_coords = self.ee.predict_ss(user_idx=0, no_full_covariance=True)
        
        # New measured locs
        new_measured_locs = [] 
        for uav_idx in range(self.n_uavs):
            new_measured_locs.append(n_measured_locs[uav_idx]-self.past_n_measured_locs[uav_idx])
            assert new_measured_locs[-1] >= 0
        self.past_n_measured_locs = n_measured_locs

        # New collisions
        new_collisions = self.ee.action_overrides - self.past_building_collisions
        self.past_building_collisions = self.ee.action_overrides
        
        # Get the reward
        pred_error, _ = self.ee.get_predict_error(raw_value_pred=raw_value_pred, median_error=self.use_median_reward, measured_coords=measured_coords)
        rewards = self.get_reward_from_error(pred_error, new_measured_locs, new_collisions)

        # Get the observation
        obs = self.get_obs(pred, sd, uav_loc_grid, true_norm_gain, n_agents = self.n_uavs, uav_loc_grids_per_uav=uav_loc_grids_per_uav)
        
        
        
        if not(self.step_idx == self.episode_length):
            self.step_idx += 1
            timesteps = []
            for uav_idx in range(self.n_uavs):
                timestep =  ts.transition(
                    obs[uav_idx], reward=rewards[uav_idx], discount=1.0)
                timesteps.append(timestep)
        else:
            self._episode_ended = True
            timesteps = []
            for uav_idx in range(self.n_uavs):
                timestep =  ts.termination(
                    obs[uav_idx], reward=rewards[uav_idx])
                timesteps.append(timestep)


        # Add the pred error
        #timestep.current_error = self.current_error
        return timesteps
        