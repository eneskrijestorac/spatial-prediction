import numpy as np
import scipy
from utils import get_session, plot_grid, plot_confusion_matrix, downsample_matrix


def shift_2d_replace(data, dx, dy, constant=0):
    """
    Shifts the array in two dimensions while setting rolled values to constant
    :param data: The 2d numpy array to be shifted
    :param dx: The shift in x
    :param dy: The shift in y
    :param constant: The constant to replace rolled values with
    :return: The shifted array with "constant" where roll occurs
    """
    shifted_data = np.roll(data, dx, axis=1)
    if dx < 0:
        shifted_data[:, dx:, :] = constant
    elif dx > 0:
        shifted_data[:, 0:dx, :] = constant

    shifted_data = np.roll(shifted_data, dy, axis=0)
    if dy < 0:
        shifted_data[dy:, :, :] = constant
    elif dy > 0:
        shifted_data[0:dy, :, :] = constant
    return shifted_data

def get_rl_obs(step_size, use_sd, clairvoyante, pred, true_norm_gain, sd, uav_loc_grid, n_agents, uav_loc_grids_per_uav, no_ob_downsample):
    # We should downsample the observation if the step size is larger than
    if (step_size > 1) and not(no_ob_downsample):
        # We do min here to not lose the information about where buildings are
        pred = downsample_matrix(pred, downsample_operation=np.min, K = step_size)
        true_norm_gain = downsample_matrix(true_norm_gain, downsample_operation=np.min, K = step_size)
        sd = downsample_matrix(sd, downsample_operation=np.max, K = step_size)
        uav_loc_grid = downsample_matrix(uav_loc_grid, downsample_operation=np.max, K = step_size)
        if 1:
            for agent_idx in range(n_agents):
                uav_loc_grids_per_uav[agent_idx] = downsample_matrix(uav_loc_grids_per_uav[agent_idx], downsample_operation=np.max, K = step_size)

    if n_agents == 1:
        if (use_sd):
            rl_ob = np.stack([pred, sd, uav_loc_grid, uav_loc_grids_per_uav[0]], axis = 2)
        elif clairvoyante:
            rl_ob = np.stack([pred, true_norm_gain, sd, uav_loc_grid, ], axis = 2)
        else:
            rl_ob = np.stack([pred, uav_loc_grid], axis = 2)
        rl_ob = [rl_ob]
    elif n_agents > 1:
        if (use_sd):
            rl_ob = []
            for agent_idx in range(n_agents):
                rl_ob_current = np.stack([pred, sd, uav_loc_grid, uav_loc_grids_per_uav[agent_idx]], axis = 2)
                l, w, _ = rl_ob_current.shape
                uav_coord = np.unravel_index(uav_loc_grids_per_uav[agent_idx].argmax(), uav_loc_grids_per_uav[agent_idx].shape)
                # plot_grid(rl_ob_current[:, :, 0])
                # plot_grid(rl_ob_current[:, :, 3])
                
                rl_ob_current = scipy.ndimage.shift(rl_ob_current, (-uav_coord[0]+l//2, -uav_coord[1]+w//2, 0))
                #rl_ob_current = shift_2d_replace(rl_ob_current, -uav_coord[1]+w//2, (-uav_coord[0]+l//2))
                # plot_grid(rl_ob_current[:, :, 0])
                # plot_grid(rl_ob_current[:, :, 3])
                rl_ob.append(rl_ob_current)
        elif clairvoyante:
            raise ValueError("Not implemented")
        else:
            raise ValueError("Not implemented")       

    return rl_ob