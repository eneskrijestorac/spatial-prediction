cd /media/enesk/optical_hdd/Enes/spatial_prediction/wi_setups_4d/
#cd /home/enesk/repos/spatial-prediction/wi_setups_6ghz

for d in */; do
	cd $d
    echo $(pwd)
	if [ -f "complete" ]; then
		echo "This scenario has been calculated."
	else 
		echo "Calculating this scenario"
		# Adding a timeout to interrupt frozen simulations
	    /usr/local/remcom/WirelessInSite/3.3.3.2/Linux-x86_64RHEL6/bin/remcom-shell <<< $'timeout --signal=INT 25m wibatch -f setup.Study\ Area.xml -out Study\ Area \nexit' && touch complete
		sleep 1
	fi
    cd ..
done


cd ..



