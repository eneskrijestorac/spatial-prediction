import numpy as np
from scipy.optimize import curve_fit
import pylab
from matplotlib import pyplot as plt


z = 12

def semivar_model(h, var_phi, d):
	var_0 = 0
	if d < 0:
		d = 0.000001
	return var_0 + var_phi * (np.exp(-np.minimum(h/d, 1000000 * np.ones(h.shape)  )))

def estimate_eta(coords, pwr, show_plot = False):
	# Estimates the path loss parameters
	# Remove outliers
	coords = coords[pwr >= -249, :]
	pwr = pwr[pwr >= -249]

	# Number of measuremenets
	M = len(pwr)

	# Take the negative log of distance to the user
	h = -10 * np.log10(np.linalg.norm(coords, axis = 1)).reshape((M, 1))

	pwr = pwr.reshape((M, 1))

	A = np.concatenate([np.ones((M,1)), h], axis = 1)

	# Estimate path loss parameters
	K, eta = np.linalg.lstsq(A, pwr, rcond=None)[0]

	if show_plot:
		predicted_pwr = A.dot(np.asarray([K, eta]).reshape((2, 1)))
		fig, ax = plt.subplots()
		ax.scatter(h, pwr, label='data')
		ax.plot(h, predicted_pwr, label='Unweighted fit')
		ax.legend()
		ax.set_xlabel("h (Distance between points)")
		ax.set_ylabel("yfit (Predicted path loss gain)")
		plt.show()

	return K, eta


def kriging_fit(coords, unmeasured_coords, pwr, K, eta, var_phi, d, show_plot = True):
	d = d
	# Number of measured coords
	M = len(pwr)
	# Number of unmeasured coords
	N = unmeasured_coords.shape[0]
	
	# Return the estimate and variance
	predicted_pwr = np.zeros((N,1))
	predicted_var = np.zeros((N,1))

	# Log distance to the user at unmeasured coords and path loss component
	predicted_h = -10 * np.log10(np.linalg.norm(unmeasured_coords, axis = 1)).reshape((N, 1))
	
	A = np.concatenate([np.ones((N,1)), predicted_h], axis = 1)
	predicted_pwr = A.dot(np.asarray([K, eta]).reshape((2, 1)))

	# Remove the path loss component
	pwr = pwr.reshape((M, 1))
	h = -10 * np.log10(np.linalg.norm(coords, axis = 1)).reshape((M, 1))
	A = np.concatenate([np.ones((M,1)), h], axis = 1)
	pwr = pwr - A.dot(np.asarray([K, eta]).reshape((2, 1)))

	# Get distance matrix
	distance_matrix = np.expand_dims(coords, axis=2) - np.expand_dims(coords.T, axis=0)
	distance_matrix = np.linalg.norm(distance_matrix, axis = 1)
	h = distance_matrix

	# Gamma matrix used for Krigin interpolation
	# This is essentially covariance matrix between measurements and measurements
	Gamma = np.zeros((M, M))
	Gamma[0:M, 0:M] = semivar_model(h, var_phi, d) + 0.001*np.eye(M,M)
	# Gamma[M, :] = 1
	# Gamma[:, M] = 1
	# Gamma[M, M] = 0
	Gamma_inv = np.linalg.pinv(Gamma)

	# These are the linear coefficients for interpolation
	W = np.zeros((N , M))

	# This is the distance between unmeasured and measured coordinates
	H = np.zeros((N, M))
	distance_matrix = np.expand_dims(unmeasured_coords, axis=2) - np.expand_dims(coords.T, axis=0)
	distance_matrix = np.linalg.norm(distance_matrix, axis = 1)
	H[:, :] = distance_matrix

	# This is the covariance between unmeasured and measured coordinates
	B = np.ones((N, M))
	B = semivar_model(H, var_phi, d)

	# This is the covariance between unmeasured and unmeasured coordinates
	distance_matrix = np.expand_dims(unmeasured_coords, axis=2) - np.expand_dims(unmeasured_coords.T, axis=0)
	distance_matrix = np.linalg.norm(distance_matrix, axis = 1)	
	D = semivar_model(distance_matrix, var_phi, d)

	# Calculate the interpolation coefficients
	W  = B.dot(Gamma_inv)
	predicted_pwr = predicted_pwr+ W.dot(pwr)
	#predicted_var = Lambda[:, M] +  np.diag(W.dot(B.T[0:M, :]))
	predicted_covar_matrix = D - W.dot(B.T)
	predicted_var = np.diag(predicted_covar_matrix)


	

	# for j in range(N):
	# 	b = np.zeros((1, M+1))
	# 	w = np.zeros((1, M))
	# 	for i in range(M+1):
	# 		if i == M:
	# 			b[0, i] = 1
	# 		else:
	# 			h = np.linalg.norm(coords[i] - unmeasured_coords[j])
	# 			b[0, i] = semivar_model(h, var_phi, d)
	# 	lamb = b.dot(Gamma_inv)
	# 	w[:, :] = lamb[0, 0:M]

	# 	predicted_pwr[j] = predicted_pwr[j]  + w.dot(pwr)

	# 	predicted_var[j] = lamb[0, M]
	# 	for m in range(M):
	# 		gamma_bar = semivar_model(h, var_phi, d)
	# 		predicted_var[j] = predicted_var[j] + b[0, m] * w[0, m]
	# 	#predicted_var[j] = np.maximum(predicted_var[j], 1)
	# print(np.sum(np.abs(predicted_var_new-predicted_var)), "np.sum(np.abs(predicted_var_new-predicted_var))")
	return predicted_pwr, predicted_var, predicted_covar_matrix


def fit_model(h, y):
	var_phi = 10
	d = 10
	p0 = var_phi, d
	w = np.ones(len(y))
	#delta = np.ones(len(y)) * 0.0001

	popt, pcov = curve_fit(semivar_model, h, y, p0, sigma = w)
		#_w = np.abs(y - semivar_model(h, *popt))
		#w = float(1)/np.maximum( delta, _w )

	return popt, pcov

def semivar_fit(coords, pwr, K, eta, show_plot = True):
	coords = coords[pwr > -240, :]
	pwr = pwr[pwr > -240]
	

	# Number of measurements
	M = len(pwr)

	# Reshape power measurements
	pwr = pwr.reshape((M, 1))

	# Log scale distance
	h = -10 * np.log10(np.linalg.norm(coords, axis = 1)).reshape((M, 1))

	# De-trend the measurements
	A = np.concatenate([np.ones((M,1)), h], axis = 1)
	pwr = pwr - A.dot(np.asarray([K, eta]).reshape((2, 1)))

	h = [] # Distance between measurements
	gamma = [] # Gamma is squared power difference


	distance_matrix = np.expand_dims(coords, axis=2) - np.expand_dims(coords.T, axis=0)
	distance_matrix = np.linalg.norm(distance_matrix, axis = 1)
	h = distance_matrix.flatten()


	# pwr_difference_matrix =0.5 *( np.expand_dims(pwr, axis=2) - np.expand_dims(pwr.T, axis=0))**2
	# pwr_difference_matrix = np.sum(pwr_difference_matrix, axis = 1)
	pwr_difference_matrix = ( np.expand_dims(pwr, axis=2)*np.expand_dims(pwr.T, axis=0))
	print("pwr_difference_matrix.shape", pwr_difference_matrix.shape)
	pwr_difference_matrix = np.sum(pwr_difference_matrix, axis = 1)
	print("pwr_difference_matrix.shape", pwr_difference_matrix.shape)
	gamma = pwr_difference_matrix.flatten()



	# for i in range(M):
	# 	for j in range(M):
	# 		h.append(np.linalg.norm(coords[i] - coords[j]))
	# 		gamma.append(0.5 * ((pwr[i] - pwr[j])**2))

	# h = np.asarray(h).flatten()
	# gamma = np.asarray(gamma).flatten()
	# unique_idx = h[:-1] != h[1:]
	# gamma = gamma[:-1][unique_idx] 
	# h = h[:-1][unique_idx] 
	# print(h.dtype)
	# print(gamma.dtype)
	

	# # Collect the unique points
	# h_old = [] + list(h)
	# gamma_old = [] + list(gamma)

	# h = np.sort(np.unique(h_old))
	# gamma = np.zeros(h.shape)

	# for j, h_j in enumerate(h):
	# 	N = 0
	# 	temp = 0
	# 	for i, h_i in enumerate(h_old):
	# 		if h_i == h_j:
	# 			temp = temp + gamma_old[i]
	# 			N = N + 1
	# 	gamma[j] = temp / N

	# print(len(h))
	# print(h.shape, gamma.shape)
	# print(len(gamma))
	# print(h.dtype)
	# print(gamma.dtype)

	# Find the fitted shadowing parameters
	# Remove outliers
	popt, pcov = fit_model(h[h<100], gamma[h<100])

	# Show the fitted curve
	yfit = semivar_model(h, *popt)

	if show_plot:
		#p = np.random.permutation(len(h))
		h = h[0:2000]
		gamma = gamma[0:2000]
		yfit = yfit[0:2000]
		h, gamma, yfit = zip(*sorted(zip(h,gamma,yfit)))
		fig, ax = plt.subplots()
		ax.scatter(h[0:2000], gamma[0:2000], label='data')
		ax.plot(h[0:2000], yfit[0:2000], label='Unweighted fit', c = 'r')
		ax.legend()
		ax.set_xlabel("h (Distance between points)")
		ax.set_ylabel("yfit (Shadowing correlation)")
		plt.show()

	return popt

if __name__ == '__main__':
	d = 3
	var_0 = 1
	var_phi = 2

	n = 100
	h = np.linspace(1, 20, n)

	yexact = var_0 + var_phi * (1 - np.exp(-h/d))
	ynoisy = yexact + np.random.randn(n) * 0.1

	popt, pcov = fit_model(h, ynoisy)

	yfit = semivar_model(h, *popt)

	print('Unweighted fit parameters:', popt)

	plt.plot(h, yexact, label='Exact')
	plt.scatter(h, ynoisy, label='Noisy')
	plt.plot(h, yfit, label='Unweighted fit')
	plt.legend()
	plt.show()
