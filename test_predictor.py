from data_set import Dataset
from predictor import Predictor, models
from predictor_utils import PiecewiseSchedule, OptimizerSpec
import tensorflow as tf
import datetime
from utils import get_session, plot_grid, plot_confusion_matrix
import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sn
import pandas as pd
from matplotlib import rc
import matplotlib

os.environ['CUDA_VISIBLE_DEVICES'] = '' # We don't need a GPU to test the model

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams.update({'font.size': 12})

rc('font',**{'family': 'sans-serif','sans-serif':['Arial']})



if __name__ == '__main__':
	
	session_name = 'prob-rw'
	config_file = "/home/enesk/repos/spatial-prediction/logs/config-%s.config" % session_name
	config = pd.read_csv(config_file, sep = '\t').to_dict(orient = 'records')
	meas_rate = ([float(record['value']) for record in config if record['parameter'] == 'measured_rate'][0])
	model_name = ([str(record['value']) for record in config if record['parameter'] == 'model'][0])
	regression = ([str(record['value']) for record in config if record['parameter'] == 'regression'][0]) == 'True'
	no_maps = ([str(record['value']) for record in config if record['parameter'] == 'no_maps'][0]) == 'True'
	random_walk = ([str(record['value']) for record in config if record['parameter'] == 'random_walk'][0]) == 'True'
	cont_meas_rate = ([str(record['value']) for record in config if record['parameter'] == 'cont_meas_rate'][0]) == 'True'
	try:
		mc_dropout = ([str(record['value']) for record in config if record['parameter'] == 'mc_dropout'][0]) == 'True' 
	except IndexError:
		mc_dropout = False

	probabilistic = ([str(record['value']) for record in config if record['parameter'] == 'probabilistic'][0]) == 'True'
	plot = True
	benchmark = False
	kriging = True
	try: 
		model_scale = ([int(record['value']) for record in config if record['parameter'] == 'model_scale'][0])
	except IndexError:
		model_scale = 1
	
	if cont_meas_rate:
		meas_rate = 0.03

	print(meas_rate)
	


	ds = Dataset(measured_rate = meas_rate, no_maps = no_maps)
	session = get_session()

	model_checkpoint = "/home/enesk/repos/spatial-prediction/models/model-%s.cpkt" % session_name


	predictor = Predictor(
            ds = ds, 
            model = models[model_name], 
            session = session,
            model_checkpoint = model_checkpoint,
            session_name = session_name,
            model_scale = model_scale,
            random_walk = random_walk,
            regression = regression,
            probabilistic = probabilistic,
            mc_dropout = mc_dropout,
            test = True
			)

	n_samples = 1
	
	if n_samples > 1:
		plot = False

	if benchmark or kriging:
		obs, labels, outdoors, non_scaled_maps, reconstruct_means, reconstruct_probs = ds.sample_whole_maps(n_samples, train = False, 
										include_non_scaled  = True, kriging = kriging, random_walk = random_walk, include_reconstruct = benchmark)
	else:
		obs, labels, outdoors, non_scaled_maps = ds.sample_whole_maps(n_samples, train = False, 
										include_non_scaled  = True, include_reconstruct = benchmark, random_walk = random_walk)		
	predictions = predictor.forward_pass(obs, labels)

	hard_loss = np.zeros((n_samples, 2))
	soft_loss = np.zeros((n_samples, 2))
	obs = np.asarray(obs)
	labels = np.asarray(labels)
	if regression or probabilistic or mc_dropout:
		raw_value_pred = ds.scaled_to_raw(predictions)
		
		flattened_pred1 = flattened_pred1 + list((raw_value_pred >= ds.PWR_THRESHOLD).flatten())
		hard_loss[i, 0] = np.mean(np.abs(labels[i] == (raw_value_pred >= ds.PWR_THRESHOLD)))
		soft_loss[i, 0] = np.sum(np.abs(raw_value_pred - non_scaled_maps[i][0]) * outdoors[i].reshape(non_scaled_maps[i][0].shape)) / (np.sum(outdoors[i])) 
		if benchmark or kriging:
			soft_loss[i, 1] = np.sum(np.abs(reconstruct_means[i] - non_scaled_maps[i][0]) * outdoors[i].reshape(non_scaled_maps[i][0].shape))  / (np.sum(outdoors[i])) 
			reconstruct_means[i][outdoors[i].reshape(non_scaled_maps[i][0].shape) == 0] = -250
		raw_value_pred[outdoors[i].reshape(non_scaled_maps[i][0].shape) == 0] = -250
			

	if benchmark or kriging:	
		hard_loss[i, 1] = np.mean(np.abs(labels[i] == (reconstruct_probs[i][2] >= 0.5)))
		flattened_pred2 = flattened_pred2 + list((reconstruct_probs[i][2] >= 0.5).flatten())

	for i in range(n_samples):
		if plot:
			#plot_grid(obs[i][:, :, 0], title = 'Buildings')
			plot_grid(non_scaled_maps[i][0], title = 'True signal strength', cbartitle = 'Power (dBm)')
			plot_grid(non_scaled_maps[i][1], title = 'City map', cbartitle = 'Height (m)')
			plot_grid(non_scaled_maps[i][2], title = 'Measurements', cbartitle = 'Power (dBm)')

			#plot_grid(labels[i].reshape(non_scaled_maps[i][0].shape), title = 'True signal strength', cbartitle = 'Power (dBm)')
			#plot_grid(outdoors[i].reshape(non_scaled_maps[i][0].shape), title = 'Outdoors')

			if regression or probabilistic or mc_dropout:
				plot_grid(raw_value_pred, title = 'Proposed', cbartitle = 'Power (dBm)')
			
			if benchmark: 
				plot_grid(reconstruct_means[i], title = 'Reconstruction', cbartitle = 'Power (dBm)')
				#plot_grid(reconstruct_probs[i][2], title = 'Reconstruction prediction')
			elif kriging:
				plot_grid(reconstruct_means[i], title = 'Kriging', cbartitle = 'Power (dBm)')
				#plot_grid(reconstruct_probs[i][2], title = 'Kriging prediction')
			

	print(np.mean(hard_loss, axis = 0))
	print(np.mean(soft_loss, axis = 0))

	#df_confusion = pd.crosstab(np.asarray(flattened_labels, dtype = np.int), np.asarray(flattened_pred1, dtype = np.int), rownames = ['Actual'],
						colnames = ['Predicted'], normalize = 'index')
	#plot_confusion_matrix(df_confusion, filename = 'figures/proposed_%s_c_matrix.png' % session_name)


	if benchmark:
		df_confusion = pd.crosstab(np.asarray(flattened_labels, dtype = np.int), np.asarray(flattened_pred2, dtype = np.int), rownames = ['Actual'],
							colnames = ['Predicted'], normalize = 'index')
		plot_confusion_matrix(df_confusion, filename = 'figures/benchmark_%s_c_matrix.png' % session_name)

	plt.show()
