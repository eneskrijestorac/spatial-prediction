from data_set import Dataset
from predictor import Predictor, models
from predictor_utils import PiecewiseSchedule, OptimizerSpec
import tensorflow as tf
import datetime
from utils import get_session, store_configuration
import argparse
import os

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Train predictor.')
	parser.add_argument('--sess-name', dest='session_name', 
	            action='store', help = "If included, the outputs will be stored with this prefix.", default = "")
	parser.add_argument('--model', dest='model', 
	            action='store', help = "The model that will be used.", default = "unet_new")
	parser.add_argument('--meas-rate', dest='measured_rate', type = float,
	            action='store', help = "Percentage of measurements.", default = 0.05)
	parser.add_argument('--use-gpu', dest='use_gpu', type = int,
	            action='store', help = "Select GPU", default = 1)
	parser.add_argument('--model-scale', dest='model_scale', type = int,
	            action='store', help = "Scale model factor", default = 1)
	parser.add_argument('--regression', dest='regression',
	            action='store_true', help = "Regression prediction", )
	parser.add_argument('--probabilistic', dest='probabilistic',
	            action='store_true', help = "Probabilistic prediction", )
	parser.add_argument('--simp-prob', dest='simp_prob',
	            action='store_true', help = "Simple probabilistic", )
	parser.add_argument('--mc-dropout', dest='mc_dropout',
	            action='store_true', help = "MC dropout", )
	parser.add_argument('--random-walk', dest='random_walk',
	            action='store_true', help = "Probabilistic prediction", )
	parser.add_argument('--no-maps', dest='no_maps',
	            action='store_true', help = "Don't use maps", )
	parser.add_argument('--cont-meas-rate', dest='cont_meas_rate',
	            action='store_true', help = "Cont meas rate", )
	parser.add_argument('--detrend', dest='detrend',
	            action='store_true', help = "Only learn shadowing", )

	args = parser.parse_args()

	session_name = args.session_name
	mc_dropout = args.mc_dropout
	measured_rate = args.measured_rate
	model = models[args.model]
	model_name = args.model
	use_gpu = args.use_gpu
	detrend = args.detrend
	regression = args.regression
	random_walk = args.random_walk
	probabilistic = args.probabilistic
	simp_prob = args.simp_prob
	no_maps = args.no_maps
	model_scale = args.model_scale
	cont_meas_rate = args.cont_meas_rate or True

	if not(int(use_gpu)):
		os.environ['CUDA_VISIBLE_DEVICES'] = '' # Disable GPU
	elif int(use_gpu) == 1:
		os.environ['CUDA_VISIBLE_DEVICES'] = '0' # Use GPU 0
	elif int(use_gpu) == 2:
		os.environ['CUDA_VISIBLE_DEVICES'] = '1' # Use GPU 1
	else:
		raise ValueError('Invalid GPU number selected.')

	if session_name == "":
		session_name = datetime.datetime.now().replace(microsecond=0).isoformat()

	session = get_session()
	num_iterations = 20000

	ds = Dataset(measured_rate = measured_rate, no_maps = no_maps, cont_meas_rate = cont_meas_rate)

	lr_schedule = PiecewiseSchedule([
	                                     (0,                   1e-4),
	                                     (num_iterations,  1e-4),
	                                ],
	                                outside_value=5e-6)

	optimizer = OptimizerSpec(constructor=tf.train.AdamOptimizer, kwargs=dict(epsilon=1e-4), lr_schedule=lr_schedule)

	def stopping_criterion(t):
	    return t >= num_iterations
	

	predictor = Predictor(
			ds = ds, 
			model = model, 
			session = session,
			optimizer_spec = optimizer, 
			stopping_criterion = stopping_criterion,
			session_name = session_name,
			regression = regression,
			probabilistic = probabilistic,
			mc_dropout = mc_dropout, 
			simp_prob = simp_prob,
			random_walk = random_walk,
			model_scale = model_scale,
			detrend = detrend
			)

	store_configuration('logs/config-%s.config' % session_name, 
				num_iterations = num_iterations,
				schedule = lr_schedule,
				measured_rate = measured_rate,
				regression = regression, 
				probabilistic = probabilistic,
				model = model_name,
				random_walk = random_walk,
				no_maps = no_maps,
				mc_dropout = mc_dropout,
				detrend = detrend,
				model_scale = model_scale,
				simp_prob = simp_prob,
				cont_meas_rate = cont_meas_rate)

	while not predictor.stopping_criterion_met():
		predictor.update_model()
		predictor.log_progress()