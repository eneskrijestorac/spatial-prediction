import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import tensorflow as tf


def downsample_matrix(mat, downsample_operation, K):
	"""
	mat = np.array([[  20,  200,   -5,   23],
		[ -13,  134,  119,  100],
		[ 120,   32,   49,   25],
		[-120,   12,   9,   23]])

	M, N = mat.shape
	K = 2
	L = 2

	MK = M // K
	NL = N // L
	print(mat[:MK*K, :NL*L].reshape(MK, K, NL, L).max(axis=(1, 3)))
	# [[200, 119], [120, 49]] 
	"""
	assert mat.shape[0] == mat.shape[1]
	assert mat.shape[0] % K == 0
	assert len(mat.shape) == 2
	assert downsample_operation in [np.max, np.min]
	
	M, N = mat.shape
	L = K
	MK = M // K
	NL = N // L

	mat_rehshape = mat.reshape(MK, K, NL, L)
	mat_downsample = downsample_operation(mat_rehshape, axis=(1, 3))
	
	return mat_downsample

def plot_grid(grid, fig_ax = None, colorbar = False, old_colorbar=True, mask = [], points = [],  range_val = [], title = '', cbartitle = '', spacing = 4, step = 10, dots = []):
	grid = np.copy(grid)
	if fig_ax:
		# For plots with multiple subplots
		old_colorbar=False
		fig, ax1, axes = fig_ax
	else:
		fig, ax1 = plt.subplots()
		ax_last = ax1

	masked_array = np.ma.array (grid, mask=np.isnan(grid))
	if colorbar == 2:
		cmap = matplotlib.cm.plasma
	else:
		cmap = matplotlib.cm.jet
	cmap.set_bad('white',1.)

	if len(mask):
		grid[mask] = None
	
	if len(range_val) == 0:
		im1 = ax1.imshow(grid, cmap=cmap)
	else:
		#grid = (grid - range_val[0]) / (range_val[1] - range_val[0])
		im1 = ax1.imshow(grid, cmap=cmap, vmin = range_val[0], vmax = range_val[1])
	# ax1.imshow(masked_array, interpolation='none', cmap=cmap)
	# ax1.imshow(masked_array, cmap=cmap)


	ax1.set(xlabel='x (m)', ylabel='y (m)',
	  title=title)
	if len(dots):
		ax1.legend()

	ax1.set_xticks(np.arange(grid.shape[1], step = step))
	ax1.set_xticklabels(np.arange(grid.shape[1], step = step) * spacing)
	ax1.set_yticklabels(np.arange(grid.shape[0], step = step) * spacing)
	ax1.set_yticks(np.arange(grid.shape[0], step = step))

	if colorbar == 2:
		fig.colorbar(im1, ax=axes.ravel().tolist(), cmap=cmap, fraction=0.046, pad=0.04, location='left')
	elif colorbar == 1:
		fig.colorbar(im1, ax=axes.ravel().tolist(), cmap=cmap, fraction=0.046, pad=0.04)
		#cbar1.ax.set_ylabel(cbartitle, rotation=-90, va="bottom")
	else:
		ax1.figure.colorbar(im1, ax=ax1, cmap=cmap)


	
	if len(dots):
		ax1.scatter(dots[0], dots[1], c = 'w', s = 10, marker='x')
		ax1.get_legend().remove()

	# # Plot points
	if len(points):
		ax1.scatter(points[0], points[1], c = 'g', s = 50)
		#ax1.get_legend().remove()
	

	
	return fig

def plot_grid_3d(grid, building_grid, title = '', cbartitle = '', spacing = 4, step = 5, dots = []):
	grid = np.copy(grid)
	resize_len = grid.shape[0]
	resize_len = 46
	startX = 50
	startY = 0
	grid = grid[startX:startX+resize_len, startY:startY+resize_len]
	building_grid = building_grid[startX:startX+resize_len, startY:startY+resize_len]

	# Initiate plot
	fig = plt.figure(figsize=(14, 14), dpi=400)
	# fig = plt.figure(figsize=(28, 28))
	fig.patch.set_alpha(0.0)
	ax2 = fig.add_subplot(122, projection='3d')


	

	cmap = matplotlib.cm.jet
	cmap.set_bad('white',1.)

	# Scale the grid
	grid = grid - np.min(grid)
	grid = grid / np.max(grid)

	# Remove indor areas
	# grid[building_grid > 10] = None


	# Plot the heat map
	x, y, z = np.indices(grid.shape + (11,))
	# cube1 = (z == 10) & (x == 30) & (y >= 23) & (y <= 30)
	# cube2 = (z == 10) & (x <= 30) & (y == 23) & (x >= 10)
	# cube3 = (z == 10) & (x <= 30) & (y == 10) 
	# voxelarray = cube1 | cube2 | cube3
	voxelarray = z == 10
	#ax2.plot_surface(xx, yy, Z, rstride=1, cstride=1, facecolors=cmap(grid), shade=False)
	print((grid[y, x]).shape)
	ax2.voxels(voxelarray, facecolors=cmap(grid[y, x]), edgecolor=None)

	
	## Create a building plot
	x, y, z = np.indices(grid.shape + (15,))
	cube1 = (building_grid[y, x] > 0) & (building_grid[y, x] >= z)
	voxelarray = cube1
	colors = np.empty(voxelarray.shape, dtype=object)
	colors[cube1] = 'white'
	ax2.voxels(voxelarray, facecolors=colors, edgecolor=None)


	## Create a ground plot
	x, y, z = np.indices(grid.shape + (2,))
	cube1 = (x >= 0) & (z == 0)
	cube2 = (np.abs(grid[y, x] - np.max(grid)) <= 0) & (z == 1)
	#cube2 = (x == 20) & (y == 40) & (z == 1)
	voxelarray = cube1 | cube2
	print(x[cube2], np.max(grid))
	colors = np.empty(voxelarray.shape, dtype=object)
	colors[cube1] = 'grey'
	colors[cube2] = 'red' # For user location
	ax2.voxels(voxelarray, facecolors=colors, edgecolor=None)

	# Plot a line
	ax2.plot([0, 30], [0,30],zs=[0,10])


	# Change view
	ax2.view_init(elev=25., azim=120)
	#ax2.view_init(elev=90., azim=270)

	# Z-axis is stretetched a lot
	ax2.set_xlim([0, resize_len])
	ax2.set_ylim([0, resize_len])
	ax2.set_zlim([0, resize_len/2])

	# Hide all axes
	ax2.set_xlabel("x (m)")
	ax2.set_ylabel("y (m)")
	ax2.set_zlabel("z (m)")
	ax2.grid(False)
	# ax2.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
	# ax2.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
	ax2.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
	# ax2.set_xticks([])
	# ax2.set_yticks([])
	ax2.set_zticks([])
	# ax2.xaxis._axinfo["grid"]['color'] =  (1,1,1,0)
	# ax2.yaxis._axinfo["grid"]['color'] =  (1,1,1,0)
	ax2.zaxis._axinfo["grid"]['color'] =  (1,1,1,0)
	ax2.set_axis_off()


	# ax1.set_xticks(np.arange(grid.shape[1], step = step))
	# ax1.set_xticklabels(np.arange(grid.shape[1], step = step) * spacing)
	# ax1.set_yticklabels(np.arange(grid.shape[0], step = step) * spacing)
	# ax1.set_yticks(np.arange(grid.shape[0], step = step))
	# cbar1 = ax1.figure.colorbar(im1, ax=ax1, cmap=cmap, fraction=0.046, pad=0.04)
	# cbar1.ax.set_ylabel(cbartitle, rotation=-90, va="bottom")
	return fig

def store_configuration(file, **kwargs):

	config_file = file
	f = open(config_file, 'w')
	f.write('parameter\tvalue\n')
	for key in kwargs.keys():
		f.write('%s\t%s\n' % (key, str(kwargs[key])))
	f.close()


def get_available_gpus():
	from tensorflow.python.client import device_lib
	local_device_protos = device_lib.list_local_devices()
	return [x.physical_device_desc for x in local_device_protos if x.device_type == 'GPU']


def get_session():
	tf.reset_default_graph()
	tf_config = tf.ConfigProto(
		inter_op_parallelism_threads=1,
		intra_op_parallelism_threads=1,)
	session = tf.Session(config=tf_config)
	print("AVAILABLE GPUS: ", get_available_gpus())
	return session

def huber_loss(x, delta=1.0):
	# https://en.wikipedia.org/wiki/Huber_loss
	return tf.where(
		tf.abs(x) < delta,
		tf.square(x) * 0.5,
		delta * (tf.abs(x) - 0.5 * delta)
	)


def plot_confusion_matrix(df_confusion, title='Confusion matrix', cmap=plt.get_cmap('Blues'), filename = 'c_matrix.png'):
	plt.matshow(df_confusion, cmap=cmap) # imshow
	#plt.title(title)
	plt.colorbar()
	tick_marks = np.arange(len(df_confusion.columns))
	plt.xticks(tick_marks, df_confusion.columns, rotation=45)
	plt.yticks(tick_marks, df_confusion.index)
	for (i, j), z in np.ndenumerate(df_confusion):
		plt.text(j, 0.5*i+0.25, '{:0.4f}'.format(z), ha='center', va='center')
	#plt.tight_layout()
	plt.ylabel(df_confusion.index.name)
	plt.xlabel(df_confusion.columns.name)
	plt.savefig(filename)
